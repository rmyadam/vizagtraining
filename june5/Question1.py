'''
This program exhibits inheritance where rocket is super class and 
 marsrover and moonrover are sub class 
 @Rithvik 
'''


class Rocket:
    '''
    This class contains the information about rocket
    that is used to deliver different rovers
    attributes: name , fuel, company, engine configuration
    methods : Moon rover and Mars rover
    ''' 
    def __init__(self):
        '''
        This method creates objects with following attributes which are global to all 
            child classes
        '''
        self.rocketName="falcon9"
        self.fuel="kerosine"
        self.company="Spacex"
        self.engine="Merlin"
        self.speed="39600 km/h"
        
    def rocketConfiguration(self):
        '''
        This method prints all the attributes of the rocket 
        '''
        print("The name of the rocket is: ",self.rocketName)
        print("The fuel used in rocket is :",self.fuel)
        print("The company which manufactured rocket is :",self.company)
        print("The engine used in rocket is :", self.engine)
        print("The rocket speed is :",self.speed)
    def reusableStage2(self,rover):
        '''
        The method checks whether the stage 2 if rocket is reusable for travel 
        '''
        if rover == "mars":
            print("The stage 2 is not reusable")
        if rover == "moon":
            print("The stage 2 is reusable")




class MoonRover(Rocket):
    '''
    This class inherits the rocket class and has configuration of rover,
    destination, distance of travel, time taken 
    attributes: rover, destination, distance, time to reach the destination 
    '''
    def __init__(self):
        Rocket.__init__(self)
        '''
        This method creates the object with following attributes
        '''
        self.rover="Moon Rover"
        self.destnation= "Moon"
        self.distance="384400 kilometres"
        self.time=0
    
    def timeTaken(self):
        '''
        This method calculates the time taken for travel by accessing the speed of 
            rocket which is a parent class
        '''
        self.distance=self.distance.split(' ')
        self.distance=int(self.distance[0])
        print(self.distance)
        self.speed=self.speed.split(' ')
        self.speed=int(self.speed[0])
        self.time=self.distance/self.speed
        return self.time

    def moonRoverConfiguration(self):
        '''
        This method prints all attributes of this class 
        '''
        print("The name of rover is :",self.rover)
        print("The destination is :",self.destnation)
        print("The distance of travel is : ",self.distance)
        print("The time taken is : ",self.time)


class MarsRover (Rocket):
    '''
    This class inherits the Rocket class and has configuration of rover,
    destination, distance of travel, time taken
    attributes : rover , destination, distance, time taken 
    '''
    def __init__(self):
        Rocket.__init__(self)
        '''
        This method creates object with following attributes
        '''
        self.rover="Moon Rover"
        self.destnation= "Moon"
        self.distance="14500000 kilometres"
        self.time=0
    def timeTaken(self):
        '''
        This method calculates the time by accessing the speed of rocket which is a parent class 

        '''
        self.distance=self.distance.split(' ')
        self.distance=int(self.distance[0])
        self.speed=self.speed.split(' ')
        self.speed=int(self.speed[0])
        self.time=self.distance/self.speed
        return self.time
    def marsRoverConfiguration(self):
        '''
        prints all attributes of this class
        '''
        print("The name of rover is :",self.rover)
        print("The destination is :",self.destnation)
        print("The distance of travel is : ",self.distance)
        print("The time taken is : ",self.time)


    
def main():
    # creates the object of child class moonrover
    moonRover1 = MoonRover()
    # access the method of parent class (Rocket)
    moonRover1.rocketConfiguration()
    # access the method of parent class (Rocket)
    moonRover1.reusableStage2("moon")
    # method of child class (moonRover) by using speed attribute from parent class (Rocket)
    moontime = moonRover1.timeTaken()
    print("The time taken to trvael to moon : ",moontime,"hrs")
     # creates the object of child class mars Rover
    marsRover1 = MarsRover()
    # access the method of parent class (Rocket)
    marsRover1.rocketConfiguration()
    # access the method of parent class (Rocket)
    marsRover1.reusableStage2("mars")
    # method of child class (marsRover) by using speed attribute from parent class (Rocket)
    marstime = marsRover1.timeTaken()
    print("The time taken to travel to mars is : ",marstime,"hrs") 
main()    
    