'''
This programs depicts implementation of linkedlist by object oriented programming 
    and operations on linkedlist
 @Rithvik
'''

class Node:
    '''
    This class is used to create a node of linkedlist
    attributes: self.value and self.next()
    '''
    def __init__(self,value):
        '''
        This method is used to create an object of respective clasc(Node)
        self.value -> value of that particular node
        self.next -> pointer pointing towards the adjacent node
        '''
        self.value = value
        self.next = None

class LinkedList:
    '''
    This class consists of methods which perform operation on linked list
    attributes : self.head,self.length and self.index
    self.head -> points the head of list 
    self.length -> the length of the list
    self.index -> index of the list on which operation has to be performed
    '''

    def __init__(self):
        '''
        This method is used to create object of linkedList class
        '''
        self.head = None
        self.length = 0
        self.index = 0
    
    def addNode(self,value):
        '''
        This method is used to add new nodes to the linkedList
        input: value (Int)
        output: forms a linked list by appending new nodes  
        '''
        # creation of newnode (object of Node class)
        newNode = Node(value)
        # assigning the first node to head 
        if self.head is None: 
            self.head = newNode
            return
        # aggining the head of list to temporary variable(temp)
        temp = self.head
        # assiging each node next to respective adjacent node 
        while True:
            if temp.next is None:
                temp.next = newNode
                return
            temp = temp.next
        
    
    def printLinkedList(self):
        '''
        This method prints all the nodes in the linked list
        '''
        # assigning the head of linked list to temporary variable(temp)
        temp = self.head
        # if there are no nodes in list throws error
        if temp is None:
            print("The linkedList is empty")
            return 
        # else iterates the list by checking the node next is None or not 
        #   and prints the respective node values till the end 
        while True:
            if temp is None:
                print(temp)
                return
            print(temp.value, end='->')
            temp = temp.next

    def insertElement(self,value):
        '''
        This method inserts a new node in the start of linked list 
        '''
        # creation of new node(addnode) of class Node
        addnode = Node(value)
        # asigning the head node to addnode next and making addnode head 
        #   of the linked list 
        addnode.next = self.head
        self.head = addnode
        return
    
    def insertLastElement(self,value):
        '''
        This method adds new node at the end of linked list 
        '''
        # creation of new node of class Node
        addnode = Node(value)
        # assigning head of linked list to temporary variable(temp)
        temp = self.head 
        # iterating through nodes 
        while True:
            # if list is empty adds node in the front by using self.insertElement
            #   method
            if temp is None:
                self.insertElement(value)
                return
            # else traverses the list till the node whose next is none and points the last node
            #   next to add node 
            if temp.next is None:
                temp.next = addnode
                return 
            temp = temp.next       
        
    def deleteElement(self):
        '''
        This method is used to delete the element in the end of linked list
        '''
        # assigning the head of linked list to temporary variable(temp)
        temp = self.head
        # if the list is empty it throws error 
        if temp is  None:
            print("There are no elements to delete")
            return
        # else assign the head to second node and making the first node
        #   next equal to None
        self.head = self.head.next
        temp.next = None
        return 
    def deleteLastElement(self):
        '''
        This method is used to delete the node in the end of linked list 
        '''
        # assigning the head of list to temporary variable(temp)
        temp = self.head
        # iterating through the list 
        while True: 
            # if list is empty it throws error 
            if temp is None:
                print("There are no elements to delete ")
                return
            # else it makes last butone node next to None 
            if temp.next.next is None:
                temp.next = None
                return 
            temp = temp.next

    def lengthOfList(self):
        '''
        This method is used to find the length of the linked list
        '''
        # assigning the head of list to temporary variable(temp)
        temp = self.head
        # iterating through elements and incrementing the counter(self.length)
        while True:
            if temp is not None:
                self.length +=1
            else:
                break
            temp = temp.next

    def insertMiddleElement(self,position,value):
        '''
        This method is used to insert the node in the middle of list according to
            the position given 
        input: value(Int)->value of new node position(Int)-> index at which the node 
                to be placed
        '''
        # creation of a new node(addnode) of class Node
        addnode = Node(value)
        # assigning the head of list to temporary variable(temp)
        temp = self.head 
        # if given position is out of bounds it throws error 
        if position > self.length:
            print("Enter the position within the length of linked list")
            return
        # else iterates through the list 
        while True:
            # when the index is reached position the add node next is assigned to
            #   current node next and current node next is assigned to add node  
            if (position!=0) and temp is not None:
                self.index += 1
                if self.index == position:
                    addnode.next = temp.next
                    temp.next = addnode 
                    return
            # if position is 0 then id uses self.insertElement method 
            #   to add new node(addnode) in the start of list 
            if position == 0:
                self.insertElement(value)
                return
            temp = temp.next

    def deleteMiddleElement(self,position):
        '''
        This method is used to delete a node at given position 
        input: position(Int)->value of new node position(Int)-> index at which the node 
                to be deleted
        '''
        # if the given position is out of bounds it throws error
        if position >= self.length:
            print("Enter the position within the length of linked list")
            return
        # assigning head of list and head's next to temporary variables
        #   temp1 and temp2 respectively 
        # print(self.head)
        temp1 = self.head
        temp2 = temp1.next
        # iterating through the list 
        while True:
            # when the index is reached position previous node next is assigned to 
            #   the current node next and the current node next is made None
            if (position!=0) and temp2 is not None:
                if self.index == position:
                    temp1.next = temp2.next
                    temp2.next = None 
                    return
            # if given position is 0 the self.deleteElement() is used 
            #    to remove the element from the start 
            if position == 0 and temp1 is None:
                self.deleteElement()
                return
            temp1 = temp1.next
            temp2 = temp2.next


def listMethods():
    '''
    This function calls the methods defined in class Node and class LinkedList
        which perform various operations on linked list 
    '''
    # creates the object of LinkedList class 
    listObject = LinkedList()
    # this medthod find the length of the list
    # adding the nodes into the linked list
    listObject.addNode(1)
    listObject.addNode(2)
    listObject.addNode(3)
    listObject.addNode(5)
    listObject.insertElement(6)
    listObject.insertLastElement(7)
    listObject.lengthOfList()
    listObject.insertMiddleElement(2,8)
    listObject.lengthOfList()
    listObject.deleteMiddleElement(4)
    # print the linked list
    print("The linked list is : ")
    listObject.printLinkedList()
if __name__=='__main__':
    listMethods()