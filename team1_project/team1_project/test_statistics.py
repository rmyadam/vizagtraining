'''Implementation of differnt testcases for statistics'''
import unittest
from statistics import *
# import seaborn as sns


class testcases(unittest.TestCase):


    def test_validate_list(self):
        '''
        This test suite tests the validate function for various 
        data types in a list 
        '''
        # creation of object of Statistics class
        sts = Statistics()

        # test cases for list as an input with int and float data types
        self.assertTrue(sts.validate([1,2,3,4,5]))
        self.assertTrue(sts.validate([1.2,3.5,5.5,7.5]))
        self.assertTrue(sts.validate([1.5,2,3.5,4,5]))

        # test cases for empty list and string as input 
        self.assertFalse(sts.validate([]))
        self.assertFalse(sts.validate([[]]))
        self.assertFalse(sts.validate(""))
        self.assertFalse(sts.validate([""]))

        # test cases for list of strings as input 
        self.assertFalse(sts.validate(["mohitha yalla"]))
        self.assertFalse(sts.validate(["1","2","3","4","5"]))
        self.assertFalse(sts.validate(["1.2","3.5","5.5","7.5"]))
        self.assertFalse(sts.validate(["1.5","2","3.5","4","5"]))

    def test_validate_tuple(self):
        '''
        This test suite tests the validate function with various 
        data types in a tuple 
        '''

        # test cases for tuple with int and float data types 
        self.assertTrue(sts.validate((1,2,3,4,5)))
        self.assertTrue(sts.validate((1.2,3.5,5.5,7.5)))
        self.assertTrue(sts.validate((1.5,2,3.5,4,5)))

        # test cases for empty tuples as input 
        self.assertFalse(sts.validate(()))
        self.assertFalse(sts.validate((())))
        self.assertFalse(sts.validate(("")))

        # test cases for tuple of strings as input 
        self.assertFalse(sts.validate("mohitha"))
        self.assertFalse(sts.validate(("mohitha yalla")))
        self.assertFalse(sts.validate(("1","2","3","4","5")))
        self.assertFalse(sts.validate(("1.2","3.5","5.5","7.5")))
        self.assertFalse(sts.validate(("1.5","2","3.5","4","5")))


        # test cases for empty sets as input  
        self.assertFalse(sts.validate({}))
        self.assertFalse(sts.validate({""}))

        # test cases for sets with int and float data types 
        self.assertFalse(sts.validate({1,2,3,4,5}))
        self.assertFalse(sts.validate({1.2,3.5,5.5,7.5}))
        self.assertFalse(sts.validate({1.5,2,3.5,4,5}))

        # test cases for set of strings as input 
        self.assertFalse(sts.validate({"mohitha yalla"}))
        self.assertFalse(sts.validate({"mohitha"}))
        self.assertFalse(sts.validate({"1","2","3","4","5"}))
        self.assertFalse(sts.validate({"1.2","3.5","5.5","7.5"}))
        self.assertFalse(sts.validate({"1.5","2","3.5","4","5"}))
        
# '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    def test_stdeviation_list(self):
        '''
        This test suite tests the std_dev(standard deviation)
        function with various data types in list 
        '''

        # test cases for list with int and float data types 
        self.assertTrue(sts.std_dev([1,2,3,4,5]))
        self.assertTrue(sts.std_dev([1.2,3.5,5.5,7.5]))
        self.assertTrue(sts.std_dev([1.5,2,3.5,4,5]))

        # test case for empty list 
        self.assertFalse(sts.std_dev([]))       

        # test cases for list of strings as input 
        self.assertFalse(sts.std_dev(["mohitha yalla"]))
        self.assertFalse(sts.validate(["1","2","3","4","5"]))

        # problem 
        self.assertEqual(sts.std_dev([1,2,3,3,3]),0.8)
        self.assertNotEqual(sts.std_dev({1,2,3,3,3}),2)
        self.assertNotEqual(sts.std_dev({1,2,3,3,3}),2)

        self.assertAlmostEqual(sts.std_dev([1,2,3,3,3]),0.8)
        self.assertNotAlmostEqual(sts.std_dev([1,2,3,3,3]),0.7)

        self.assertIn(sts.std_dev([1,2,3,3,3]),[0.1,0.4,0.8])
        self.assertIn(sts.std_dev([1,2,3,3,3]),[0.1,0.4,0.8])
        self.assertNotIn(sts.std_dev([1,2,3,3,3]),[0.1,0.4,1.5])
        

        self.assertEqual(sts.std_dev((1,2,3,3,3)),0.8)
        self.assertAlmostEqual(sts.std_dev((1,2,3,3,3)),0.8)
        self.assertNotAlmostEqual(sts.std_dev([1,2,3,3,3]),0.7)
        self.assertNotEqual(sts.std_dev((1,2,3,3,3)),2)

        self.assertFalse(sts.std_dev({1,2,3,3,3}),3)
        
        
    def test_stdeviation_tuple(self):
        '''
        This test suite tests the std_dev(standard deviation)
        function with various data types in tuple 
        '''

        # test cases for tuple with int and float data types 
        self.assertTrue(sts.std_dev((1,2,3,4,5)))
        self.assertTrue(sts.std_dev((1.2,3.5,5.5,7.5)))
        self.assertTrue(sts.std_dev((1.5,2,3.5,4,5)))

        # test cases for empty tuple as input 
        self.assertFalse(sts.std_dev(()))

        # test cases for tuple of strings as input 
        self.assertFalse(sts.std_dev(("1","2","3","4","5")))
        self.assertFalse(sts.std_dev(('sam','john')))

    def test_stdeviation_set(self):
        '''
        This test suite tests the std_dev(standard deviation)
        function with various data types in a set 
        '''

        # test cases for sets with int and float data types 
        self.assertFalse(sts.std_dev({1,2,3,4,5}))
        self.assertFalse(sts.std_dev({1.2,3.5,5.5,7.5}))
        self.assertFalse(sts.std_dev({1.5,2,3.5,4,5}))

        # test cases for sets of strings as input 
        self.assertFalse(sts.std_dev({"mohitha yalla"}))
        self.assertFalse(sts.std_dev({"1","2","3","4","5"}))
        self.assertFalse(sts.std_dev({"1.2","3.5","5.5","7.5"}))
        self.assertFalse(sts.std_dev({"1.5","2","3.5","4","5"}))

# ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    def test_variance_list(self):
        '''
        This test suite tests the variance function with
        various data types in list
        '''

        # test cases for list with int and float data types 
        self.assertTrue(sts.variance([1,2,3,4,5]))
        self.assertTrue(sts.variance([1.2,3.5,5.5,7.5]))
        self.assertTrue(sts.variance([1.5,2,3.5,4,5]))

        # test case for empty list as input 
        self.assertFalse(sts.variance([]))
        # test case for list of strings as input 
        self.assertFalse(sts.variance(["1","2","3","4","5"]))
        self.assertFalse(sts.variance(['sam','john']))

        # test cases to verify the variance of given data set in list 
        self.assertEqual(sts.variance([1,2,3]),0.6666666666666666)
        self.assertAlmostEqual(sts.variance([1,2,3]),0.666666666)
        self.assertNotAlmostEqual(sts.variance([1,2,3]),0.666)

        # test cases to verify the variance of given data set in a tuple
        self.assertEqual(sts.variance((1,2,3)),0.6666666666666666)
        self.assertAlmostEqual(sts.variance((1,2,3)),0.666666666)
        self.assertNotAlmostEqual(sts.variance((1,2,3)),0.666)

    def test_variance_tuple(self):
        '''
        This test suite tests the variance function with 
        various data types in a tuple 
        '''

        # test cases for tuples with int and float data type 
        self.assertTrue(sts.variance((1,2,3,4,5)))
        self.assertTrue(sts.variance((1.2,3.5,5.5,7.5)))
        self.assertTrue(sts.variance((1.5,2,3.5,4,5)))

        # test case for empty tuple as input 
        self.assertFalse(sts.variance(()))
    
        # test cases for tuple of strings as input 
        self.assertFalse(sts.variance(("1","2","3","4","5")))
        self.assertFalse(sts.variance(('sam','john')))

        # test cases to verify the variance of given data set in a tuple
        self.assertEqual(sts.variance((1,2,3)),0.6666666666666666)
        self.assertAlmostEqual(sts.variance((1,2,3)),0.666666666)
        self.assertNotAlmostEqual(sts.variance((1,2,3)),0.666)

    def test_variance_set(self):
        '''
        This test suite tests the variance function with 
        various data types in a set 
        ''' 

        # test cases for sets with int and float data types 
        self.assertFalse(sts.variance({1,2,3,4,5}))
        self.assertFalse(sts.variance({1.2,3.5,5.5,7.5}))
        self.assertFalse(sts.variance({1.5,2,3.5,4,5}))

        # test case for empty set as input 
        self.assertFalse(sts.variance({}))

        # test cases for sets of strings as input 
        self.assertFalse(sts.variance({"mohitha yalla"}))
        self.assertFalse(sts.variance({"1","2","3","4","5"}))
        self.assertFalse(sts.variance({"1.2","3.5","5.5","7.5"}))
        self.assertFalse(sts.variance({"1.5","2","3.5","4","5"}))
        
# ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    def test_mode_list(self):
        '''
        This test suite tests the mode function with various
        data types in list 
        '''

        # test cases for list with int and float data types 
        self.assertTrue(sts.mode([1,2,3,4,5]))
        self.assertTrue(sts.mode([1.2,3.5,5.5,7.5]))
        self.assertTrue(sts.mode([1.5,2,3.5,4,5]))
        self.assertEqual(sts.mode([1.5,2,3.5,4,5]),5)

        # test case for empty list as an input 
        self.assertFalse(sts.mode([]))

        # test cases for list of strings as input 
        self.assertFalse(sts.mode(["1","2","3","4","5"]))
        self.assertFalse(sts.mode(['sam','john']))

        # test cases to verify mode for a given data set in list
        self.assertEqual(sts.mode([1,2,3,3,3]),3)
        self.assertAlmostEqual(sts.mode([1,2,3,3,3]),3.0)
        self.assertNotEqual(sts.mode({1,2,3,3,3}),2)


    def test_mode_tuple(self):
        '''
        This test suite tests the mode function with various 
        data types in tuple
        '''

        # test cases for tuple with int and float data type 
        self.assertTrue(sts.mode((1,2,3,4,5)))
        self.assertTrue(sts.mode((1.2,3.5,5.5,7.5)))
        self.assertTrue(sts.mode((1.5,2,3.5,4,5)))

        # test case for empty tuple as input 
        self.assertFalse(sts.mode(()))
    
        # test cases for tuple of strings as input 
        self.assertFalse(sts.mode(("1","2","3","4","5")))
        self.assertFalse(sts.mode(('sam','john')))

        # test cases to verify mode for given data set in tuple 
        self.assertEqual(sts.mode((1,2,3,3,3)),3)
        self.assertAlmostEqual(sts.mode((1,2,3,3,3)),3)
        self.assertNotEqual(sts.mode((1,2,3,3,3)),5)

    def test_mode_set(self):
        '''
        This test suite tests the mode function with various 
        data types in set
        '''

        # test cases for set with int and float data type 
        self.assertFalse(sts.mode({1,2,3,4,5}))
        self.assertFalse(sts.mode({1.2,3.5,5.5,7.5}))
        self.assertFalse(sts.mode({1.5,2,3.5,4,5}))

        # test case for empty tuple as input 
        self.assertFalse(sts.mode({}))

        # test cases for tuple of strins as input 
        self.assertFalse(sts.mode({"mohitha yalla"}))
        self.assertFalse(sts.mode({"1","2","3","4","5"}))
        self.assertFalse(sts.mode({"1.2","3.5","5.5","7.5"}))
        self.assertFalse(sts.mode({"1.5","2","3.5","4","5"}))

        # test cases to verify mode for given data set in a set  
        self.assertNotEqual(sts.mode({1,2,3,3,3}),3)
        self.assertFalse(sts.mode({1,2,3,3,3}),3)

        
# '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

    def test_mean_list(self):
        '''
        This test suite tests the mode function with various 
        data types in set
        '''

        # test cases for list with int and float data type 
        self.assertTrue(sts.mean([1,2,3,4,5]))
        self.assertTrue(sts.mean([1.2,3.5,5.5,7.5]))
        self.assertTrue(sts.mean([1.5,2,3.5,4,5]))

        self.assertEqual(sts.mean([1,2,3]),2.0)
        self.assertAlmostEqual(sts.mean([1,2,3]),2)
        self.assertNotAlmostEqual(sts.mean([1,2,3]),1.95)

        self.assertEqual(sts.mean((1,2,3)),2.0)
        self.assertAlmostEqual(sts.mean((1,2,3)),2)
        self.assertNotAlmostEqual(sts.mean((1,2,3)),1.95)

        self.assertNotEqual(sts.mean({1,2,3}),2.0)
        self.assertFalse(sts.mean({1,2,3}),2.0)

        self.assertFalse(sts.mean([]))

        self.assertFalse(sts.mean(["1","2","3","4","5"]))


        # mean of tuple
         
        self.assertTrue(sts.mean((1,2,3,4,5)))
        self.assertTrue(sts.mean((1.2,3.5,5.5,7.5)))
        self.assertTrue(sts.mean((1.5,2,3.5,4,5)))

        self.assertFalse(sts.mean(()))
    
        self.assertFalse(sts.mean(("1","2","3","4","5")))


        # mean of set

        self.assertFalse(sts.mean({1,2,3,4,5}))
        self.assertFalse(sts.mean({1.2,3.5,5.5,7.5}))
        self.assertFalse(sts.mean({1.5,2,3.5,4,5}))

        self.assertFalse(sts.mean({}))
        self.assertFalse(sts.mean({"mohitha yalla"}))

        self.assertFalse(sts.mean({"1","2","3","4","5"}))
        self.assertFalse(sts.mean({"1.2","3.5","5.5","7.5"}))
        self.assertFalse(sts.mean({"1.5","2","3.5","4","5"}))

    # ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        # gmean of list

        self.assertTrue(sts.gmean([1,2,3,4,5]))
        self.assertTrue(sts.gmean([1.2,3.5,5.5,7.5]))
        self.assertTrue(sts.gmean([1.5,2,3.5,4,5]))
        self.assertFalse(sts.gmean([]))
        self.assertFalse(sts.gmean(["1","2","3","4","5"]))


        self.assertEqual(sts.gmean([1,2,3]),1.8171205928321397)
        self.assertAlmostEqual(sts.gmean([1,2,3]),1.81712059283)
        self.assertNotEqual(sts.gmean({1,2,3}),1.817120592)

        self.assertEqual(sts.gmean((1,2,3)),1.8171205928321397)
        self.assertAlmostEqual(sts.gmean((1,2,3)),1.81712059283)
        self.assertNotEqual(sts.gmean((1,2,3)),1.817120592)

        self.assertNotEqual(sts.gmean({1,2,3}),1.8171205928321397)
        self.assertFalse(sts.gmean({1,2,3}),1.8171205928321397)


        # gmean of tuple 

        self.assertTrue(sts.gmean((1,2,3,4,5)))
        self.assertTrue(sts.gmean((1.2,3.5,5.5,7.5)))
        self.assertTrue(sts.gmean((1.5,2,3.5,4,5)))

        self.assertFalse(sts.gmean(()))
    
        self.assertFalse(sts.gmean(("1","2","3","4","5")))

        # gmean of set

        self.assertFalse(sts.gmean({}))
        self.assertFalse(sts.gmean({1,2,3,4,5}))
        self.assertFalse(sts.gmean({1.2,3.5,5.5,7.5}))
        self.assertFalse(sts.gmean({1.5,2,3.5,4,5}))
        self.assertFalse(sts.gmean({"mohitha yalla"}))
        self.assertFalse(sts.gmean("mohitha"))
        self.assertFalse(sts.gmean({"1","2","3","4","5"}))
        self.assertFalse(sts.gmean({"1.2","3.5","5.5","7.5"}))
        self.assertFalse(sts.gmean({"1.5","2","3.5","4","5"}))
        
# ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        # hmean of list

        self.assertTrue(sts.hmean([1,2,3,4,5]))
        self.assertTrue(sts.hmean([1.2,3.5,5.5,7.5]))
        self.assertTrue(sts.hmean([1.5,2,3.5,4,5]))
        self.assertFalse(sts.hmean([]))
        self.assertFalse(sts.hmean(["1","2","3","4","5"]))

        self.assertEqual(sts.hmean([1,2,3]),1.6509636244473134)
        self.assertAlmostEqual(sts.hmean([1,2,3]),1.650963624447)
        self.assertNotEqual(sts.hmean({1,2,3}),1.65096)

        self.assertEqual(sts.hmean((1,2,3)),1.6509636244473134)
        self.assertAlmostEqual(sts.hmean((1,2,3)),1.650963624447)
        self.assertNotEqual(sts.hmean((1,2,3)),1.65096)

        self.assertNotEqual(sts.hmean({1,2,3}),1.6509636244)
        self.assertFalse(sts.hmean({1,2,3}),1.6509636244473134)

        
        # hmean of tuple 
        self.assertTrue(sts.hmean((1,2,3,4,5)))
        self.assertTrue(sts.hmean((1.2,3.5,5.5,7.5)))
        self.assertTrue(sts.hmean((1.5,2,3.5,4,5)))
        self.assertFalse(sts.hmean(()))  
        self.assertFalse(sts.hmean(("1","2","3","4","5")))

        # hmean of set

        self.assertFalse(sts.hmean({}))
        self.assertFalse(sts.hmean({1,2,3,4,5}))
        self.assertFalse(sts.hmean({1.2,3.5,5.5,7.5}))
        self.assertFalse(sts.hmean({1.5,2,3.5,4,5}))
        self.assertFalse(sts.hmean({"mohitha yalla"}))
        self.assertFalse(sts.hmean("mohitha"))
        self.assertFalse(sts.hmean({"1","2","3","4","5"}))
        self.assertFalse(sts.hmean({"1.2","3.5","5.5","7.5"}))
        self.assertFalse(sts.hmean({"1.5","2","3.5","4","5"}))

# ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        # frequency of list 


        self.assertTrue(sts.frequency([1,2,3,4,5]))
        self.assertTrue(sts.frequency([1.2,3.5,5.5,7.5]))
        self.assertTrue(sts.frequency([1.5,2,3.5,4,5]))
        self.assertFalse(sts.frequency([]))
        self.assertFalse(sts.frequency(["1","2","3","4","5"]))


        self.assertEqual(sts.frequency([1,2,3,3,3]),{1: 1, 2: 1, 3: 3})
        self.assertAlmostEqual(sts.frequency([1,2,3,3,3]),{1: 1, 2: 1, 3: 3})
        self.assertNotEqual(sts.frequency({1,2,3,3,3}),{1: 2, 2: 2, 3: 2})

        self.assertEqual(sts.frequency((1,2,3,3,3)),{1: 1, 2: 1, 3: 3})
        self.assertAlmostEqual(sts.frequency((1,2,3,3,3)),{1: 1, 2: 1, 3: 3})
        self.assertNotEqual(sts.frequency((1,2,3,3,3)),{1: 2, 2: 2, 3: 2})

        self.assertNotEqual(sts.frequency({1,2,3,3,3}),{1: 1, 2: 1, 3: 3})
        self.assertFalse(sts.frequency({1,2,3,3,3}),{1: 1, 2: 1, 3: 3})


        # frequency of tuple 


        self.assertTrue(sts.frequency((1,2,3,4,5)))
        self.assertTrue(sts.frequency((1.2,3.5,5.5,7.5)))
        self.assertTrue(sts.frequency((1.5,2,3.5,4,5)))

        self.assertFalse(sts.frequency(()))
    
        self.assertFalse(sts.frequency(("1","2","3","4","5")))


        # frequency of set


        self.assertFalse(sts.frequency({}))
        self.assertFalse(sts.frequency({1,2,3,4,5}))
        self.assertFalse(sts.frequency({1.2,3.5,5.5,7.5}))
        self.assertFalse(sts.frequency({1.5,2,3.5,4,5}))
        self.assertFalse(sts.frequency({"mohitha yalla"}))
        self.assertFalse(sts.frequency("mohitha"))
        self.assertFalse(sts.frequency({"1","2","3","4","5"}))
        self.assertFalse(sts.frequency({"1.2","3.5","5.5","7.5"}))
        self.assertFalse(sts.frequency({"1.5","2","3.5","4","5"}))

# '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        # _pdf of list

        self.assertTrue(sts._pdf([1,2,3,4,5]))
        self.assertTrue(sts._pdf([1.2,3.5,5.5,7.5]))
        self.assertTrue(sts._pdf([1.5,2,3.5,4,5]))

        self.assertFalse(sts._pdf([]))

        self.assertFalse(sts._pdf(["1","2","3","4","5"]))



        # _pdf of tuple

        self.assertTrue(sts._pdf((1,2,3,4,5)))
        self.assertTrue(sts._pdf((1.2,3.5,5.5,7.5)))
        self.assertTrue(sts._pdf((1.5,2,3.5,4,5)))

        self.assertFalse(sts._pdf(()))
    
        self.assertFalse(sts._pdf(("1","2","3","4","5")))

        # _pdf of set

        self.assertFalse(sts._pdf({}))
        self.assertFalse(sts._pdf({1,2,3,4,5}))
        self.assertFalse(sts._pdf({1.2,3.5,5.5,7.5}))
        self.assertFalse(sts._pdf({1.5,2,3.5,4,5}))
        self.assertFalse(sts._pdf({"mohitha yalla"}))
        self.assertFalse(sts._pdf("mohitha"))
        self.assertFalse(sts._pdf({"1","2","3","4","5"}))
        self.assertFalse(sts._pdf({"1.2","3.5","5.5","7.5"}))
        self.assertFalse(sts._pdf({"1.5","2","3.5","4","5"}))

# '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        # _zscores of list

        self.assertTrue(sts._zscores([1,2,3,4,5]))
        self.assertTrue(sts._zscores([1.2,3.5,5.5,7.5]))
        self.assertTrue(sts._zscores([1.5,2,3.5,4,5]))

        self.assertFalse(sts._zscores([]))

        self.assertFalse(sts._zscores(["1","2","3","4","5"]))

        # _zscores of tuple

        self.assertTrue(sts._zscores((1,2,3,4,5)))
        self.assertTrue(sts._zscores((1.2,3.5,5.5,7.5)))
        self.assertTrue(sts._zscores((1.5,2,3.5,4,5)))

        self.assertFalse(sts._zscores(()))
    
        self.assertFalse(sts._zscores(("1","2","3","4","5")))

        # _zscores of set

        self.assertFalse(sts._zscores({}))
        self.assertFalse(sts._zscores({1,2,3,4,5}))
        self.assertFalse(sts._zscores({1.2,3.5,5.5,7.5}))
        self.assertFalse(sts._zscores({1.5,2,3.5,4,5}))

        self.assertFalse(sts._zscores({"mohitha yalla"}))
        self.assertFalse(sts._zscores("mohitha"))

        self.assertFalse(sts._zscores({"1","2","3","4","5"}))
        self.assertFalse(sts._zscores({"1.2","3.5","5.5","7.5"}))
        self.assertFalse(sts._zscores({"1.5","2","3.5","4","5"}))


# '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        # _length of list

        self.assertTrue(sts._length([1,2,3,4,5]))
        self.assertTrue(sts._length([1.2,3.5,5.5,7.5]))
        self.assertTrue(sts._length([1.5,2,3.5,4,5]))

        self.assertFalse(sts._length([]))

        self.assertFalse(sts._length(["1","2","3","4","5"]))


        # _length of tuple

        self.assertTrue(sts._length((1,2,3,4,5)))
        self.assertTrue(sts._length((1.2,3.5,5.5,7.5)))
        self.assertTrue(sts._length((1.5,2,3.5,4,5)))

        self.assertFalse(sts._length(()))
    
        self.assertFalse(sts._length(("1","2","3","4","5")))


        # _length of set

        self.assertFalse(sts._length({}))
        self.assertFalse(sts._length({1,2,3,4,5}))
        self.assertFalse(sts._length({1.2,3.5,5.5,7.5}))
        self.assertFalse(sts._length({1.5,2,3.5,4,5}))

        self.assertFalse(sts._length({"mohitha yalla"}))
        self.assertFalse(sts._length("mohitha"))

        self.assertFalse(sts._length({"1","2","3","4","5"}))
        self.assertFalse(sts._length({"1.2","3.5","5.5","7.5"}))
        self.assertFalse(sts._length({"1.5","2","3.5","4","5"}))


# ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        # _sum of list

        self.assertTrue(sts._sum([1,2,3,4,5]))
        self.assertTrue(sts._sum([1.2,3.5,5.5,7.5]))
        self.assertTrue(sts._sum([1.5,2,3.5,4,5]))

        self.assertTrue(sts._sum([1,2,3]))

        self.assertFalse(sts._sum(["1","2","3","4","5"]))

        # _sum of tuple

        self.assertTrue(sts._sum((1,2,3,4,5)))
        self.assertTrue(sts._sum((1.2,3.5,5.5,7.5)))
        self.assertTrue(sts._sum((1.5,2,3.5,4,5)))

        self.assertFalse(sts._sum(()))
    
        self.assertFalse(sts._sum(("1","2","3","4","5")))

        # _sum of set 

        self.assertFalse(sts._sum({}))
        self.assertFalse(sts._sum({1,2,3,4,5}))
        self.assertFalse(sts._sum({1.2,3.5,5.5,7.5}))
        self.assertFalse(sts._sum({1.5,2,3.5,4,5}))
        self.assertFalse(sts._sum({"mohitha yalla"}))
        self.assertFalse(sts._sum("mohitha"))

        self.assertFalse(sts._sum({"1","2","3","4","5"}))
        self.assertFalse(sts._sum({"1.2","3.5","5.5","7.5"}))
        self.assertFalse(sts._sum({"1.5","2","3.5","4","5"}))

# '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        # _product of list

        self.assertTrue(sts._product([1,2,3,4,5]))
        self.assertTrue(sts._product([1.2,3.5,5.5,7.5]))
        self.assertTrue(sts._product([1.5,2,3.5,4,5]))

        self.assertFalse(sts._product([]))

        self.assertFalse(sts._product("mohitha"))

        # _product of tuple 

        self.assertTrue(sts._product((1,2,3,4,5)))
        self.assertTrue(sts._product((1.2,3.5,5.5,7.5)))
        self.assertTrue(sts._product((1.5,2,3.5,4,5)))

        self.assertFalse(sts._product(()))
    
        self.assertFalse(sts._product(("1","2","3","4","5")))

        # _product of set

        self.assertFalse(sts._product({}))
        self.assertFalse(sts._product({1,2,3,4,5}))
        self.assertFalse(sts._product({1.2,3.5,5.5,7.5}))
        self.assertFalse(sts._product({1.5,2,3.5,4,5}))

        self.assertFalse(sts._product({"mohitha yalla"}))
        self.assertFalse(sts._product("mohitha"))

        self.assertFalse(sts._product({"1","2","3","4","5"}))
        self.assertFalse(sts._product({"1.2","3.5","5.5","7.5"}))
        self.assertFalse(sts._product({"1.5","2","3.5","4","5"}))

# ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        #  max of list

        self.assertTrue(sts.max([1,2,3,4,5]))
        self.assertTrue(sts.max([1.2,3.5,5.5,7.5]))
        self.assertTrue(sts.max([1.5,2,3.5,4,5]))

        self.assertFalse(sts.max([]))
        self.assertFalse(sts.max([[]]))
        self.assertFalse(sts.max(""))
        self.assertFalse(sts.max([""]))

        self.assertFalse(sts.max(["mohitha yalla"]))
        self.assertFalse(sts.max(["1","2","3","4","5"]))
        self.assertFalse(sts.max(["1.2","3.5","5.5","7.5"]))
        self.assertFalse(sts.max(["1.5","2","3.5","4","5"]))

        # max tuple 

        self.assertTrue(sts.max((1,2,3,4,5)))
        self.assertTrue(sts.max((1.2,3.5,5.5,7.5)))
        self.assertTrue(sts.max((1.5,2,3.5,4,5)))
        self.assertFalse(sts.max(()))

        self.assertFalse(sts.max((())))
        self.assertFalse(sts.max(("")))

        self.assertFalse(sts.max("mohitha"))
        self.assertFalse(sts.max(("mohitha yalla")))

        self.assertFalse(sts.max(("1","2","3","4","5")))
        self.assertFalse(sts.max(("1.2","3.5","5.5","7.5")))
        self.assertFalse(sts.max(("1.5","2","3.5","4","5")))
        
        # max set

        self.assertFalse(sts.max({}))
        self.assertFalse(sts.max({""}))

        self.assertFalse(sts.max({1,2,3,4,5}))
        self.assertFalse(sts.max({1.2,3.5,5.5,7.5}))
        self.assertFalse(sts.max({1.5,2,3.5,4,5}))

        self.assertFalse(sts.max({"mohitha yalla"}))
        self.assertFalse(sts.max({"mohitha"}))

        self.assertFalse(sts.max({"1","2","3","4","5"}))
        self.assertFalse(sts.max({"1.2","3.5","5.5","7.5"}))
        self.assertFalse(sts.max({"1.5","2","3.5","4","5"}))


 # ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        #  min of list
        self.assertTrue(sts.min([1,2,3,4,5]))
        self.assertTrue(sts.min([1.2,3.5,5.5,7.5]))
        self.assertTrue(sts.min([1.5,2,3.5,4,5]))

        self.assertFalse(sts.min([]))
        self.assertFalse(sts.min([[]]))
        self.assertFalse(sts.min(""))
        self.assertFalse(sts.min([""]))

        self.assertFalse(sts.min(["mohitha yalla"]))
        self.assertFalse(sts.min(["1","2","3","4","5"]))
        self.assertFalse(sts.min(["1.2","3.5","5.5","7.5"]))
        self.assertFalse(sts.min(["1.5","2","3.5","4","5"]))

        # min tuple 
        self.assertTrue(sts.min((1,2,3,4,5)))
        self.assertTrue(sts.min((1.2,3.5,5.5,7.5)))
        self.assertTrue(sts.min((1.5,2,3.5,4,5)))

        self.assertFalse(sts.min(()))
        self.assertFalse(sts.min((())))
        self.assertFalse(sts.min(("")))

        self.assertFalse(sts.min("mohitha"))
        self.assertFalse(sts.min(("mohitha yalla")))

        self.assertFalse(sts.min(("1","2","3","4","5")))
        self.assertFalse(sts.min(("1.2","3.5","5.5","7.5")))
        self.assertFalse(sts.min(("1.5","2","3.5","4","5")))
        
        # min set
        self.assertFalse(sts.min({}))
        self.assertFalse(sts.min({""}))

        self.assertFalse(sts.min({1,2,3,4,5}))
        self.assertFalse(sts.min({1.2,3.5,5.5,7.5}))
        self.assertFalse(sts.min({1.5,2,3.5,4,5}))

        self.assertFalse(sts.min({"mohitha yalla"}))
        self.assertFalse(sts.min({"mohitha"}))

        self.assertFalse(sts.min({"1","2","3","4","5"}))
        self.assertFalse(sts.min({"1.2","3.5","5.5","7.5"}))
        self.assertFalse(sts.min({"1.5","2","3.5","4","5"}))


#  '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        # range of list
        self.assertTrue(sts.range([1,2,3,4,5]))
        self.assertTrue(sts.range([1.2,3.5,5.5,7.5]))
        self.assertTrue(sts.range([1.5,2,3.5,4,5]))

        self.assertFalse(sts.range([]))
        self.assertFalse(sts.range([[]]))
        self.assertFalse(sts.range(""))
        self.assertFalse(sts.range([""]))

        self.assertFalse(sts.range(["mohitha yalla"]))

        self.assertFalse(sts.range(["1","2","3","4","5"]))
        self.assertFalse(sts.range(["1.2","3.5","5.5","7.5"]))
        self.assertFalse(sts.range(["1.5","2","3.5","4","5"]))

        # range tuple 
        self.assertTrue(sts.range((1,2,3,4,5)))
        self.assertTrue(sts.range((1.2,3.5,5.5,7.5)))
        self.assertTrue(sts.range((1.5,2,3.5,4,5)))

        self.assertFalse(sts.range(()))
        self.assertFalse(sts.range((())))
        self.assertFalse(sts.range(("")))
        self.assertFalse(sts.range("mohitha"))

        self.assertFalse(sts.range(("mohitha yalla")))
        self.assertFalse(sts.range(("1","2","3","4","5")))
        self.assertFalse(sts.range(("1.2","3.5","5.5","7.5")))
        self.assertFalse(sts.range(("1.5","2","3.5","4","5")))
        
        # range set
        self.assertFalse(sts.range({}))
        self.assertFalse(sts.range({""}))
        self.assertFalse(sts.range({1,2,3,4,5}))

        self.assertFalse(sts.range({1.2,3.5,5.5,7.5}))
        self.assertFalse(sts.range({1.5,2,3.5,4,5}))
        self.assertFalse(sts.range({"mohitha yalla"}))
        
        self.assertFalse(sts.range({"mohitha"}))
        self.assertFalse(sts.range({"1","2","3","4","5"}))
        self.assertFalse(sts.range({"1.2","3.5","5.5","7.5"}))
        self.assertFalse(sts.range({"1.5","2","3.5","4","5"}))

# '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        # relative frequency
        
        



if __name__=='__main__':
    unittest.main()

# true
# false
# equal
# not equal