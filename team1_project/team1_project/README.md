# Custom made statistics Package

## Packaging Documentation
https://packaging.python.org/tutorials/packaging-projects/

## Usage
This package can be used to implement statistical functions.
This is a **bold** text

## How to write readme
Press ```ctr + shift + p``` in vscode and select Markdown : Open Preview

Use `git status` to list all new or modified files that haven't yet been committed.

Some basic Git commands are:
```
git status
git add
git commit
git push
```

This site was built using [GitHub Pages](https://pages.github.com/).

1. First list item
   - First nested list item
     - Second nested list item

## Instructions for packaging
```
python3 -m pip install --user --upgrade setuptools wheel
```

Now run this command from the same directory where setup.py is located:
```
python3 setup.py sdist bdist_wheel
```

This command should output a lot of text and once completed should generate two files in the dist directory:
`One whl file` and `One gz file`