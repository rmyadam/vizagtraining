import math
import matplotlib.pyplot  as plt
# import seaborn as sns
class Statistics():
    # def __init__(self):
    #     self.product = 1
    #     self.sum = 0

    def validate(self,data):
        if type(data)==int or type(data)==float:
            return True
        elif type(data)==list or type(data)==tuple:
            for x in data:
                if type(x)==int or type(x)==float:
                    pass 
                else:
                    return False
                return True
        else:
            return False 

    def correlation(self,data1,data2):
        if self.validate(data1)==True and self.validate(data2)==True:
            if len(data1)==len(data2):
                meanOfData1 = self.mean(data1)
                meanofData2 = self.mean(data2)
                meanDifference = 0
                for value1,value2 in zip(data1,data2):
                    meanDifference = meanDifference + ((value1-meanOfData1)*(value2-meanofData2))
                
                coefficient = (meanDifference/self.std_dev(data1)*self.std_dev(data2))
                print(coefficient)
                coefficient = round(coefficient/(len(data1)-1),3)
                if coefficient>=-1 and coefficient<=1:
                    return(str(coefficient)+','+"The data is corelated")
                else:
                    return(str(coefficient)+',',"The data is not corelated")
            else:
                return False 
        else:
            return False 

    def quantile(self,param,data):
            if self.validate(data)==True:
                data = sorted(data) 
                if (type(param)==int or type(param(float))):
                    numberOfElements = len(data)
                    param = param/100
                    print(param)
                    element =  int(param*(numberOfElements+1))
                    if element > len(data):
                        return False 
                    else:
                        return data[element]
                else:
                    return False  
            else:
                return False


    def std_dev(self, sequence):
        if self.validate(sequence) == True:
            standardDeviation = math.sqrt(self.variance(sequence))
            return standardDeviation
        else:
            return False

    def coeffVar(self, data):
        return self.std_dev(data)/self.mean(data)

    def variance(self, sequence):
        if self.validate(sequence) == True:
            sumOfDiffromMean = 0
            for ele in sequence:
                sumOfDiffromMean += (ele - self.mean(sequence))**2
            sigmaSquare = sumOfDiffromMean / self._length(sequence)
            return sigmaSquare
        else:
            return False

    def mode(self, sequence):
        if self.validate(sequence) == True:
            freqDict = self.frequency(sequence)
            freqList = sorted(freqDict.items(), key=lambda x: x[1])
            return freqList[-1][0]

    def median(self,sequence):
        """ to find the median of the given sequence"""
        sequence = sorted(sequence)
        count = len(sequence)
        if count % 2 == 0:
            median1 = sequence[count//2]
            median2 = sequence[count//2 - 1]
            median = (median1 + median2)/2
        else:
            median = sequence[count//2]
        return median

    def mean(self, sequence):
        if self.validate(sequence) == True:
            arithmeticMean = self._sum(sequence) / self._length(sequence)
            return arithmeticMean
        else:
            print("Data Type Not Supported.")
            return False

    def gmean(self,sequence):
        if self.validate(sequence) == True:
            geometricMean = self._product(sequence) ** (1/self._length(sequence))
            return geometricMean
        else:
            print("Data Type Not Supported.")
            return False

    def hmean(self, sequence):
        if self.validate(sequence) == True:
            harmonicMean = (self.gmean(sequence)**2)/self.mean(sequence)
            return harmonicMean
        else:
            return False
    
    def relativeFrequency(self, data):
        total_count = sum(self.frequency(data).values())
        relative = {}
        for ele in data:
            relative[ele] = data[ele] / total_count
        print(relative)
        return relative

    def frequency(self, sequence):
        if self.validate(sequence) == True:
            sequence = sorted(sequence)
            freq = {}
            for ele in sequence:
                if ele in freq:
                    freq[ele] += 1
                else:
                    freq[ele] = 1
            return freq

    def _pdf(self, sequence):
        if self.validate(sequence) == True:
            pdf = []
            for ele in sequence:
                pdf.append(math.exp((-ele**2)/2)/math.sqrt(2*math.pi))
            return pdf

    def _zscores(self, sequence):
        if self.validate(sequence) == True:
            zscores = []
            for ele in sequence:
                zscores.append((ele - self.mean(sequence) / self.std_dev(sequence)))
            return zscores

    def gaussian(self, data, mean, sigma):
        if self.validate(data) == True:
            gaus = []
            # mean = self.mean(data)
            # sigma = self.std_dev(data)
            # lowerbound = self.min(data)
            # upperbound = self.max(data)
            # ztransformlb = (lowerbound - mean) / sigma
            # ztransformub = (upperbound - mean) / sigma
            for ele in data:
                gaus.append((1 / math.sqrt(2*math.pi*self.variance(data))) * (math.exp((-ele**2)/2)/math.sqrt(2*math.pi)))
            return gaus


    def _length(self,sequence):
        if self.validate(sequence) == True:
            sequence = list(sequence)
            return len(sequence)

    def _sum(self, sequence):
        if self.validate(sequence) == True:
            sum = 0
            for ele in sequence:
                sum += ele 
            return sum
        else:
            return False

    def _product(self, sequence):
        if self.validate(sequence) == True:
            product = 1
            for ele in sequence:
                product *= ele
            return product
        else:
            return False

    def max(self,data):
        if (self.validate(data)==True):
            return max(data)
        else:
            return False

    def min(self,data):
        if (self.validate(data)==True):
            return min(data)
        else:
            return False 

    def range(self,data):
        if (self.validate(data)==True):
            min = self.min(data)
            max = self.max(data)
            return min,max
        else:
            return False 

    def histogram(self,data,animation=True):
        if animation == True:
            plt.hist(data)
            plt.show()
        else:
            print(self.frequency(data))

    def skewness(self, data):
        skew = (3 * (self.mean(data) - self.median(data))) / self.mode(data)
        return skew
        
    def dist(self,data):
        sns.distplot(data)
        plt.show()
    # def dist(self,data):
    #     lowerbound = self.min(data)
    #     upperbound = self.max(data)
    #     x = lowerbound
    #     increment = (upperbound - lowerbound) / 100
    #     list1 = []
    #     for i in range(100):
    #         x += increment
    #         list1.append(x)
    #     print(list1)
    #     plt.plot(self.gaussian(list1, 0, 1))
    #     plt.show()
        
sts = Statistics()
# print(sts.gmean([1,2,3]))
# print(sts.mean([1,2,3]))
# print(sts.hmean([1,2,3]))
# print(sts.variance([1,2,3]))
# print(sts.variance([1,2,3]))
# print(sts.frequency([1,2,3,3,3]))
# print(sts.mode([1,2,3,3,3]))
# histogram([1,1,4,5,3,3],False)
# print(sts._zscores([26, 33, 65, 28, 34, 55, 25, 44, 50, 36, 26, 37, 43, 62, 35, 38, 45, 32, 28, 34]))
# print(sts._pdf([26, 33, 65, 28, 34, 55, 25, 44, 50, 36, 26, 37, 43, 62, 35, 38, 45, 32, 28, 34]))
data = [0.23,-0.7,-0.74,0.67,-2.64,1.75,-0.24,1.67,0.44,1.18,0.82,0.41,-1.01,-0.76,1.48,0.32,-0.52,-0.9,-0.73,-2.05,-0.99,1.64,0.17,0.09,-2.46,1.21,-1.49,-0.8,-1.14,-1.55,-1.73,0.49,-0.31,-1.19,-0.47,-0.58,-1.44,-0.96,-0.98,0.23,-1.97,0.57,-0.54,0.04,
0.83,0.42,0.22,-0.18,-0.42,-1.82,-0.67,-1.19,-1.05,-1.55,-0.32,1.09,0.82,-0.13,-1.78,0.08,-0.35,1.31,0.12,0.24,0.55,-1.08,0.3,0.57,
1.65,0.96,0.02,-1.4,-0.13,-1.93,-0.66,-0.1,-1.37,-0.07,1.87,1.09,2.4,0.6,-3.05,1.36,-0.68,-0.78,-0.86,0,0.13,0.82,-1.26,-0.06,0.58,
1.13,-0.9,1.27,0.39,-1.77,1.99,0.46]

# histogram(data)
print(sts.gaussian(data, 0, 1))
# sts.dist(data)
# sts.dist([1,1,1,1,2,2,2,2,3,3,4,4,5,5,6,7])
# print(sts.skewness(data))
print(sts.relativeFrequency([1,2,3,3,3]))