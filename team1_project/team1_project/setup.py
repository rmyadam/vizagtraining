import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="stringoperations-Mohitha Yalla", # Replace with your own username
    version="0.0.1",
    author="Mohitha Yalla",
    author_email="myalla@innominds.com",
    description="linkedlist operations packaging",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/Mohitha Yalla/linkedlist",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=['']
