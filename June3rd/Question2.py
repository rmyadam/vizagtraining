'''
This program copies the characters from a file
	reverses it and places in the in another file in same location
	@Rithvik
'''
import sys

def readCharacter(inputFile,generatedPath):
	'''
	This function reads the file, reverses the content in the file and 
	copies in the new file
	param: inputFile(file), generatedPath(Str)
	result: outputFile(file)
	'''
	# reads the content from inputFile 
	for char in inputFile:
		# splits the content and reverses it 
		char=char.split('\n')
		char=char[0]
		char=char[::-1]
		# copies the content into new file with the reversed name 
		outputFile=open(generatedPath,'a')
		outputFile.write(char+'\n')

def generateFileName(path):
	'''
	This function generates the file name by reversing the name of
	inputFile
	param: path(path of inputFile in form of string)
	result: generatedPath(Str)
	'''
	# path is split and file name is reversed
	newPath=path.split('/')
	splitPath=newPath[0]
	newPath=newPath[1]
	newPath=newPath.split('.')
	fileName=newPath[0]
	fileName=fileName[::-1]
	# reverse file name is concatinated to location of file 
	generatedPath=splitPath+'/'+fileName+'.txt'
	return generatedPath 


def reverseFile():
	'''
	This function takes creates an input file copies the content of 
	the file into new file with content reversed and name of file reversed
	'''
	# creation of input file 
	path=sys.argv[1]		
	inputFile=open(path)
	# generates the new path of file in same location as input file 
	generatedPath=generateFileName(path)
	# writes the content of input file in new file 
	readCharacter(inputFile,generatedPath)
	print("The data is added into file successfully")

if __name__=='__main__':
	reverseFile()	