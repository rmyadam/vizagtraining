'''
This program takes the comman line arguements and finds whether 
	file is present or not by using sys module 
	@Rithvik
'''
import sys

def fileCheck():
	'''
	The function checks for the file 
	'''
	try:
		# raises an error if file not found
		file=open(sys.argv[1])
		# if file is found then it prints the content in file
		for line in file:
				print(line.strip())
	except:
		print("file not found")

if __name__=='__main__':
	fileCheck()