'''
This program reads a csv file and adds columns (Total,Percentage) and updates 
	subsequent rows 
	@Rithvik
'''

def updateColumn(row1,addcol):
	'''
	This function adds the columns in first row of csv file 
	param : row1(List), addcol(file object)
	result: writes the added columns in existing csv file 
	'''
	# split the row 
	row1=row1.split(',')
	for colname in ['Total','Percentage']:
		row1.append(colname)
	# converts to string with ',' 
	row1=','.join(row1)
	addcol.write(row1+'\n')

def updateRow(subRows,addcol):
	'''
	This function calculates the total and percentage and append it
	to each row
	param: subRows(List), addcol(file object)
	result : writes the total and percentage in each row 
	'''
	# split each row
	subRows=subRows.split(',')
	sumValue=0
	# adds all elements from index2 and finds sum 
	for j in range(1,len(subRows)):
		marks=int(subRows[j])
		sumValue=sumValue+marks
	# calculates percentage
	percent= (sumValue/300)*100
	result=[sumValue,percent]
	# appends sum  and percentage in each row
	for i in result:
		subRows.append(str(i))
	# convert each row to string 
	subRows=','.join(subRows)
	# write back in same file 
	addcol.write(subRows+'\n')

def fileManipulation():
	'''
	This function appens two columns in the existing file 
	'''
	# create an input file 
	inputFile=open('reasources/csvcheck.txt')
	# read content of file 
	inputRow=inputFile.read()
	inputFile.close()
	# create file object addcol
	addcol=open('reasources/csvcheck.txt','w')
	inputRow=inputRow.split('\n')
	# updates the columns in csv file(reasources/csvcheck.txt)
	updateColumn(inputRow[0],addcol)
	# updates each row in the csv file(reasources/csvcheck.txt) 
	for i in range(1,len(inputRow)):
		row2=inputRow[i]
		updateRow(row2,addcol)
	print("The data added successfully")

if __name__=='__main__':
	fileManipulation()