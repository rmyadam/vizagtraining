'''
This program consists the test cases of linkedlist methods such 
as insert and delete 
@Rithvik
'''

import os
import sys
sys.path.insert(1, os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
# importing pytest for testing 
import pytest
# import the classes LinkedList and Node 
from LinkedListModule.linkedlist import LinkedList,Node


def test_LinkedList():
    '''
    This test suite consists of test cases for methods of linked list
    '''

    # creation of object of linked list 
    listObj=LinkedList()
    # deleting the elements from an empty list returns False 
    assert listObj.delete(0) == False 
    
    # printing the linked list when empty returns false 
    assert listObj.printLinkedList() == False 

    # insert a element in the index from right of list  
    assert listObj.insert(-1,6.5) == True


    # insert an element in index which is positive
    assert listObj.insert(0,1) == True
    assert listObj.insert(1,2) == True
    assert listObj.insert(2,3) == True
    assert listObj.insert(3,4) == True
    assert listObj.insert(4,5) == True

    # test cases for insert for data types other than
    # integer and float data types 
    assert listObj.insert('r',7) == False 
    assert listObj.insert([2,2],[12,3,3,2]) == False 
    assert listObj.insert(0,'sam') == False 
    assert listObj.insert(0,(1,2,2)) == False 
    assert listObj.insert(-1,{1,5,4}) == False

    # test cases for insert for indices which
    # are out of range  
    assert listObj.insert(-10,3) == False 
    assert listObj.insert(10,2) == False 
    assert listObj.insert([],2) == False 
    
    # test case for delete for positive and negative index
    assert listObj.delete(2) == True
    assert listObj.delete(-1) == True
    assert listObj.delete(-1.5) == False 

    # test cases for delete for indices out of range 
    assert listObj.delete(10) == False 
    assert listObj.delete(-11) == False

    # test cases for delete method for data types 
    # other than integer and float 
    assert listObj.delete('') == False
    assert listObj.delete([]) == False
    assert listObj.delete(()) == False
    assert listObj.delete({2}) == False

    # printing the linkedlist with elements in it gives True
    assert listObj.printLinkedList() == True
