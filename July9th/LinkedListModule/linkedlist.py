'''
This programs depicts implementation of linkedlist by object oriented programming 
    and operations on linkedlist
 @Rithvik
'''

class Node:
    '''
    This class is used to create a node of linkedlist
    attributes: self.value and self.next()
    '''
    def __init__(self,value):
        '''
        This method is used to create an object of respective clasc(Node)
        self.value -> value of that particular node
        self.next -> pointer pointing towards the adjacent node
        '''
        self.value = value
        self.next = None

class LinkedList:
    '''
    This class consists of methods which perform operation on linked list
    attributes : self.head,self.length and self.index
    self.head -> points the head of list 
    self.length -> the length of the list
    self.index -> index of the list on which operation has to be performed
    '''

    def __init__(self):
        '''
        This method is used to create object of linkedList class
        '''
        self.head = None
    

    def __insertElement(self,value):
        '''
        This method inserts a new node in the start of linked list 
        '''

        # checking the type of the value given continues if it is integer
        if type(value)==int or type(value)==float: 
        # creation of new node(addnode) of class Node
            addnode = Node(value)
            # asigning the head node to addnode next and making addnode head 
            #   of the linked list 
            addnode.next = self.head
            self.head = addnode
            return True
        # if the value is not integer it returns False
        else:
            return False 
    

    def __insertLastElement(self,value):
        '''
        This method adds new node at the end of linked list 
        '''

        # checking the type of value if integer or float then continues 
        if type(value)==int or type(value)==float:

            # creation of new node of class Node
            addnode = Node(value)
            # assigning head of linked list to temporary variable(temp)
            temp = self.head 
            # iterating through nodes
             
            while True:
                # if list is empty adds node in the front by using self.insertElement
                #   method
                if temp is None:
                    self.__insertElement(value)
                    return
                # else traverses the list till the node whose next is none and points the last node
                #   next to add node 
                if temp.next is None:
                    temp.next = addnode
                    return True
                temp = temp.next 

        # if value is not integer then it returns False 
        else:
            return False 


    
    def insert(self,position,value):
        '''
        This method is used to insert the node in the middle of list according to
            the position given 
        input: value(Int)->value of new node position(Int)-> index at which the node 
                to be placed
        '''

        # check the type of the value if integer or float 
        # check the type of position if integer then it continues  
        if (type(value)==int or type(value)==float) and (type(position)==int):
            # assigning a temporary variable lenght and templength 
            length = 0
            templength = self.head
            # iterating through elements and incrementing the counter(length)
            while True:
                if templength is not None:
                    length +=1
                else:
                    break
                templength = templength.next

            # creation of a new node(addnode) of class Node
            addnode = Node(value)
            # assigning the head of list to temporary variable(temp)
            temp = self.head 

            # if the postion given is negative then it gets the position 
            # from the right side of list
            if position<0 and abs(position)<=length+1 :
                position = (length+1) - abs(position)

            # if given position is out of bounds it throws error 
            elif abs(position) > length:
                print("Enter the position within the length of linked list")
                return False

            # initializing the temporary index to insert the element
            index = 0
            # else iterates through the list 
            while True:
                # when the index is reached position the add node next is assigned to
                #   current node next and current node next is assigned to add node  
                if (position!=0) and temp is not None:
                    index += 1
                    if index == position:
                        addnode.next = temp.next
                        temp.next = addnode
                        return True

                # if position is 0 then id uses self.insertElement method 
                #   to add new node(addnode) in the start of list 
                if position == 0:
                    self.__insertElement(value)
                    return True
                # if the position is in the end of list then it uses 
                # self.insertLastElement method to insert value  
                if temp is None:
                    self.__insertLastElement(value)
                    return True
                temp = temp.next

        # if value is not integer then returns False
        else:
            return False 


    def __deleteElement(self):
        '''
        This method is used to delete the element in the end of linked list
        '''

        # assigning the head of linked list to temporary variable(temp)
        temp = self.head

        # if the list is empty it throws error 
        if temp is  None:
            print("There are no elements to delete")
            return False 

        # else assign the head to second node and making the first node
        #   next equal to None
        self.head = self.head.next
        temp.next = None
        return True
    
    
    def delete(self,position):
        '''
        This method is used to delete a node at given position 
        input: position(Int)->value of new node position(Int)-> index at which the node 
                to be deleted
        '''

        # checking the type of the position if integer then continues 
        if (type(position)==int):
            # assigning temporary variables length and templength
            length = 0
            templength = self.head

            # iterating through elements and incrementing the counter(length)
            while True:
                if templength is not None:
                    length +=1
                else:
                    break
                templength = templength.next

            # if the position is negative 
            if position<0 and abs(position)<=length :
                position = (length) - abs(position)

            # if the position entered is out of bound then it throws error   
            if abs(position) >= length:
                print("Enter the position within the length of linked list")
                return False 

            # assigning head of list and head's next to temporary variables
            #   temp1 and temp2 respectively 
            temp1 = self.head
            temp2 = self.head.next
            index = 0

            # iterating through the list 
            while True:
                # when the index is reached position previous node next is assigned to 
                #   the current node next and the current node next is made None
                if (position!=0) and temp2 is not None:
                    index += 1
                    if index == position:
                        temp1.next = temp2.next
                        temp2.next = None 
                        return True 
                # if given position is 0 the self.deleteElement() is used 
                #    to remove the element from the start 
                if position == 0:
                    self.__deleteElement()
                    return True
                temp1 = temp1.next
                temp2 = temp2.next

        # if the position is not integer it returns False 
        else:
            return False 



    def printLinkedList(self):
        '''
        This method prints all the nodes in the linked list
        '''

        # assigning the head of linked list to temporary variable(temp)
        temp = self.head
        # if there are no nodes in list throws error
        if temp is None:
            print("The linkedList is empty")
            return False 

        # else iterates the list by checking the node next is None or not 
        #   and prints the respective node values till the end 
        while True:
            if temp is None:
                print(temp)
                return True
            print(temp.value, end='->')
            temp = temp.next
