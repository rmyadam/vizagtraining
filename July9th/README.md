# custom made linked list module
This module is a replica of linked list data structure 

##  Usage 
This package is a replica of linked list data structure
It has methods insert and delete

insert(): This method inserts the element in particular index
    syntax: insert(index,value)

delete(): This method deletes the element of particular index
    syntax(): delete(index)


## Instructions for installation
```
pip install "wheel file"

```
This command will install the module successfully 

## Instructions to use module 
In order to use the methods

```
from LinkedListModule import linkedlist.LinkedList
```
for creation of object
```
listObject = LinkedList()
```
This insert method inserts the element in index 1 
```
listObject.insert(1,5)
```
This delete method deletes the element in index 3
```
listObject.delete(3)
```