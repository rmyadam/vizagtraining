'''
This program contains the driver code to use the functions defines in 
    program(Question1.py)
 @Rithvik 
'''
# imports the functions from Qustion1 program 
from Question1 import sumOfNumbers,fibonacci,sumOfRange,factorial
# calls the functions after being imported 
if __name__=="__main__":
    print("The sum of numbers with in the given range is:",sumOfNumbers(5))
    print("The factorial of given number is:",factorial(4))
    print("The element in fibonacci series according to given index is :",fibonacci(4))
    print("The sum of all numbers between the start and end given is : ",sumOfRange(3,6))