'''
This program defines few recursive functions which can be used
    in any other program 
 @Rithvik
'''

def sumOfNumbers(num):
    '''
    This function takes input and adds all the numbers within
        the range of that input
    input: num(Int)
    output: adds all the numbers within num
    '''
    # if num is 1 return 1
    if num == 1:
        return 1
    # else add recursively all the numbers within num 
    return num + sumOfNumbers(num-1)

def factorial(num):
    '''
    This function is used to find the factorial of given input
    input:num(Int)
    output: returns the reccursive multiplied value within range of 
            num
    '''
    # if num is 1 it returns 1
    if num == 1:
        return 1
    # else recursively calls the factorial function to find the
    #   multiplication of all numbers within the num 
    else:
        return num * factorial(num-1) 


def fibonacci(position):
    '''
    This function is used to find the particular number for given 
        respective position in fibonacci series 
    input: position(Int)
    output: returns value of given position 
    '''
    # if position is one then returns 1 as the value is in 1's place
    if position == 1:
        return 0
    # if position is 2 then returns 1 ad the value is in 2's place
    if position == 2:
        return 1
    # else returns recursively the value of addition of before two elements in 
    #   in fibonacci series  
    else: 
        return fibonacci(position-1) + fibonacci(position-2)

def sumOfRange(start,end):
    '''
    This function adds all the numbers between the given start and 
        end value and returns it
    input:start(Int),end(Int)
    output:returns the added value of all the numbers between start and 
            end  
    '''
    # if start is equal to end then it returns end because there are no 
    #   elements left to iterate
    if start == end:
        return end
    # else it adds all the values recursively and returns it  
    else:
        return end + sumOfRange(start,end-1)