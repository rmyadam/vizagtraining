'''
This program exhibits the methods of dictionary 
 @Rithvik
'''

def sortbyKeys(dictionary):
	'''
	This function sorts the dictionary in ascending and descending order according
		to the keys
	param: dictionary(dict)
	result: ascendDict(dict),descendDict(dict) 
	'''
	# initialization of two empty dicts (ascendDict,descendDict)
	ascendDict={}
	descendDict={}
	# sort the elements in dictionary by extracting the list of tuples by method items 
	#	and converting the list of tuples back to a dictionary data type
	ascendDict=dict(sorted(dictionary.items()))
	print("The dictionary in ascending order :",ascendDict)
	descendDict=dict(sorted(dictionary.items(),reverse=True))
	print("The dictionary in descending order is :",descendDict)

def sortbyValue(dictionary):
	'''
	This function sorts the dictionary in ascending and descending order according to 
		the values 
	param: dictionary(dict)
	result: ascendDict(dict),descendDict(dict)
	'''
	# initialization of two empty lists (ascendDict,descendDict)
	ascendDict={}
	descendDict={}
	# sort the dictionary by extracting the list of tuples by method items and 
	#	converting the list of tuples back to dictionary data type 
	ascendDict=dict(sorted(dictionary.items(),key=lambda x:x[1]))
	print("The ascending order is: ",ascendDict)
	descendDict=dict(sorted(dictionary.items(),key=lambda x:x[1],reverse=True))
	print("The descending order is :",descendDict)
	
def genDict(limit):
	'''
	This function creates a dictionary whose keys are in range of the input number given 
		and the values are squares of the respective keys 
	param:limit(Int)
	result: dict1(dict)
	'''
	# initialization of empty dictionary (dict1)
	dict1={}
	# traversing through the range of the input arguement (limit)
	for ele in range(limit):
		# assigning the keys to dict1 and assigning squares of respective keys as values
		dict1[ele]=ele**2	
	return dict1
	print(dict1)

def sumOfDict(inputDictionary):
	'''
	This function find outs the sum of all the keys and values in a dictionary
	param: inputDictionary (dict)
	result: keySum(Int),valueSum(Int)
	'''
	# initialization of two variables keySum and valueSum
	keySum=0
	valueSum=0
	# traversing key and value in the inputDictionary
	for key,value in inputDictionary.items():
		# add particular key and value to keySum and valueSum respectively
		keySum=keySum+key
		valueSum=valueSum+value
	print("The sum of keys",keySum)
	print("The sume of values ",valueSum)


def MergeDicts(dict1,dict2):
	'''
	This function merges two different dictionaries 
	param: dict1(dict),dict2(dict)
	result: dict1(dict)
	'''
	# merge dict2 into dict1 by update method
	dict1.update(dict2)
	print("The merged dictionary is ",dict1)


def duplicate():
	'''
	This method eliminates the duplicate dictonary in a nested dictionary 
	input: studentData
	output: resultData
	'''
	# studentData is the iinput 
	studentData = {'id1': {'name': ['Sara'], 
			'class': ['V'], 
			'subject_integration': ['english, math, science']},
		'id2': 
		{'name': ['David'], 
			'class': ['V'], 
			'subject_integration': ['english, math, science']
		},
		'id3': 
			{'name': ['Sara'], 
			'class': ['V'], 
			'subject_integration': ['english, math, science']
		},
		'id4': 
		{'name': ['Surya'], 
			'class': ['V'],
		},
		}
	# initialization of a empty dictionary(resultDict)
	resultDict={}
	# traversing key and value in studentData 
	for key,value in studentData.items():
		# unique dictionaries from studentData are added into resultDict
		if value not in resultDict.values():
			resultDict[key]=value	
	print(resultDict)	

def dictManipulation():
	'''
	This function takes input and calls other functions 
	'''
	# input dictionaries 
	dict2={1:"rithvik",6:"manideep",5:"deepak"}
	dict1={"Rithvik":10455548,"rohith":10432,"manideep":30434,"deepak":40343}
	# sort the dict2 according to keys 
	sortbyKeys(dict2)
	# sort the dict1 according to values 
	sortbyValue(dict1)
	# merge dict1 and dict2 
	MergeDicts(dict1,dict2)
	# takes input for arguement needed in function genDict(limit)
	limit=int(input("Enter the limit to generate a dictionary"))
	generatedDictionary = genDict(limit)
	# adds and displays the sum of keys and values in generatedDictionary
	sumOfDict(generatedDictionary)
	# this function depicts the elimination of duplicate dictionaries in a nested dictionary
	duplicate()
if __name__=='__main__':
	dictManipulation()