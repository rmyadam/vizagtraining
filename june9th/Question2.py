'''
This program gets the json objects from the link and performs different operations on 
	the json objects
 @Rithvik
'''


import json
import requests

def replaceTaskId(data):
	'''
	This function replaces the id with task id in every json object
	param: data(dict)
	output: replaces id with task id
	'''
	# traverses each element in data
	for record in data:
		# creating a new key task id and assiging the value of id to it
		#	 and deleting the id key
		value=record["id"]
		record["taskid"]=value
		del record["id"]
	print(data)
	print("The replacement of id with taskid is done successfully")

def countUsers(data):
	'''
	This function counts the number of users in the data and prints it 
	param: data(dict)
	output: users(list)
	'''
	# creating an empty list to count the number of users
	users=[]
	# traversing each element in data 
	for record in data:
		# appending all the users in the list(users)
		value=record["userId"]
		if value not in users:
			users.append(value)
	return (users)

def sortTasks(data):
	'''
	This function creates a json object by segregating the users who have completed the
		tasks amd users who didnt 
	input:data (dict)
	output: writes the created json object(json String) into the json file 
	'''
	# initializing the list of completed id's and list of incompleted id's 
	completedIds=[]
	incompletedIds=[]
	# traversing each record 
	for record in data:
		# if the task is completed then the user id is appended to completedIds(list)
		if record["completed"] == True:
			taskid=record["taskid"]
			completedIds.append(taskid)
		# if the task is incompleted the user Id is appended to incompletedIds (list)
		if record["completed"]== False:
			taskid=record["taskid"]
			incompletedIds.append(taskid)
	# The both lists(completedIds,incompletedIds) are set as values for a dictionary
	#	the dictionary is converted to json string and written in file (Tasks.json)
	dictionary={"completed":completedIds,"incompleted":incompletedIds}
	file = open("reasources/Tasks.json","w")
	json.dump(dictionary,file)

def maxTasks(data,users):
	'''
	This function sorts the user ids according to number of taks completed and returns 
		the sorted list
	input: data(dict), users(list) 
	'''
	# initialize the dictionary for sorting
	ascend={}
	# traversing each userid in users(list)
	for userid in range(len(users)+1):
		count=0
		# traversing each record and incrementing the counter
		for record in data:
    		# if the userid is matching with the userid in record anf if task is completed
			#	icrement the count
			if record["userId"]==userid and record["completed"]==True:
				count=count+1
		# append the count to ascend dictionary with respective user id
		ascend[userid]=count
	# convert the ascend(dict) to ascendList(list of tuples) and sort it
	ascendList = dict(sorted(ascend.items(),key=lambda x:x[1]))
	# extract the sorted keys to numberOfUser(list)
	numberOfUser = list(ascendList)
	# add userid: string to each element to form a new list sortedList(list)
	sortedList = ['userid:'+str(i) for i in numberOfUser]
	return (sortedList)
	
def main():
    # get the content from the link provided 
	response = requests.get("https://jsonplaceholder.typicode.com/todos")
	# convert the json objects into the dictionary by loads method 
	data = json.loads(response.text)
	# replaces the id's with task id's
	replaceTaskId(data)
	# gets the no of users in data 
	countOfUsers = int(len(countUsers(data)))
	users=countUsers(data)
	print("The number of users are: ",countOfUsers)
	# The tasks are saperated according to the status of completion of task 
	sortTasks(data)
	print("The ids according to the status of completion of task are segregated and written into Tasks.json file")
	# The userids are sorted according to the number of tasks completed 
	sortedList = maxTasks(data,users)
	print("The sorted list according to number of tasks completed is : ",sortedList)
main()