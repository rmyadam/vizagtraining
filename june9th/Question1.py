'''
This program exhibits the methods of the json library
 @Rithvik
'''
import json 

def jsonLoads():
    '''
    This function converts the json object into the python dictionary
    input: jsonString(Str)
    output: dictionary(Dict)
    '''
    # initialise an input
    jsonString='{"id":"09", "name": "Nitin", "department":"Finance"}'
    print("The type of json object before loads method is : ",type(jsonString))
    # converts json string to dictionary by method loads
    dictionary=json.loads(jsonString)
    # prints the converted dictionary and its type
    print("The converted dictionary is : ",dictionary)
    print("The type of converted dictionary after the method loads :" ,type(dictionary))


def jsonLoadFile():
    '''
    This function gets the content from employee.json file converts the content(json object)
        into python dictioanary and prints it 
    input: employee.json(file)
    output: employeeDictionary(Dict)
    '''
    # open the file and create a file object named as file 
    file=open("reasources/employee.json")
    # convert the content into python dictionary by load method
    employeeDictionary = json.load(file)
    # print the converted content 
    print("The details of employee from employee.json file is: ",employeeDictionary)


def jsonDumpFile():
    '''
    This function makes use of dump method to convert given input dictionary into 
        a json object and write it into employee.json file 
    input:empdetail(Dict)
    output:The empdetail is written in employee.json file in form of json object 
    '''
    # take the input dictionary (empdetail)
    empdetail={"empno":10443,"empname":"Bhaskar","experience":4}
    # open file and create file object 
    file=open("reasources/employee.json","w")
    # convert empdetail and write it in the employee.json file 
    json.dump(empdetail,file)
    print("The employee detail is succesfully deplaoyed in employee.json file")


def jsonDumps():
    '''
    This function makes use of the dumps method to convert a python dictionary to 
        a json object
    input:dictionary(Dict)
    output:jsonstring(Str)
    '''
    # take input as python dictionary
    dictionary={"empno":10447,"empname":"Ravi","experience":3}
    # converts the dictionary into a json string
    jsonString=json.dumps(dictionary)
    # prints the json object and its type before and after coversion 
    print("The type of the dictionary which is given as input: ",type(dictionary))
    print("The coverted json String is : ",jsonString)
    print("The type of the object after the method dumps: ", type(jsonString))


def jsonPrettyPrinting():
    '''
    This method exhibits the pretty printing of json strings by taking python dictionaries 
        as input 
    input:dictionary(dict)
    output:jsonString(Str)
    '''
    # take input in form of a python dictionary  
    dictionary={"person1":{"age":43,"occupation":"farmer","country":"India","crop":["jowar","rice","bajra"]},
    "person2":{"age":"34","occupation":"engineer","country":"USA","Languages":["java","python","c++"]}}
    # uses dumps method to convert dictioanary to json String
    jsonString=json.dumps(dictionary,indent=4)
    print("The json string by pretty printing is : ",jsonString)

def jsonMethods():
    '''
    calls the above defined functions
    '''
    jsonLoads()
    jsonDumps()
    jsonLoadFile()
    jsonDumpFile()

if __name__=='__main__':
    jsonMethods()

