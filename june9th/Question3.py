'''
This program performs json methods on file nbaData
 @Rithvik
'''

import json

def findNba(data2):
	'''
	This function finds out the list of teams that have nba franchise 
	param: data2(dict)
	output: nbaList(list) , withoutNbaList(list) 
	'''
	# creation of two lists (nbaList,withoutNbaList)
	nbaList=[]
	withoutNbaList=[]
	# iteration through the data2 for finding the "isNBAFranchise" key 
	for i in data2:
		country=data2[i]
		for number in country:
			# if value of "isNBAFranchise" value iis True the append the 
			#	respective team into nbaList
			detail=number
			if detail["isNBAFranchise"]==True:
				nbaList.append(detail['fullName'])
			# if value of "isNBAFranchise" is False append the respective
			#	 team into withoutNbaList
			if detail["isNBAFranchise"]==False:
				withoutNbaList.append(detail['fullName'])
	# print both lists
	print("The list of teams with nba : \n",nbaList)
	print("The list of teams without nba: \n",  withoutNbaList)


def League(data2):
	'''
	This method finds a league with maximum number of NBA franchises out of all 
		leagues in data2 
	param: data2(dict)
	output: maxLeague(Str)
	'''
	# creation of a dictionary(leagueDict)
	leagueDict={}
	# iterating it through the data2 till key "isNBAFranchise"
	for i in data2:
		# initialize a counter(count) to count the number of nba franchises in a league 
		count=0
		country=data2[i]
		for number in country:
			detail=number
			# if value of "isNBAFranchise" is True then increment the counter
			if detail["isNBAFranchise"]==True:
				count = count+1
		leagueDict[i]=count
	# prints a league which has maximum value of count
	maxLeague= max(leagueDict, key = lambda k: leagueDict[k])
	print(maxLeague)

def printCityNames(data2):
	'''
	This method prints all the city names 
	param: data2(dict)
	output: prints all the citynames in the data 
	'''
	# initialize the list(cityList) for appending city names 
	cityList = []
	# iterates through the data2 till key "city"
	for i in data2:
		country=data2[i]
		for number in country:
			detail=number
			cityList.append(detail["city"])
	print("The city names are: \n",cityList)

def saperateCityNames(data2):
	'''
	This method segregates the cities according to the status of nba and all star franchise 
	param: data2(dict)
	output: prints two lists one with nba and other with all star franchise 
	'''
	# initialization of two lists nbaCity(with nba franchise) 
	# 	allstarCity (with allstar franchise )
	nbaCity=[]
	allstarCity=[]
	# iterating through data2 till key "isNBAFranchise"
	for i in data2:
		country=data2[i]
		for number in country:
			detail=number
			# if the value of "isNBAFranchise" is True then append the 
			#	respective city to nbaCity
			if detail["isNBAFranchise"]==True:
				value=detail["city"]
				nbaCity.append(value)
			# if the value of "isAllStar" is True then appendthe
			# 	 respective city to allstarCity  
			if detail["isAllStar"]==True:
				value=detail["city"]
				allstarCity.append(value)
	# print both lists
	print("The city with nba are: \n",nbaCity)
	print("The city with all star are : \n", allstarCity) 

def main():
	# open the file and create a file object (file)
	file=open("reasources/nbaData.json")
	# convert the content of the file to a dictionary by using load method
	data=json.load(file)
	data2=data["league"]
	# to find the teams which have NBA
	findNba(data2)
	# to find the league with maximum number of NBA franchises
	League(data2)
	# to print all the city names in data 
	printCityNames(data2)
	# to segregate the cities having NBA or AllStar 
	saperateCityNames(data2)
main()