import math
import matplotlib.pyplot  as plt
class Statistics():
    # def __init__(self):
    #     self.product = 1
    #     self.sum = 0

    def validate(self,data):
        if type(data)==int or type(data)==float:
            return True
        elif type(data)==list or type(data)==tuple:
            for x in data:
                if type(x)==int or type(x)==float:
                    pass 
                else:
                    return False
                return True
        else:
            return False 

    def std_dev(self, sequence):
        if self.validate(sequence) == True:
            return math.sqrt(self.variance)
        else:
            return False

    def variance(self, sequence):
        if self.validate(sequence) == True:
            sumOfDiffromMean = 0
            for ele in sequence:
                sumOfDiffromMean += (ele - self.mean(sequence))**2
            sigmaSquare = sumOfDiffromMean / self._length(sequence)
            return sigmaSquare
        else:
            return False

    def mode(self, sequence):
        if self.validate(sequence) == True:
            freqDict = self.frequency(sequence)
            freqList = sorted(freqDict.items(), key=lambda x: x[1])
            return freqList[-1][0]

    def mean(self, sequence):
        if self.validate(sequence) == True:
            arithmeticMean = self._sum(sequence) / self._length(sequence)
            return arithmeticMean
        else:
            print("Data Type Not Supported.")
            return False

    def gmean(self,sequence):
        if self.validate(sequence) == True:
            geometricMean = self._product(sequence) ** (1/self._length(sequence))
            return geometricMean
        else:
            print("Data Type Not Supported.")
            return False

    def hmean(self, sequence):
        if self.validate(sequence) == True:
            harmonicMean = (self.gmean(sequence)**2)/self.mean(sequence)
            return harmonicMean
        else:
            return False

    def frequency(self, sequence):
        if self.validate(sequence) == True:
            sequence = sorted(sequence)
            freq = {}
            for ele in sequence:
                if ele in freq:
                    freq[ele] += 1
                else:
                    freq[ele] = 1
            return freq

    def _pdf(self, sequence):
        if self.validate(sequence) == True:
            pdf = []
            for ele in sequence:
                

    def _zscores(self, sequence):
        if self.validate(sequence) == True:
            zscores = []
            for ele in sequence:
                zscores.append((ele - self.mean(sequence) / self.std_dev(sequence)))


    def _length(self,sequence):
        if self.validate(sequence) == True:
            sequence = list(sequence)
            return len(sequence)

    def _sum(self, sequence):
        if self.validate(sequence) == True:
            sum = 0
            for ele in sequence:
                sum += ele 
            return sum
        else:
            return False

    def _product(self, sequence):
        if self.validate(sequence) == True:
            product = 1
            for ele in sequence:
                product *= ele
            return product
        else:
            return False

def histogram(data,animation=True):
    if animation == True:
        plt.hist(data)
        plt.show()
    else:
        sts = Statistics()
        print(sts.frequency(data))

sts = Statistics()
print(sts.gmean([1,2,3]))
print(sts.mean([1,2,3]))
print(sts.hmean([1,2,3]))
print(sts.variance([1,2,3]))
print(sts.variance([1,2,3]))
print(sts.frequency([1,2,3,3,3]))
print(sts.mode([1,2,3,3,3]))
histogram([1,1,4,5,3,3],False)