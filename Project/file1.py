import math 
class StatMath:
	def validate(self,data):
		if type(data)==int or type(data)==float:
			return True
		elif type(data)==list or type(data)==tuple:
			for x in data:
			
				if type(x)==int or type(x)==float:
					pass 
				else:
					return False
				return True
		else:
			return False 
	def summation(self,data):
		if (self.validate(data)==True):
			return sum(data)
		else:
			return False 
	def max(self,data):
		if (self.validate(data)==True):
			return max(data)
		else:
			return False
	def min(self,data):
		if (self.validate(data)==True):
			return min(data)
		else:
			return False 
	def range(self,data):
		if (self.validate(data)==True):
			min = self.min(data)
			max = self.max(data)
			return min,max
		else:
			return False 
	
	def percentile(self,param,data):
		if s.validate(data)==True:
			if param in data and (type(param)==int or type(param(float))):
				number1=len(data)
				list1 = [x for x in data if x<param]
				number2 = len(list1)
				percentile = (number2/number1)*100
				return round(percentile,2)
			else:
				return False  
		else:
			return False 

	def quantile(self,param,data):
		if s.validate(data)==True:
			data = sorted(data) 
			if (type(param)==int or type(param(float))):
				numberOfElements = len(data)
				param = param/100
				print(param)
				element =  int(param*(numberOfElements+1))
				if element > len(data):
					return False 
				else:
					return data[element]
			else:
				return False  
		else:
			return False
	def _length(self,sequence):
		if self.validate(sequence) == True:
			sequence = list(sequence)
			return len(sequence)

	def _sum(self, sequence):
		if self.validate(sequence) == True:
			sum = 0
			for ele in sequence:
				sum += ele 
			return sum
		else:
			return False

	def mean(self, sequence):
		if self.validate(sequence) == True:
			arithmeticMean = self._sum(sequence) / self._length(sequence)
			return arithmeticMean
		else:
			print("Data Type Not Supported.")
			return False


	def variance(self, sequence):
		if self.validate(sequence) == True:
			sumOfDiffromMean = 0
			for ele in sequence:
				sumOfDiffromMean += (ele - self.mean(sequence))**2
			sigmaSquare = sumOfDiffromMean / self._length(sequence)
			return sigmaSquare
		else:
			return False


	def std_dev(self, sequence):
		if self.validate(sequence) == True:
			return math.sqrt(self.variance(sequence))
		else:
			return False
	

	def correlation(self,data1,data2):
		if self.validate(data1)==True and self.validate(data2)==True:
			if len(data1)==len(data2):
				meanOfData1 = self.mean(data1)
				meanofData2 = self.mean(data2)
				meanDifference = 0
				for value1,value2 in zip(data1,data2):
					meanDifference = meanDifference + ((value1-meanOfData1)*(value2-meanofData2))
				
				coefficient = (meanDifference/self.std_dev(data1)*self.std_dev(data2))
				print(coefficient)
				coefficient = round(coefficient/(len(data1)-1),3)
				if coefficient>=-1 and coefficient<=1:
					return(str(coefficient)+','+"The data is corelated")
				else:
					return(str(coefficient)+',',"The data is not corelated")
			else:
				return False 
		else:
			return False 



s=StatMath()
# print(s.validate(('42',2,2,3)))
# print(s.max((4,2,3,2,)))
# print(s.min((4,2,3,2,)))
# print(s.range((1,3,3,332,4)))
print(s.percentile(71,[45, 49, 53, 61, 65, 71, 79, 85, 91]))
print(s.quantile(56,(2,6)))
print(s.correlation([3,3,6],[2,3,4]))



