import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="StatisticModule-Rithvik-Myadam", # Replace with your own username
    version="0.0.1",
    author="Subhasis Chand",
    author_email="subhasis.chand@aspireinfolabs.com",
    description="Example for string operations packaging",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/subhasis-chand/stringOperations",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=['numpy']

)