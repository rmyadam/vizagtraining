'''
This program uses object oriented programming to find out emptymatrix,
	transpose,trace,sum,maximum,minimum,zero and unit matrix,rotate matrix 
	@Rithvk
'''

class Matrix:
	'''
	This class contains all the methods of matrix
	'''

	def __init__(self,givenMatrix):
		self.givenMatrix = givenMatrix
		self.rows = len(givenMatrix)
		self.cols = len(givenMatrix[0])
		self.trace = 0
		self.sumValue=0
		self.maxValue=0
		self.minValue=1
	def generateEmptyMatrix(self,noOfRows):
		'''
		This method is used to create empty matrix by taking number of rows
		param: noOfRows(Int)
		result: self.emptyMatrix(list)
		'''
		for row in range(noOfRows):
			# creating empty list for the rows given by list comprehension
			self.emptyMatrix=[[] for i in range(noOfRows)]
		return self.emptyMatrix

	def findTrace(self):
		'''
		This method find the trace of matrix
		result: self.trace(Int)
		'''
		for index in range(len(self.givenMatrix)):
			# adding all the diagonal elements
			self.trace = self.trace + self.givenMatrix[index][index]
		return self.trace

	def findTranspose(self):
		'''
		This method finds the transpose of matrix 
		result: self.transposedMatrix(list)
		'''
		# create a zero matrix with name transposedMatrix
		self.transposedMatrix=[[0 for i in range(self.cols)] for j in range(self.rows)]
		for i in range(self.rows):
			for j in range(self.cols):
				# append the value of rows from givenMatrix as columns in transposedMatrix
				self.transposedMatrix[i][j]=self.givenMatrix[j][i]
		return self.transposedMatrix

	def rotateMatrix(self,direction):
		'''
		This method rotates matrix(given matrix) in either clockwise or
			anticlockwise
		result: self.rotatedMatrix(list)
		'''
		# rotates the matrix anticlockwise
		if direction=="anticlockwise":
			# reverses the rows of transposed matrix
			matrix=self.findTranspose()
			matrix[::-1]
			self.rotatedMatrix=matrix
			return self.rotatedMatrix
		# rotates the matrix anticlockwise
		elif direction=="clockwise":
			# rotates the columns of transposed matrix
			matrix=self.findTranspose()
			print(matrix)
			for i in range(self.rows):
				matrix[i].reverse()
				self.rotatedMatrix=matrix
			return self.rotatedMatrix
	
	def generateZeroMatrix(self,rowno,colno):
		'''
		This method generates the zero matrix for given number of rows 
			and columns
		params: rowno(Int) and colno(Int)
		result: self.zeroMatrix(list)
		'''
		# generating zero matrix by list comprehension
		self.zeroMatrix=[[0 for col in range(colno)] for row in range(rowno)]
		return (self.zeroMatrix)

	def generateUnitMatrix(self,rowno,colno):
		'''
		This method generates the unit matrix for given number of rows 
			and columns
		params: rowno(Int) and colno(Int)
		result: self.unitMatrix(list)
		'''
		# generates unit matrix by list comprehension 
		self.unitMatrix=[[1 for col in range(colno)] for row in range(rowno)]
		return (self.unitMatrix)

	def findSum(self):
		'''
		This method finds the sum of all the elements in a matrix 
		result: self.sumValue(Int)
		'''
		# traversing each row 
		for row in self.givenMatrix:
			# traversing each element and adding it to self.sumValue
			for element in row:
				self.sumValue=self.sumValue+element
		return (self.sumValue)

	def findMaxValue(self):
		'''
		This method finds the maximum value in matrix
		result: self.maxValue(Int) 
		'''
		# traversing each row
		for row in self.givenMatrix:
			max1= max(row)
			# comparing the maximum value of each row with self.maxValue
			if max1>self.maxValue:
				self.maxValue=max1
		return (self.maxValue)

	def findMinValue(self):
		'''
		This method finds the minimum value in matrix
		result : self.minvalue(Int)
		'''
		# traversing each row 
		for row in self.givenMatrix:
			min1=min(row)
			# comparing minimum value of rach row with self.minValue
			if min1<self.minValue:
				self.minValue=min1
		return (self.minValue)


def main():
	'''
	This function creates object of class Matrix and executes methods 
	'''
	# initialization of matrix
	matrix=[[1,2,3],[4,5,6],[7,8,9]]
	# creation of object mathObj
	matrixObj=Matrix(matrix)
	print("Enter the option for suitable operation on matrix: ")
	print("""\t\t1.Trace\n\t\t2.Transpose\n\t\t3.Rotate matrix\n\t\t4.Generate Zero matrix
	\t5.Sum of all elements\n\t\t6.Maximum value in matrix
	\t7.Minimum value in matrix\n\t\t8.Unit matrix\n\t\t9.Generate empty matrix""")
	option=int(input("Choose from the above options:  "))
	# displaying different options for list of functions 
	while(True):
		if option==1:
			traceValue=(matrixObj.findTrace())
			print("The trace Value of object is :",traceValue)
		if option==2:
			transposedMatrix=matrixObj.findTranspose()
			print("The transpose of the matrix is :",transposedMatrix)
		if option==3:
			print("Please enter either clockwise or anticlockwise")
			direction = input("Enter the direction for rotation: ")
			rotatedMatrix=(matrixObj.rotateMatrix(direction))
			print("The rotated matrix is : ",rotatedMatrix)
		if option==4:
			zeroMatrix=(matrixObj.generateZeroMatrix(4,3))
			print("The zeroMatrix is: ",zeroMatrix)
		if option==5:
			sumValue=(matrixObj.findSum())
			print("The calculated sumvalue is :", sumValue)
		if option==6:
			maxValue=matrixObj.findMaxValue()
			print("The maximum value in matrix is :", maxValue)
		if option==7:
			minValue=(matrixObj.findMinValue())
			print("The minimum value in Matrix is :", minValue)
		if option==8:
			unitMatrix=matrixObj.generateUnitMatrix(4,3)
			print("The unit matrix is: ", unitMatrix)
		if option==9:
			rows=int(input("Enter the number of rows for matrix"))
			emptyMatrix=matrixObj.generateEmptyMatrix(rows)
			print("The empty matrix is :",emptyMatrix)
		break
main()