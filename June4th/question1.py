'''
This program perfroms different functions on file by using object oriented
 programming
 @Rithvik

'''

class FileOperator:
	'''
	This class contains the methods for executing functions on file
	'''
	def __init__(self,path):
		'''
		This method creates the object of class FileOperator
		param:path(Str)
		'''
		self.noOfCharacters=0
		self.noOfWords=0
		self.noOfLines=0
		self.path=path
		self.countWord=0
	def readString(self):
		'''
		This method reads the content in the file and displays it
		'''
		self.readWord=open(self.path)
		# traversing the content in the read object and printing it
		for line in self.readWord:
			print(line.strip())
		self.readWord.close()

	def writeString(self,inputLine):
		'''
		This method writes the string in the file
		param: inputLine(Str)
		'''
		writeWord=open(self.path,'w')
		writeWord.write(inputLine)
		print("The string is written successfully")
		writeWord.close()

	def appendString(self,inputLine):
		'''
		This method appends the string into the file
		param: inputLine(Str)
		'''
		appendWord=open(self.path,'a')
		appendWord.write('\n'+inputLine)
		print("The string is appended successfully")
		appendWord.close()

	def Characters(self):
		'''
		This method prints the number of lines,words,characters in
			the file
		'''
		openFile=open(self.path)
		readContent=openFile.read()
		# reads the number of characters
		self.noOfCharacters=len(readContent)
		print("The number of charcters:", self.noOfCharacters)
		lineList=readContent.split('\n')
		# reads the number of lines
		self.noOfLines=len(lineList)
		print("The number of lines are: ", self.noOfLines)
		# traversing each word in list containing lines
		#	finds the number of words
		for sentence in lineList:
			wordList=sentence.split(' ')
			self.noOfWords=self.noOfWords+len(wordList)
		print("The number of words are :",self.noOfWords)

def fileMethods():
		file1=FileOperator('reasources/copyfile.txt')
		print("The menu for file operation is as below")
		print('''\t\t1.Read and display the content\n\t\t2.Write a string
		3.Append a string\n\t\t4.Display the number of lines,words and characters''')
		option=int(input("Enter the option from above menu: "))
		while (True):
			if option==1:
				file1.readString()
			if option==2:
				inputString=input("Enter the string that should be written: ")
				file1.writeString(inputString)
			if option==3:
				inputString=input("Enter the string that should be written: ")
				file1.appendString(inputString)
			if option==4:
				file1.Characters()
			break		
if __name__=='__main__':
	fileMethods()