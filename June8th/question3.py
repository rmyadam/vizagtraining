'''
This program exhibits the different methods of strings and displays the ouput
 @Rithvik
'''
def stringMethods():
    '''
    This function displays various methods of string and implementation of it 
    '''
    inputString="The software engineer works on a computer "
    print("Enter the valid option")
    print('''1.capitalcase\n2.center\n3.count\n4.endwith\n5.expandtabs\n6.find\n7.index\n8.isalnum\n9.isalpha
            \r10.isdigit\n11.islower\n12.isnumeric\n13.isspace\n14.istitle\n15.isupper\n16.join\n17.len\n18.ljust\n19.lower
            \r20.lstrip\n21.maketrans\n22.max\n23.min\n24.replace\n25.rindex\n26.rstrip\n27.split\n28.splitlines
            \r29.swapcase\n30.title\n31.zfill\n32.rfind\n33.rjust\n34.isdecimal\n35.startswith\n36.strip\n37.decoding and encoding''')
    option=int(input("Enter the option for the manipulation "))
    while (True):
        # The string is converted to capital case
        if option==1:
            print("The capital case of string is ",inputString.capitalize())
        # The string is aligned to center according to length parameter and white spaces are filled 
        #   with characters given in filchar parameter
        if option==2:
            length=int(input("enter the length of result string after padding with characters"))
            filchar=input("enter the characters that need to fill in spaces")
            print("The centered string is ",inputString.center(length,filchar))
        # It counts the number of times a substring has occured 
        if option==3:
            subString=input("Enter the substring to be searched for ")
            print("The count for the substring is :",inputString.count(subString))
        # Verifies whether the string ends with input sub string
        if option==4:
            subString=input("Enter the substring to be searched for ")
            print("The string endswith the {0}".format(subString),inputString.endswith(subString))
        if option==5:
        # It sets tabspaces to specified number given in tabsize parameter
            tabsize=int(input("Enter the tabsize"))
            print("The result string after the expand tabs is :",inputString.expandtabs(tabsize=tabsize))
        # Finds the substring between specifies start and end index
        if option==6:
            subString=input("Enter the substring to be searched for ")
            start=int(input("Enter the start index"))
            end=int(input("enter the end index"))
            print("Index for the substring in input string:",inputString.find(subString,start,end))
        # returns the index of substring between specified start and end index
        if option==7:
            subString=input("Enter the substring to be searched for ")
            start=int(input("Enter the start index"))
            end=int(input("enter the end index"))
            print("The index of substring is",inputString.index(subString,start,end))
        # checks whether all charaters in string are numeric
        if option==8:
            print("Check for the alpha numberic :", inputString.isalnum())
        # checks whether all characters in string are alphabets
        if option==9:
            print("check for the all alphabets: ",inputString.isalpha())
        # checks whetehr all characters in string are digits
        if option==10:
            print("Check for all digits in string:",inputString.isdigit())
        # checks whether all the characters in the string are in lower case
        if option==11:
            print("Checks all the characters in string are in lowercase:",inputString.islower())
        # checks whether all the characters in the string are numeric
        if option==12:
            print("Checks for the numbers in string :", inputString.isnumeric())
        # checks whether all the characters are spaces
        if option==13:
            print("prints True if all are white spaces in  string",inputString.isspace())
        # checks whether all the words in string are in title case 
        if option==14:
            print("prints True if all are in tilecase in string:",inputString.istitle())
        # checks whether all the words are in upper case 
        if option==15:
            print("prints True if all are in uppercase in string:",inputString.isupper())
        # Returns the string by joining the saperator 
        if option==16:
            saperator=input("Enter the saperator")
            print("Returns a string with saperator between each character")
            print(saperator.join(inputString))
        # returns the length of the string 
        if option==17:
            print("The length of the string is :",len(inputString))
        # It removes all the spaces from left according to length parameter and fills 
        #   characters as given in fillchar parameter 
        if option==18:
            filchar=input("Enter the character to be filled ")
            length= int(input("enter the required length of result string"))
            fillchar=input("Enter the character to be filled, only one character")
            print("returns the string removing left spaces and replaces spaces with character given")
            print(inputString.ljust(length,fillchar))
        # This converts the input string to lowercase
        if option==19:
            print("prints the string in lowercase",inputString.lower())
        # returns a string by removing the spaces from left
        if option==20:
            print("returns a string removing the white spaces to the left")
            print(inputString.lstrip())
        # Returns the string by replacing them according to table 
        if option==21:
            table=str.maketrans("abcd","efgh")
            print("The translated string is :",inputString.translate(table))
        # Returns the maximum value alphabet of string
        if option==22:
            print("prints the highest value in string: ",max(inputString))
        # Returns the minimum value alphabet of string
        if option==23:
            print("prints the lowest value in string: ",min(inputString))
        # Returns the string replaced by new one with old one 
        if option ==24:
            oldstring=input("Enter the old string")
            newstring=input("Enter the new string for replacement")
            print("The replaced string is :",inputString.replace(oldstring,newstring))
        # Returns the index of substring according to start index and end index
        if option==25:
            subString=input("Enter the substring to be searched for ")
            start=int(input("Enter the start index"))
            end=int(input("enter the end index"))
            print("prints the index of substring from right:",inputString.rindex(subString,start,end))
        # Returns a string that removes the white spaces from right 
        if option==26:
            print("prints the string by removing whitespaces from right :",inputString.rstrip())
        # prints the list by saperating string at saperator and specified maxsplit  
        if option==27:
            saperator=input("Enter the saperator for string")
            maxsplit=int(input("enter the maxsplit of string"))
            print("The splitted string is :",inputString.split(saperator,maxsplit))
        # prints the lines sapersted at \n 
        if option==28:
            print("The string with the split lines is :",inputString.splitlines())
        # prints the string in lower case if string in uppercase and vice versa
        if option==29:
            print("The swapped string is :",inputString.swapcase())
        # prints the string in title case
        if option==30:
            print("The string with titlecase is :", inputString.title())
        # prints the string by padding characters in front accoring to length 
        if option==31:
            length=int(input("Enter the length of result string"))
            print("The result string by zfil is :",inputString.zfill(length))
        # it find the highest index of the substring entered
        if option==32:
            subString=input("Enter the substring to be searched for: ")
            print("The highes index of substring is : ",inputString.rfind(subString))
        # prints the string by right shifting according to length provided and fill the spaces by fillchar
        if option==33:
            length=int(input("Enter the length of the resultant string"))
            fillchar=input("Enter the single character to replace the spaces")
            print("The resultant string by right shifting and character fill is:",inputString.rjust(length,fillchar))
        # checks whether all the characters in the string are decimals 
        if option==34:
            print("prints true if all characters in string are decimal :",inputString.isdecimal())
        # checks whether the string starts with the substring entered 
        if option==35:
            subString=input("Enter the substring you want to search : ")
            print("prinst true if the input string starts with given substring: ",inputString.startswith(subString))
        # prints the string by removing all the spaces from both the ends of the string
        if option==36:
            print("The string is printed by removing all the characters from left and rigth: ",inputString.strip())
        # it encodes the input string and it decodes it 
        if option==37:
            encodedString=inputString.encode()
            print("The encoded string is : ",encodedString)
            decodedString=encodedString.decode()
            print("The decoded String is : ",decodedString)

        break
if __name__=='__main__':
    stringMethods()






