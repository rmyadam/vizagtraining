'''
This program counts the number of ones and zeros in the binary number
 @Rithvik
'''

def count():
    '''
    This function finds the number of ones in binary format of given number
    param: number(Int)
    result: 
    '''
    # take user input and initialising the counts of one's and zero's to 0
    number = int(input("Enter a decimal number to count the number of one's and zero's"))
    oneCount = 0
    zeroCount = 0
    # if the least bit in binary is 1 then incrementing the oneCount else incrementing
    #   zeroCount
    while (number):
        if (number&1) ==1:
            oneCount=oneCount + 1
        else:
            zeroCount=zeroCount + 1
        number = number>>1
    # displaying the oneCount and zeroCount
    print("The number of one's in binary of number is : ",oneCount)
    print("The number of zero's in binary of number is : ",zeroCount)

if __name__=='__main__':
    count()
