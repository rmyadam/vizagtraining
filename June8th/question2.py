'''
This program exhibits all the bitwise operations of two numbers 
 @rithvik
'''
def bitwiseOperations():
    number1 =int(input("Enter the first number for operaation: "))
    number2 = int(input("Enter the second number for operation: "))
    # finds the bitwise ANd operaion and prints result 
    bitwiseAnd = number1 & number2
    print("The bitwise ANDresult of two numbers is : ",bitwiseAnd)
    # finds bitwise OR operation and prints result
    bitwiseOr = number1|number2
    print("The bitwise OR result of two numbers is : ",bitwiseOr)
    # finds the bitwise NOT operation and prints result 
    bitwiseNot1 = ~number1
    bitwiseNot2 = ~number2 
    print("The bitwise NOT of number1 is : ",bitwiseNot1)
    print("The bitwise NOT of number 2 is : ",bitwiseNot2)
    # left shifts the number by 1 and prints result equivalent to multiplication by2
    lshift1 = number1<<1
    lshift2 = number2<<1
    print("The leftshit of the number 1 is : ", lshift1)
    print("The leftshift of the number 2 is : ",lshift2)
    # right shifts the number by 1 and prints result equivalent to division by 2
    rshift1 = number1>>1
    rshift2= number2>>1
    print("The rightshift of number1 is : ",rshift1)
    print("The rightshift of number2 is : ",rshift2)
    # find the bitwise XOR of two numbers and prints it
    bitwiseXor = number1 ^ number2
    print("The bitwise XOR of two numbers is : ",bitwiseXor)  
    
if __name__=='__main__':
    bitwiseOperations()
