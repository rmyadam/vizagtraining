'''
This program uses object oriented programming to find out emptymatrix,
	transpose,trace,sum,maximum,minimum,zero and unit matrix,rotate matrix 
	@Rithvk
'''

class Matrix:
	'''
	This class contains all the methods of matrix
	'''

	def __init__(self,givenMatrix):
		self.givenMatrix = givenMatrix
		self.rows = len(givenMatrix)
		self.cols = len(givenMatrix[0])
		self.trace = 0
		self.sumValue=0
		self.maxValue=0
		self.minValue=1
	def generateEmptyMatrix(self,noOfRows):
		'''
		This method is used to create empty matrix by taking number of rows
		param: noOfRows(Int)
		result: self.emptyMatrix(list)
		'''
		for row in range(noOfRows):
			# creating empty list for the rows given by list comprehension
			self.emptyMatrix=[[] for i in range(noOfRows)]
		return self.emptyMatrix

	def findTrace(self):
		'''
		This method find the trace of matrix
		result: self.trace(Int)
		'''
		for index in range(len(self.givenMatrix)):
			# adding all the diagonal elements
			self.trace = self.trace + self.givenMatrix[index][index]
		return self.trace

	def findTranspose(self):
		'''
		This method finds the transpose of matrix 
		result: self.transposedMatrix(list)
		'''
		# create a zero matrix with name transposedMatrix
		self.transposedMatrix=[[0 for i in range(self.cols)] for j in range(self.rows)]
		for i in range(self.rows):
			for j in range(self.cols):
				# append the value of rows from givenMatrix as columns in transposedMatrix
				self.transposedMatrix[i][j]=self.givenMatrix[j][i]
		return self.transposedMatrix

	def rotateMatrix(self,direction):
		'''
		This method rotates matrix(given matrix) in either clockwise or
			anticlockwise
		result: self.rotatedMatrix(list)
		'''
		# rotates the matrix anticlockwise
		if direction=="anticlockwise":
			# reverses the rows of transposed matrix
			matrix=self.findTranspose()
			matrix[::-1]
			self.rotatedMatrix=matrix
			return self.rotatedMatrix
		# rotates the matrix anticlockwise
		elif direction=="clockwise":
			# rotates the columns of transposed matrix
			matrix=self.findTranspose()
			for i in range(self.rows):
				matrix[i].reverse()
				self.rotatedMatrix=matrix
			return self.rotatedMatrix
	
	def generateZeroMatrix(self,rowno,colno):
		'''
		This method generates the zero matrix for given number of rows 
			and columns
		params: rowno(Int) and colno(Int)
		result: self.zeroMatrix(list)
		'''
		# generating zero matrix by list comprehension
		self.zeroMatrix=[[0 for col in range(colno)] for row in range(rowno)]
		return (self.zeroMatrix)

	def generateUnitMatrix(self,rowno,colno):
		'''
		This method generates the unit matrix for given number of rows 
			and columns
		params: rowno(Int) and colno(Int)
		result: self.unitMatrix(list)
		'''
		# generates unit matrix by list comprehension 
		self.unitMatrix=[[1 for col in range(colno)] for row in range(rowno)]
		return (self.unitMatrix)

	def findSum(self):
		'''
		This method finds the sum of all the elements in a matrix 
		result: self.sumValue(Int)
		'''
		# traversing each row 
		for row in self.givenMatrix:
			# traversing each element and adding it to self.sumValue
			for element in row:
				self.sumValue=self.sumValue+element
		return (self.sumValue)

	def findMaxValue(self):
		'''
		This method finds the maximum value in matrix
		result: self.maxValue(Int) 
		'''
		# traversing each row
		for row in self.givenMatrix:
			max1= max(row)
			# comparing the maximum value of each row with self.maxValue
			if max1>self.maxValue:
				self.maxValue=max1
		return (self.maxValue)

	def findMinValue(self):
		'''
		This method finds the minimum value in matrix
		result : self.minvalue(Int)
		'''
		# traversing each row 
		for row in self.givenMatrix:
			min1=min(row)
			# comparing minimum value of rach row with self.minValue
			if min1<self.minValue:
				self.minValue=min1
		return (self.minValue)

 
	def __add__(self,other):
		'''
		This method overload the add operation by adding all the elements in two matrices
		result: matrix3(List) a matrix with all added elements 
		'''
		# checks whether the rows and columns are equal
		if self.cols==other.cols and self.rows==other.rows:
			# traverses each element in both matrices and adds them
			matrix3=[[0 for i in range(self.cols)] for j in range(self.rows)]
			for row in range(self.rows):
				for col in range(self.cols):
					matrix3[row][col]=self.givenMatrix[row][col]+other.givenMatrix[row][col]
			return matrix3
		else:
			print("addition operation is not possible due to inconsistent number of rows and columns")


	def __sub__(self,other):
		'''
		This method overload the substraction operation by substracting elements of matrix1 by matrix2
		result: matrix3(List) matrix with the elements substracted 
		'''
		# checks whether the rows and columns are equal
		if self.cols==other.cols and self.rows==other.rows:
			# traverses each element in both matrices and substracts them
			matrix3=[[0 for i in range(self.cols)] for j in range(self.rows)]
			for row in range(self.rows):
				for col in range(self.cols):
					matrix3[row][col]=self.givenMatrix[row][col]-other.givenMatrix[row][col]
			return matrix3
		else:
			print("subraction is not possible due to inconsistent number of rows and columns")

	def __truediv__(self,other):
		'''
		This method overloads the true division by performing division between the elements of both matrices
		result : matrix3 (List) matrix with elements divided 
		'''
		# checks whether the rows and columns are equal 
		if self.cols==other.cols and self.rows==other.rows:
			# traverses each element in both matrices and divides them
			matrix3=[[0 for i in range(self.cols)] for j in range(self.rows)]
			for row in range(self.rows):
				for col in range(self.cols):
					matrix3[row][col]=self.givenMatrix[row][col]/other.givenMatrix[row][col]
			return matrix3
		else:
			print("division is not possible due to inconsistent number of rows and columns")
		
	def __floordiv__(self,other):
		'''
		This method overloads the floor division by perforiming floor division between two matrices 
		result: matrix3(List) matrix with elements divided  
		'''
		# checks whether the rows and columns are equal
		if self.cols==other.cols and self.rows==other.rows:
			# traverses eachelement in both matrices and divides them
			matrix3=[[0 for i in range(self.cols)] for j in range(self.rows)]
			for row in range(self.rows):
				for col in range(self.cols):
					matrix3[row][col]=self.givenMatrix[row][col]//other.givenMatrix[row][col]
			return matrix3
		else:
			print("floor division is not possible due to inconsistent number of rows and columns")

	def __mod__(self,other):
		'''
		This method overloads the modulo operation by performing the modulo of elements in matrix1 with 
			with elements in matrix2
		result: matrix3 (List) matrix with remainders 
		'''
		# chechs whether the rows and columns are equal 
		if self.cols==other.cols and self.rows==other.rows:
			# traverses each element and performs modulo of element in matrix1 and element in matrix2
			matrix3=[[0 for i in range(self.cols)] for j in range(self.rows)]
			for row in range(self.rows):
				for col in range(self.cols):
					matrix3[row][col]=self.givenMatrix[row][col] % other.givenMatrix[row][col]
			return matrix3
		else:
			print("modulo is not possible due to inconsistent number of rows and columns")

	def __and__(self,other):
		'''
		This method overloads the bitwise AND operation by performing AND on lelements of two matrices
		result: matrix3(list) matrix with the result of AND operation 
		'''
		# checks whether the rows and columns are equal 
		if self.cols==other.cols and self.rows==other.rows:
			# traverses each element and performs the AND operation 
			matrix3=[[0 for i in range(self.cols)] for j in range(self.rows)]
			for row in range(self.rows):
				for col in range(self.cols):
					matrix3[row][col]=self.givenMatrix[row][col] & other.givenMatrix[row][col]
			return matrix3
		else:
			print("bitwise and is not possible due to inconsistent number of rows and columns")
			
	def __or__(self,other):
		'''
		This method overloads the bitwise OR operation by performing OR operation between elements of two 
			matrices
		result: matrix3(List) matrix with result of OR operation
		'''
		# checks whether the rows and columns are equal 
		if self.cols==other.cols and self.rows==other.rows:
			# traverses the each element and performs the OR operation
			matrix3=[[0 for i in range(self.cols)] for j in range(self.rows)]
			for row in range(self.rows):
				for col in range(self.cols):
					matrix3[row][col]=self.givenMatrix[row][col] | other.givenMatrix[row][col]
			return matrix3
		else:
			print("bitwise and is not possible due to inconsistent number of rows and columns")
			
	def __xor__(self,other):
		'''
		The method overloads the bitwise XOR operation by performing XOR on elements of two matrices 
		result: matrix3(list) matrix with result of XOR operation
		'''
		# checks whether the rows and columns are equal 
		if self.cols==other.cols and self.rows==other.rows:
			# traverses the each element and performs XOR operation 
			matrix3=[[0 for i in range(self.cols)] for j in range(self.rows)]
			for row in range(self.rows):
				for col in range(self.cols):
					matrix3[row][col]=self.givenMatrix[row][col] ^ other.givenMatrix[row][col]
			return matrix3
		else:
			print("bitwise and is not possible due to inconsistent number of rows and columns")

	def __invert__(self):
		'''
		This method overloads bitwise NOT operation by performing the NOT for all the elements 
			 in a matrix
		result: resultMatrix(list) matrix with NOT operated elements
 		'''
		# checks whether the rows and colums are equal
		resultMatrix=[[0 for col in range(self.cols)] for row in range(self.rows)]
		# traverses all elements and performs NOT operation 
		for row in range(self.rows):
			for col in range(self.cols):
				resultMatrix[row][col]= ~self.givenMatrix[row][col]
		return resultMatrix
	
	def __lshift__(self,n):
		'''
		This method performs bitwise left shift of each element in matrix 
		result: resultMatrix(List) matrix with all left shifted elements 
		'''
		resultMatrix=[[0 for col in range(self.cols)] for row in range(self.rows)]
		for row in range(self.rows):
			for col in range(self.cols):
				resultMatrix[row][col]= self.givenMatrix[row][col] << n
		return resultMatrix
		
	def __rshift__(self,n):
		'''
		This method perform bitwise right shift of each element in matrix 
		result:resultMatrix(List) matrix with all right shifted elements 
		'''
		resultMatrix=[[0 for col in range(self.cols)] for row in range(self.rows)]
		for row in range(self.rows):
			for col in range(self.cols):
				resultMatrix[row][col]= self.givenMatrix[row][col] >> n
		return resultMatrix
	
	def __mul__(self,other):
		'''
		This method overloads the multiplication operation by performing multiplication between 
			elements of two matrices 
		result: matrix3 (List) returns a matrix with multiplied numbers   
		'''
		# creation of matrix with matrix1 rows and matrix2 columns 
		matrix3=[[0 for col in range(other.cols)] for row in range(self.rows)]
		# traversing and multiplying the elements
		for row in range(self.rows):
			for col in range(other.cols):
				for k in range(other.rows):
					matrix3[row][col] += self.givenMatrix[row][k] * other.givenMatrix[k][col]
		return matrix3
	
	def determinant(self,matrix, total=0):
		'''
		This method finds the determinant of the given matrix 
		param: matrix(List) it is input matrix and total(Int) determinant value
		result: total(Int) determinant value
		'''
		# store indices in list for row referencing
		indices = list(range(len(matrix)))
		#  when at 2x2 submatrices recursive calls end
		if len(matrix) == 2 and len(matrix[0]) == 2:
			value = matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1]
			return value
		#  define submatrix for focus column and 
		for focusColumn in indices: 
			submatrix = copy_matrix(matrix) 
			submatrix = submatrix[1:] 
			height = len(submatrix) 
	
			for i in range(height): 
				#  for each remaining row of submatrix remove the focus column elements
				submatrix[i] = submatrix[i][0:focusColumn] + submatrix[i][focusColumn+1:] 
	
			sign = (-1) ** (focusColumn % 2) 
			sub_det = self.determinant(submatrix)
			# total all returns from recursion
			total += sign * matrix[0][focusColumn] * sub_det 
	
		return total

	def __gt__(self,other):
		'''
		This method overloads the greater than comparator operator by comparing the 
			determinant of two matrices
		result: retruen True if det of matrix1 is greater than det of matrix2
		'''
		return (self.determinant(self.givenMatrix))>(other.determinant(other.givenMatrix))
			
	def __lt__(self,other):
		'''
		This method overlods the less than comparator operator by comparing the determinant
			of two matrices
		result: returns True if det of matric1 is less than det of matrix2
		'''
		return (self.determinant(self.givenMatrix))<(other.determinant(other.givenMatrix))
	def __eq__(self,other):
		'''
		This method overloads the equal to comparator operator by comparing the dets
			of two matrices
		result: returns True if det of matrix1 is equal to det of matrix2
		'''
		return (self.determinant(self.givenMatrix))==(other.determinant(other.givenMatrix))

def main():
	'''
	This function creates object of class Matrix and executes methods 
	'''
	# initialization of matrix
	matrix=[[1,2,3],[4,5,6],[7,8,9]]
	# creation of object mathObj
	matrixObj=Matrix(matrix)
	print("Enter the option for suitable operation on matrix: ")
	print("""\t\t1.Trace\n\t\t2.Transpose\n\t\t3.Rotate matrix\n\t\t4.Generate Zero matrix
	\t5.Sum of all elements\n\t\t6.Maximum value in matrix
	\t7.Minimum value in matrix\n\t\t8.Unit matrix\n\t\t9.Generate empty matrix\n\t\t10.Operation Overloading""")
	option=int(input("Choose from the above options:  "))
	# displaying different options for list of functions 
	while(True):
		if option==1:
			traceValue=(matrixObj.findTrace())
			print("The trace Value of object is :",traceValue)
		if option==2:
			transposedMatrix=matrixObj.findTranspose()
			print("The transpose of the matrix is :",transposedMatrix)
		if option==3:
			print("Please enter either clockwise or anticlockwise")
			direction = input("Enter the direction for rotation: ")
			rotatedMatrix=(matrixObj.rotateMatrix(direction))
			print("The rotated matrix is : ",rotatedMatrix)
		if option==4:
			zeroMatrix=(matrixObj.generateZeroMatrix(4,3))
			print("The zeroMatrix is: ",zeroMatrix)
		if option==5:
			sumValue=(matrixObj.findSum())
			print("The calculated sumvalue is :", sumValue)
		if option==6:
			maxValue=matrixObj.findMaxValue()
			print("The maximum value in matrix is :", maxValue)
		if option==7:
			minValue=(matrixObj.findMinValue())
			print("The minimum value in Matrix is :", minValue)
		if option==8:
			unitMatrix=matrixObj.generateUnitMatrix(4,3)
			print("The unit matrix is: ", unitMatrix)
		if option==9:
			rows=int(input("Enter the number of rows for matrix"))
			emptyMatrix=matrixObj.generateEmptyMatrix(rows)
			print("The empty matrix is :",emptyMatrix)
		if option == 10:
			print("The operations which are overloaded are")
			print("Enter the below option for operation overloading")
			print('''1.additon\n2.substraction\n3.division\n4.multiplication\n5.floordivision\n6.modulo
				\r7.bitwise And\n8.bitwiseOr\n9.bitwise NOT\n10.right shift\n11.left shift: ''')
			choice=int(input("Please enter the valid option: "))
			# The methods are executed according to the choice given
			while(True):
				if choice==1:
					matrix1=Matrix([[0,1],[0,1]])
					matrix2=Matrix([[3,5],[5,6]])
					resultmatrix=(matrix1+matrix2)
					print("The added matrix is : ",resultmatrix)
				if choice==2:
					matrix1=Matrix([[0,1],[0,1]])
					matrix2=Matrix([[3,5],[5,6]])
					resultmatrix=(matrix1-matrix2)
					print("The substraced matrix is :",resultmatrix)
				if choice==3:
					matrix1=Matrix([[0,1],[0,1]])
					matrix2=Matrix([[3,5],[5,6]])
					resultmatrix=(matrix1 / matrix2)
					print("The true division of matrices is :",resultmatrix)
				if choice==4:
					matrix1=Matrix([[0,1],[0,1]])
					matrix2=Matrix([[3,5],[5,6]])
					resultmatrix=(matrix1 * matrix2)
					print("The multiplied matrix is: ",resultmatrix)
				if choice==5:
					matrix1=Matrix([[0,1],[0,1]])
					matrix2=Matrix([[3,5],[5,6]])
					resultmatrix=(matrix1//matrix2)
					print("The floor division of matrices is :",resultmatrix)
				if choice==6:
					matrix1=Matrix([[0,1],[0,1]])
					matrix2=Matrix([[3,5],[5,6]])
					resultmatrix=(matrix1 % matrix2)
					print("The modulo of two matrices is :",resultmatrix)
				if choice==7:
					matrix1=Matrix([[0,1],[0,1]])
					matrix2=Matrix([[3,5],[5,6]])
					resultmatrix=(matrix1 & matrix2)
					print("The bitwise AND of two matrices is :",resultmatrix)
				if choice==8:
					matrix1=Matrix([[0,1],[0,1]])
					matrix2=Matrix([[3,5],[5,6]])
					resultmatrix=(matrix1|matrix2)
					print("The bitwise OR of two matrices is :",resultmatrix)
				if choice==9:
					matrix1=Matrix([[3,5],[5,6]])
					resultmatrix=(~matrix1)
					print("The NOT of given matrix is: ", resultmatrix)
				if choice==10:
					matrix1=Matrix([[3,5],[5,6]])
					resultmatrix = (matrix1>>1)
					print("The NOT of given matrix is :",resultmatrix)
				if choice==11:
					matrix1=Matrix([[3,5],[5,6]])
					resultmatrix = (matrix1<<1)
					print("The NOT of given matrix is :",resultmatrix)
				if choice==12:
					matrix1=Matrix([[0,1],[0,1]])
					matrix2=Matrix([[3,5],[5,6]])
					result=(matrix1 > matrix2)
					print("prints true if det of matrix1 is greater than matrix2",result)
				if choice==12:
					matrix1=Matrix([[0,1],[0,1]])
					matrix2=Matrix([[3,5],[5,6]])
					result=(matrix1 < matrix2)
					print("prints true if det of matrix1 is lesser than matrix2",result)
				if choice==13:
					matrix1=Matrix([[0,1],[0,1]])
					matrix2=Matrix([[3,5],[5,6]])
					result=(matrix1 < matrix2)
					print("Prints True if det of matrix 1 is equal to det of matrix2:",result)
				break	
					
		break
main()