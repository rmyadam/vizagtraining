'''
This programs finds the number of pairs that will sum up to a
 given value
 @Rithvik
'''
def getList():
	'''
	This function takes the range of list,the elements of list
	and creates a list(List1)
	'''
	list1=[]
	listRange=int(input("Enter the range for list"))
	# traversing for given length of list and appending elements
	for i in range(listRange):
		element=int(input("Enter the input element"))
		list1.append(element)
	return list1

def findCount(newList,pivot):
	'''
	This function find the number of times the pivot element has occured 
	and returns the pivot count
	param: newList(list),pivot(Int)
	result: pivotCount(Int)
	'''
	pivotCount=newList.count(pivot)
	return pivotCount

def pairCount():
	'''
	This function finds the number of pairs in a list which sum up 
	to a given value
	'''
	# creation of the list by user input
	inputList=getList()
	# initializing the sum value 
	sumValue=int(input("Enter the sumValue"))
	elementCount=0
	# traversing each element in inputList
	for number in inputList:
		# creation of new list without number in list 
		newList=[]
		pivot = sumValue-number
		newList=inputList[:number]+inputList[number+1:]
		# get pivot count 
		pivotCount=findCount(newList,pivot)
		# adding pivotCount cumulatively for elementCount to get
		#	to get the number of pairs
		elementCount=elementCount+pivotCount
	print("The number of pairs: ",elementCount)
if __name__=='__main__':
	pairCount()	     