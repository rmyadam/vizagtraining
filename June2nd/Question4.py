'''
This program adds the numbers given intwo lists and returns the 
	added value
	@Rithvik
'''


def findPadLength(list1,list2):
	'''
	This function compares the length of two lists and 
		returns the padding length
	'''

	if list1>list2:
		return (len(list1)-len(list2))
	else:
		return (len(list2)-len(list1))

def zeroPadding(list,padLength):
	'''
	This function pads the zeros in the front of the 
		smaller list and returns the zero padded list
	'''
	#inittialising the empty list
	padList=[]
	# padding the smaller list with zeros
	for j in range(padLength):
		padList.append(0)
	list=padList+list
	return list

def addition(param1,param2,carry):
	'''
	This function performs addition of three parameters and 
		returns the sum and carry
	param: param1(Int), param2(Int), carry(int)
	returns sumValue(Str), carry(Int)
	'''
	result=param1+param2+carry
	# if reuslt is greater than 9 initialise carry to zero 
	if result<=9:
		sumValue=str(result)
		carry=0
	# if not the assign leftmost bit to sumValue and remaining 
	#	bit to carry
	else:
		result=str(result)
		sumValue=(result[1])
		carry=int(result[0])
	return [sumValue,carry]

def addList():
	'''
	This function adds the number in given two lists and returns 
		prints the sum
	'''
	# taking input for two lists
	list1=[1,2,3,2]
	list2=[5,3,5,3,6,7]
	# find padding length
	padLength=findPadLength(list1,list2)
	# padding the smaller list
	if list1>list2:
		list2=zeroPadding(list2,padLength)
	elif list2>list1:
		list1=zeroPadding(list1,padLength)
	# initialising sum and carry
	sumValue=''
	carry=0
	# reversing the lists 
	list1=list1[::-1]
	list2=list2[::-1]
	# adding the each bit 
	for i in range(len(list1)):
		addResult=addition(list1[i],list2[i],carry)
		sumValue=sumValue+(addResult[0])
		carry=addResult[1]
	carry = str(carry)		
	sumValue=carry+sumValue[::-1]
	# displaying the result of the sum
	print("The sum of two numbers in lists is :",int(sumValue))

if __name__=='__main__':
	addList()
