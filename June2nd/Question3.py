'''
This program finds the number of ways to reach the corner element
	of noOfRows*noOfRows matrix
'''
import random
# Description: The function returns the random values between 0 and 1
# params: noOfRows and noOfCols are number of rows and number of columns respectively
# result : random number between 0 and 1
def generate(noOfRows,noOfCols):
	for i in range(noOfRows*noOfCols):
		# uses random module to generate the random number
		randomnumber=random.randint(0,1)
		yield randomnumber


# Description: The function initialises matrix and returns matrix with column 1 as zeros
# params: noOfRows and noOfCols are the number of rows and no of columns 
# result: matrix of dimension noOfRows*noOfCols
def initialiseMatrix(noOfRows,noOfCols):
	# initialize matrix using list comprehension
	Matrix=[[1 for i in range(noOfCols)] for j in range(noOfRows)]
	# get each value from generator function
	value=generate(noOfRows,noOfCols)
	# append the values into matrix
	for row in range(len(Matrix)):
		for column in range(len(Matrix[0])):
			element=next(value)
			Matrix[row][column] = element
	# initialize first column as ones
	Matrix[0][0]=1
	Matrix[noOfRows-1][noOfCols-1]=1
	for row in range(len(Matrix)):
		Matrix[row][0]=1
	return Matrix

# Description: The function changes the row 1 and returns matrix
# params: Matrix (the initialized matrix is taken)
# result: row1Matrix(the matrix with changed firstrow)
def subMatrix1(Matrix):
	# traverses the elements and checks the element is zero 
	#	if element is zero no updation 
	for col in range(1,len(Matrix[0])):
		if Matrix[0][col]!=0:
			# if not zero adds the previous element
			Matrix[0][col]=1
	row1Matrix=Matrix
	return row1Matrix


# Description: The function converts subsequent rows and returns the 
#				resultMatrix 
# params: row1Matrix(matrix with row1 changed)
# result: resultMatrix(matrix with all subsequent elements changed)  
def subMatrix2(row1Matrix):
	for row in range(1,len(row1Matrix)):
		for col in range(1,len(row1Matrix[0])):
			# checks the 
			if row1Matrix[row][col]!=0:
				row1Matrix[row][col]= row1Matrix[row][col-1]+row1Matrix[row-1][col]
	resultMatrix=row1Matrix
	return resultMatrix

# Description: takes the input for number of rows and columns,creates matrix
#				returns resultant matrix 
def pathOfMatrix():
	noOfRows=int(input("Enter the value of number of rows: "))
	noOfCols=int(input("Enter the value of number of columns: "))
	# initializes the matrix with given rows and columns
	Matrix=initialiseMatrix(noOfRows,noOfCols)
	print("The generated matrix is : ", Matrix)
	# updates the first row of the matrix 
	row1Matrix=subMatrix1(Matrix)
	# updates the subsequent rows of matrix 
	resultMatrix=subMatrix2(row1Matrix)
	print("The result matrix is ",resultMatrix)
	print("The number of ways to travel the corner element is :",resultMatrix[noOfRows-1][noOfCols-1])

if __name__=='__main__':
	pathOfMatrix()	