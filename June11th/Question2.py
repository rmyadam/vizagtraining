import time
'''
This program prints a statement after a stipulated time which is taken 
	as input from user
 @ Rithvik
'''

def getInput():
	'''
	This program takes the input from user for stipulated time and converts them 
		into seconds 
	input: days(Int),hours(Int),minutes(Int),seconds(Int)
	output:seconds(Int)
	'''
	while(True):
		try: # takes input from user and throws error if its not integer   
			days = int(input("Enter the number of days:  ")) 
			hours = int(input("Enter the number of hours:  "))
			minutes = int(input("Enter the number of minutes:  "))
			seconds = int(input("Enter the number of seconds:  "))
			break
		except: 
			print("Please enter the integer values")
	# converts all the values taken into seconds 
	days = days * 86400
	hours = hours * 3600
	minutes= minutes * 60
	seconds = seconds+ minutes + hours + days
	return seconds 


def timer():
	'''
	This program prints the statement after the stipulated time 
	'''
	# gets number of seconds from the getInput() function 
	seconds = getInput()
	# uses time module to hold the print statement for the given input seconds
	time.sleep(seconds)
	print("Time is up!! ")

if __name__=="__main__":
	timer()