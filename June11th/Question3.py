'''
This program generates password according to user input and validates password gievn by 
	the user
 @Rithvik
'''
# import random and string module for use in program 
import random
import string


class PasswordValidation:
	'''
	This class contains the methods that generate the password and validate the user password 
	'''
	def __init__(self):
		'''
		This constructor method creates the object of class (PasswordValidation)
		'''
		pass
		
	def generate(self,length=10,**kwargs):
		'''
		This method generates password according to custom values given by users
		param :length(default parameter,type:Int),*kwargs(keyword parameter,type:Int)
		result: self.generatedPassword (Str)
		'''
		# assigning values from parameters to the variables 
		self.length = length
		# assigning the values from kwargs to respective variables
		self.noOfUppercase = kwargs['param1']
		self.noOfLowercase = kwargs['param2']
		self.noOfNumeric = kwargs['param3']
		self.noOfSpecial = kwargs['param4']
		# initialization of string self.password
		self.password = ''
		# generating characters according to specifies number and forming a string 
		self.lowercaseLetters = ''.join([random.choice(string.ascii_lowercase) for element in range(self.noOfLowercase)])
		self.uppercaseLetters = ''.join([random.choice(string.ascii_uppercase) for element in range(self.noOfUppercase)])
		self.numericCharacters = ''.join([random.choice(string.digits) for element in range(self.noOfNumeric)])
		self.specialCharacters = ''.join([random.choice(string.punctuation) for element in range(self.noOfSpecial)])
		# concatinating all the generated charcaters to the self.password string
		self.password = self.lowercaseLetters + self.uppercaseLetters +self.numericCharacters +self.specialCharacters
		# if the created password is less in length than required length it generates random alphabets and 
		#	concatinates to the self.password
		if (len(self.password)<self.length):
			remainLength = self.length - len(self.password)
			self.remainingLetters = ''.join([random.choice(string.ascii_letters) for element in range(remainLength)]) 
			self.password = self.password + self.remainingLetters
		# The self.password is shuffled using sample method and formed a list(self.generatedPasswordList)
		self.generatedPasswordList = random.sample(self.password,len(self.password))
		# self.generatedPassword List is converted to string self.generatedPassword string and returned
		self.generatedPassword = ''.join(self.generatedPasswordList)
		print("The generated password is : ",self.generatedPassword)
		return self.generatedPassword

	def passwordChecker(self,userPassword):
		'''
		This method validates the userPassword taken from user 
		param: userPassword(Str)
		result: self.percentage(Int)
		'''
		# initialization of empty lists to append the characters according to type 
		self.alpha = []
		self.digits = []
		self.punctuation = []
		# iterating each character in userPassword(string) 
		for character in userPassword:
			# if character is alphabet append into self.alpha(list)
			if character in string.ascii_letters:
				self.alpha.append(character)
			# if character is a digit append into self.digits(list)
			if character.isdigit() == True:
				self.digits.append(character)
			# if character is a special character append into self.punctuation(list)
			if character in string.punctuation:
				self.punctuation.append(character)

	def percentageCalculator(self):
		'''
		This method assigns the percentage for the passsword gievn by user
		'''
		# initializing self.percentage(Int) as 20 
		self.percentage = 20
		# if the length of self.alpha(list) >6 assigns the percentage as 20
		if len(self.alpha) > 6:
			self.alphaPercentage = 20
		# else calculates the percentage by taking length of self.alpha and dividing it by 20  
		else: 
			self.alphaPercentage = (len(self.alpha)/20) * 100
		# if the length of self.alpha(list) >5 assigns the percentage as 20
		if len(self.digits) > 5:
			self.digitPercentage = 10
		# else calculates the percentage by taking length of self.digits and dividing it by 20  
		else:	
			self.digitPercentage = (len(self.digits)/20)*100
		# if the length of self.punctuation(list) >5 assigns the percentage as 20 
		if len(self.punctuation) >5:
			self.punctuationPercentage = 10
		# else calculates the percentage by taking length of self.digits and dividing it by 20  
		else:
			self.punctuationPercentage = (len(self.punctuation)/20)*100
		# adds all the individual percentages to self.percentage and prints it  
		self.percentage = self.percentage + self.alphaPercentage + self.digitPercentage + self.punctuationPercentage
		print("The strength of given password is : ",str(self.percentage)+'%')
		
def passwordMethods():
	'''
	This function takes requirements of a password from user and generates a password and
		gives the strength of password which is given by user 
	input: length(Int),uppercase(Int),lowercase(Int),special(Int)
	output:self.generatedPassword(Str),self.percentage(Str)
	'''
	# takes input for length and number of uppercase letters,lowercase letters,special characters,digits
	while(True):
		try:
			length = int(input("Enter the length of the password: "))
			uppercase= int(input("Enter the no on uppercase letters: "))
			lowercase = int(input("Enter the no of lowercase letters: "))
			numeric = int(input("Enter the no of numeric values: "))
			special = int(input("Enter the no of special characters: "))
			# handling the error if the input values for uppercase,lowercase,digits and special
			#	is greater than given input length 
			if uppercase+lowercase+numeric+special > length:
				raise NameError
			break
		# handles error if input values are not integers 
		except ValueError:
			print("Enter the integer values only")
		except NameError:
			print("Enter the values in specified length")
			return(0)
	# creation of object of class:PasswordValidation
	counterSign = PasswordValidation()
	# calling the method generate defined in class(PasswordValidation) to generate password
	counterSign.generate(length,param1=uppercase,param2=lowercase,param3=numeric,param4=special)
	# take input from user to find the strength of given password 
	userPassword = input("Enter the password that needs to be verified: ")
	# calling methods (passwordChecker and passworddCalculator) defined in class(PasswordValidation)
	# 	to find strength of given userPassword
	counterSign.passwordChecker(userPassword)
	counterSign.percentageCalculator()
if __name__=='__main__':
	passwordMethods()