'''
This program exhibits all the set methods of two sets 
 @Rithvik
'''

def setMethods(set1,set2):
    '''
    This program defines the methods which perform operation on two sets
    param: set1(set),set2(set)
    result: prints the calculated set method outputs
    '''
    # adds and element to set1 and prints it
    set1.add(13)
    print("The set1 is added with element 13 : ",set1)
    # copies the set1 into resultset and prints it 
    resultset=set1.copy()
    print("The set1 is copied into a resultset : ",resultset)
    # prints if the elements in set1 are True
    all(set1)
    print("prints True if elements in set1 are True: ",(all(set1)))
    # prints if atleast one element in set1 is True 
    any(set1)
    print("prints True if atleast one element in set1 are True: ",(any(set1)))
    # returns tuple of indexes and respective values into enumeratedList
    #   converts into list of tuples and prints it 
    enumeratedList = enumerate(set1)
    print("returns a tuple consisting of index and item : ",list(enumeratedList))
    # removes the element 13 and prints the set1
    set1.discard(13)
    print("prints the set1 by discarding the element 13: ",set1)
    # finds the union of two sets (set1 and set2) prints the set containing elements of both sets 
    unionSet=set1.union(set2)
    print("The union of set1 and set2 is : ",unionSet)
    # finds the difference of set1 and set2(prints only elements that present in set1 )
    differenceSet = set1.difference(set2)
    print("The difference of set1 and set2 is : ",differenceSet)
    # finds the intersection of two sets (set1 and set2) prints the set having common elements
    #   from the both sets
    intersectionSet = set1.intersection(set2)
    print("The intersection of set1 and set2 is : ",intersectionSet)
    # uses update method to add the elements of set2 to set1
    set1.update(set2)
    print("The update of set1 with set2 is : ",set1)
    # finds whether all the elements of set2 are present in set1 and prints True if
    #   condition is satisfied,prints false if not 
    print("prints True if set2 is subset of set1: ",set2.issubset(set1))
    # finds that all the elements of set2 are present in set1 and prints True if 
    #   condition is satisfied , prints false if not
    print("prints True if set1 is super set of set2: ",set1.issuperset(set2))
    # set 1 gets updated to set difference of set1 and set2
    set1.difference_update(set2)
    print("The difference update of set1 with set2 is : ",set1)
    # finds whether set1 is disjoint of set2 (the elements in set1 are not present in set2)
    #   prints True if the condition is satisfied
    print("prints True if set1 is disjoint of set2: ",set1.isdisjoint(set2))

if __name__=='__main__':
    # initializing the input (set1 and set2)
    set1 = {1,2,3,4,5,6,7,13}
    set2 = {1,2,4,5,3}
    setMethods(set1,set2)