'''
This program demonstrates the palindrome checking of a given input 
only if it is a list,tuple or string 
@Rithvik 
'''

def ispalindrome(inputString):
    '''
    This fucntion checks for the palindrome condition 
    input: inputString (Str)
    output: returns True if satisfied or returns False otherwise
    '''
    # checks whether the given input is list,string or tuple if it is not
    # then returns false 
    if type(inputString)!=str and type(inputString)!=list and type(inputString)!=tuple:
        return False
    # if the given input is a string then remove the spaces and convert
    # each letter to lower case 
    if len(inputString)==0:
        return False  
    if type(inputString)==str:
        inputString = inputString.replace(" ","")
        inputString = inputString.lower()
    # reversing the obtained input by slicing 
    rev = inputString[::-1] 
    # check for palindrome condition 
    if rev==inputString:
        return True 
    else:
        return False  