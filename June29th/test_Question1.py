'''
This program tests the palindrome program for various inputs
@Rithvik 
'''

# importing the pytest and ispalindrome function from Question1 file  
import pytest 
from Question1 import ispalindrome


def test_ispalindrome():
    '''
    This function tests ispalindrome function for various inputs
    inputs: lists,tuples,strings 
    '''

    '''
    The lists are given as input to ispalindrome function 
    '''
    # list containing numbers that satisfy palindrome condition  
    assert ispalindrome([1,2,3,2,1]) == True
    assert ispalindrome([1,2,3,4,4,4,4]) == False 
    # empty list is not a palindrome  
    assert ispalindrome([]) == False 
    # a list containing special charaters 
    assert ispalindrome(['#','&','*','&','#']) == True
    # list oif strings 
    assert ispalindrome(["Sam",'John']) == False
    # list of lists as input 
    assert ispalindrome([[1,2],[3,3],[1,2]]) == True
    assert ispalindrome([[1,4],[3,6],[5,2]]) == False
    # list of dictionaries   
    assert ispalindrome([{1:2},{3:4},{3:2}]) == False
    # list of tuples  
    assert ispalindrome([(5,6),(3,3),(5,6)]) == True

    '''
    The various strings are gievn as input to ispalindrome function 
    '''
    # a string containing single word which can satisfy palindrome condition
    assert ispalindrome("bob") == True
    # string containing words when all letters put togeather satisfy palindrome
    # condition 
    assert ispalindrome("never odd or even") == True
    # string with different case as input 
    assert ispalindrome("Bob") == True
    # string with words and different case 
    assert ispalindrome("TOO hot to Hoot") == True
    # string consisting numbers 
    assert ispalindrome("12321") == True
    # string with words as input
    assert ispalindrome("this is a computer") == False 
    # empty string does not satify palindrome condition 
    assert ispalindrome('') == False
    # a string consisting of special characters which satisfy palindrome condition  
    assert ispalindrome('###&&**&&##') == False

    '''
    The various tuples are given as input to palindrome function 
    '''
    # a tuple with numbers as input 
    assert ispalindrome((1,2,5,34,4,34)) == False
    # a tuple with numbers which satisfy the palindrome condition 
    assert ispalindrome((1,2,3,3,2,1)) == True
    # empty tuple does not satisfy the palindrome condition 
    assert ispalindrome(()) == False
    # a tuple consisting the strings which satisfy palindrome condition  
    assert ispalindrome('###&&**&&##') == False
    # a tuple consisting of different strings which satisfy the
    # palindrome condition  
    assert ispalindrome(('rithvik','ravi','rithvik')) == True
    # a tuple of strings as input 
    assert ispalindrome(('rithvik','ravi')) == False
    # tuple consisting of lists which satisfy the palindrome condition 
    assert ispalindrome(([5,6],[3,3],[5,6])) == True
    
    '''
    The sets are given as input to palindrome function 
    '''
    # the palindrome condition is not met by sets as the insertion
    # order is not preserved 
    assert ispalindrome({1,2,3,3,2,1}) == False 
    assert ispalindrome({"sam","john","david"}) == False 