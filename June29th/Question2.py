'''
This program depicts few methods of linked list such as adding the node,
inserting the node and deleting the node from the front 
@Rithvik 
'''

class Node:
    '''
    This class is used to create a node of linkedlist
    attributes: self.value and self.next()
    '''
    def __init__(self,value):
        '''
        This method is used to create an object of respective clasc(Node)
        self.value -> value of that particular node
        self.next -> pointer pointing towards the adjacent node
        '''
        self.value = value
        self.next = None
            

class LinkedList:
    '''
    This class consists of methods which perform operation on linked list
    attributes : self.head,self.length and self.index
    self.head -> points the head of list 
    self.length -> the length of the list
    self.index -> index of the list on which operation has to be performed
    '''

    def __init__(self):
        '''
        This method is used to create object of linkedList class
        '''
        self.head = None
        self.length = 0
        self.index = 0
    
    def addNode(self,value):
        '''
        This method is used to add new nodes to the linkedList
        input: value (Int)
        output: forms a linked list by appending new nodes  
        '''
        # cheks if the given value is integer if not it returns False 
        if type(value)==str or type(value)==list or type(value)==tuple or type(value)==set:
            return False 
        newNode = Node(value)
        # assigning the first node to head 
        if self.head is None: 
            self.head = newNode
            return True
        # aggining the head of list to temporary variable(temp)
        temp = self.head
        # assiging each node next to respective adjacent node 
        while True:
            if temp.next is None:
                temp.next = newNode
                return True
            temp = temp.next
    def printLinkedList(self):
        '''
        This method prints all the nodes in the linked list and returns False if the the 
        linked list is empty 
        '''
        # assigning the head of linked list to temporary variable(temp)
        temp = self.head
        # if there are no nodes in list throws error
        if temp is None:
            print("The linkedList is empty")
            return False
        # else iterates the list by checking the node next is None or not 
        #   and prints the respective node values till the end 
        while True:
            if temp is None:
                print(temp)
                return True
            print(temp.value, end='->')
            temp = temp.next

    def insertElement(self,value):
        '''
        This method inserts a new node in the start of linked list and returns True if the 
        value given is integer 
        '''
        # checks if the value given is integer if not then returns False 
        if type(value)==str or type(value)==list or type(value)==tuple or type(value)==set:
            return False 
        else:
            # creation of new node(addnode) of class Node
            addnode = Node(value)
            # asigning the head node to addnode next and making addnode head 
            #   of the linked list 
            addnode.next = self.head
            self.head = addnode
            return True 
    
    def deleteElement(self):
        '''
        This method is used to delete the element in the end of linked list if the 
        elements are present in list it returns True or else it returns False 
        '''
        # assigning the head of linked list to temporary variable(temp)
        temp = self.head
        # if the list is empty it throws error and returns False 
        if temp is  None:
            print("There are no elements to delete")
            return False 
        # else assign the head to second node and making the first node
        #   next equal to None
        self.head = self.head.next
        temp.next = None
        return True