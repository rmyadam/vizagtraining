'''
This program tests few LinkedList methods such as addition,deletion,insertion 
of nodes
@Rithvik  
'''

# importing pytest module to test 
import pytest 
# import the LinkedList and Node classes from another file 
from Question2 import LinkedList,Node

def test_linkedList():
    '''
    This function contains the test cases of LinkedList with various inputs 
    ''' 
    # creation of object of LinkedList class 
    ll = LinkedList()
    # deleting the element from linkedlist when it is empty returns False  
    assert ll.deleteElement() == False
    # printing the empty linked list wil return False 
    assert ll.printLinkedList() == False 
    # adding the tuple throws error hence returns False 
    assert ll.addNode((1,2,3)) == False
    # adding the empty list gives the error hence returns False  
    assert ll.addNode([]) == False 
    # adding a set throws error hence returns False
    assert ll.addNode({1,2,3}) == False 
    # adding new nodes to create linked list 
    assert ll.addNode(5) == True
    assert ll.addNode(6) == True
    assert ll.addNode(8) == True
    assert ll.addNode(10) == True
    # printing the linked list after adding nodes gives True 
    assert ll.printLinkedList() == True
    # insertting elements into the linked list 
    assert ll.insertElement(3) == True
    # inserting a list will return False
    assert ll.insertElement([4,3,4,7,6]) == False
    assert ll.insertElement([]) == False 
    # inserting the tuple will return False 
    assert ll.insertElement((1,2,3)) == False
    # inserting a string will return False  
    assert ll.insertElement('') == False 
    # inserting the string will return False 
    assert ll.insertElement(("rajesh")) == False
    # inserting the set will return False 
    assert ll.insertElement({4,3,4,7,6}) == False
    # deleting the element is possible after the linked list has elements
    assert ll.deleteElement() == True