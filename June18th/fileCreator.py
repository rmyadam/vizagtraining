'''
This program is used to generate 100 dummy files in specified folder
@Rithvik 
''' 
import random,string

def dummyFiles():
    '''
    This function is used to generate 100 dummy files 
    '''
    # iterting through the range of 101  
    for i in range(1,101):
        # creation of file(file object) by open method in write mode and 
        #   closing the respective file, assigning random name for all created files 
        name = ''.join(random.choices(string.ascii_letters,k=5))
        file =open('reasources/myfolder/'+name+'.txt',"w")
        file.close()
    print("The files are created successfully")
if __name__=='__main__':
    dummyFiles()
