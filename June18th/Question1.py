'''
This program renames all the files that are present in specified location
@Rithvik
'''
# import os and glob module for furthur use in program
import os,glob
import sys,argparse

def renameFiles():
    '''
    This function renames all the names of files in a particular location
    input:path(Str),prefix(Str),postfix(Int),extension(Str)
    output: renames all file names in myfolder directory
    '''
    # input is taken from commanline arguements by arguement parser 
    # creation of parser object 
    parser = argparse.ArgumentParser()
    # adding all the areguements to the parser object 
    parser.add_argument('--path',type=str,default='reasources/myfolder',
                        help ='The path at which the files to be renamed')
    parser.add_argument('--prefix', type=str, default='myfile',
                        help='The prefix for renaming files')
    parser.add_argument('--postfix', type=int, default=5,
                        help='The postfix for renaming files')
    parser.add_argument('--extension', type=str, default='.csv',
                        help='The extension for renaming files')
    # creating a namespace of argparse (args) and getting all the required arguements in it 
    args = parser.parse_args()
    # accessing each individual arguement from args
    path = args.path
    prefix = args.prefix
    postfix = args.postfix
    extension = args.extension
    # opens the folder(myfolder) location of created files
    os.chdir(r"C:\Users\IMVIZAG\Desktop\vizagtraining\\"+path)
    # creating an iterator with all files having .txt extension
    directory = glob.glob('*.txt')
    # iterating the range of postfix and files in directory simultaneously
    for file,i in zip((directory),range(postfix,postfix+101)):
        # gets the path of files in directory and creates new path(newFileLocation)
        #   checks if file named as newFileName already exists or not 
        path = (os.path.abspath(file))
        fileLocation = os.path.dirname(path)
        # the new name which has to be replaced for created files 
        newFileName = "\\"+prefix+str(i)+extension
        # the location at which the files are to be renamed
        newFileLocation = (fileLocation+newFileName)
        exist = os.path.exists(newFileLocation)
        # if the file named as newFileName exists gives options either to overwrite
        #   the file with the same name of other name 
        if exist == True:
            # displays file name which is already existing 
            print("The which already exists is : ",newFileName)
            print("1.rename the file with other name \n2.Leave the file as it is ")
            option = int(input("Enter the option "))
            # option 1 is for renaming file name with other name
            if option ==1:
                existFileName = input("Enter the name of file for renaming")
                existFileLocation = fileLocation +"/"+ existFileName + extension
                os.rename(path,existFileLocation)
            # option 2 is leaving the file as it is
            else:
                pass
        # renames the files to newFilename 
        else:
            os.rename(path,newFileLocation)
    print("The files are renamed successfully")
if __name__=='__main__':
    renameFiles()
