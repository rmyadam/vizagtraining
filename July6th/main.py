'''
This program contains the functions that need to be tested using 
unittest module 
@Rithvik 
'''

# import logging module 
import logging


def add(value1, value2):
    '''
    This function adds the two values which are taken as input and 
    returns the result 
    input: value1(Int),value2(Int)
    output: result(Int)
    '''
    # verify whether the input values are integers if not returns false 
    if type(value1)==str or type(value2)==str:
        return False 
    elif type(value1)==list or type(value2)==list:
        return False 
    elif type(value1)==tuple or type(value2)==tuple:
        return False 
    # if the both values(value1&value2) are integers returns added value
    result = value1 + value2 
    return result 

def divide(value1,value2):
    '''
    This function divides the first input with second input if 
    both of them are integers if not it raises the Type Error 
    '''
    # checks both inputs(value1&value2) are integers and divides them
    if type(value1)==int and type(value2)==int:
        return value1/value2
    # if not integers raises Type Error 
    else:
        raise TypeError


def returnSequence(param):
    '''
    This function simply returns the input which is parsed
    input: param(any sequence)
    '''
    return param


def checkList(param):
    '''
    This function checks whether the input is present in predefined list
    if present it returns True or else returns None
    input: param(Int)
    '''
    # checking the input is present in list and returning True
    if param in [1,23,4,2,4,34,53,34]:
        logging.debug("The list is checked")
        return True 
    # if not present then returns None
    else:
        return None