'''
The program test the functions from another file(main.py) by 
unittest module
@rithvik
'''

# import unitest module and all functions from main.py file  
import unittest
from main import* 

class Test_add(unittest.TestCase):
    '''
    This class imports the TestCase from unittest module
    This class contains the test suits which tests the different functions 
    '''
    
    def test_integer(self):
        '''
        This method contains the test cases which only compare the integers 
        '''
        # assertEqual compares the result of add function and the given input 
        # if both are equal then passses this test case 
        self.assertEqual(add(1,2),3)
        self.assertEqual(add([1,2,323],[2,323]),False,"The lists cannot be added")
        self.assertEqual(add("rithvik","myadam"),False,"The strings cannot be added")
        # assertNotEqual compares the result of add function and the given input 
        self.assertNotEqual(add(21,31),add(41,10))
        # this test case passes if parameter1 is greate or equal to parameter2
        self.assertGreaterEqual(add(2,3),add(4,1))
        # this test case passes if parameter1 is greater than paramter2
        self.assertGreater(add(2,9),add(1,2))
        # test case passes if parameter1 is less than parameter2
        self.assertLess(add(3,4),add(21,5))
        # test case passes if parameter1 is less than parameter2
        self.assertLessEqual(add(6,4),add(5,5))
        # test case passes if both parameters are equal 
        self.assertIs(add(12,13),add(14,11))
        # test case passes if both parameters are not equal 
        self.assertIsNot(add(2,5),add(9,43))
        # test case passes if both parameters are equal it only works for float values 
        self.assertAlmostEqual(add(2.4534,5.7328343),add(5.323,2.8632343))
        # test case passes if both parameters are not equal it only works for float values 
        self.assertNotAlmostEqual(add(1.23243,5.45454),add(4.4564,8.545))
        # test case passes if parameter1 is not present in parameter2
    def test_sequence(self):
        '''
        This method contains the test cases which tests the sequences 
        '''
        self.assertNotIn(10,returnSequence([1,2,34,2,4,2,4]))
        # test case passes if parameter1 is present in parameter2
        self.assertIn(1,returnSequence([1,7,4,54,3]))
        # test case passes if parameter retruns None
        self.assertIsNone(checkList(12))
        # testcase passes if parameter returns other than None 
        self.assertIsNotNone(checkList(2))
        # test case passes if both parameters(strings) are equal
        self.assertMultiLineEqual(returnSequence('''software company'''),returnSequence('''software company'''))
        # test case passes if both parameters(sequences) are equal 
        self.assertSequenceEqual(returnSequence([1,2,3,4,]),returnSequence([1,2,3,4]))
        # test case passes if both parameters(sets) are equal 
        self.assertSetEqual(returnSequence({3,6,8,4,9}),returnSequence({3,9,8,6,4}))
        # test case passes if both parameters(tuples) are equal 
        self.assertTupleEqual(returnSequence((4,7,5,2)),returnSequence((4,7,5,2)))
        # test case passes if both parameters(dicts) are equal 
        self.assertDictEqual(returnSequence({1:2,4:6}),returnSequence({1:2,4:6}))
        # test case passes if both parameters(sequences) have the same elements even with
        # different order 
        self.assertCountEqual(returnSequence([1,1,2,2,4]),returnSequence([1,2,1,2,4]))
        # test case passes if parameter2(dict) is present in parameter2(dict) 
        self.assertDictContainsSubset(returnSequence({1:2}),returnSequence({1:2,5:6}))

    def test_errors(self):
        '''
        This method contains the assertions which test the errors reaised by the 
        functions 
        '''
        # test case passes if the functon given as parameter raises an error 
        self.assertRaises(ZeroDivisionError,divide,5,0)
        self.assertRaises(TypeError,divide,'sam','John')
        # test case passes if regex pattern is not found in the sequence 
        self.assertRaisesRegex(TypeError,r's',returnSequence("Sam"))
        # test case passes if parameter2 (a regex pattern) is found in parameter1(string)
        self.assertRegex(returnSequence("Innominds is a company"),r'[a-z][A-z]')
        # test case passes if parameter2 (*as regex pattern) is not found in parameter1(string)
        self.assertNotRegex(returnSequence("Innominds is a company"),r'[0-9]')
        # test case passes if the parameter2 throws respective warning given in parameter1 
        self.assertWarns(DeprecationWarning,self.assertEquals(add(3,6),add(5,4)))
        # test case passes if parameter2 returns a log message according to parameter2
        self.assertLogs(checkList([1,2,21,3,2]),'DEBUG')
        # test case passes if parameter1 is a object of parameter2 
        self.assertIsInstance(returnSequence([1,2,3,2,23]),list)
        # test case passes if parameter1 is not a object of parameter2 
        self.assertNotIsInstance(returnSequence([1,3,2,4,3]),str)
        
if __name__=='__main__':
    unittest.main()