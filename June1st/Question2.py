'''
This program finds the roots of given quadratic equation
	@Rithvik

'''

def determinant(a,b,c):
	'''
	This function finds the determinant by taking three parameters
	param : a (Int),b(Int),c(Int)
	'''
	# finds the determinant using the formula 
	x=(b**2)-(4*a*c)
	x= pow(x,0.5)
	# returns the obtained result 
	return x

def validate(equation):
	'''
	This function validates the input given equation 
	param: equation(Str) in string format
	'''
	# removes the spaces in the given equation
	equation=equation.lstrip()
	# splits the equation based on the character 'x'
	equation=(equation.split('x')) 
	a,b,c=equation[0],equation[1],equation[2]
	# removes spaces and assigns value to a
	a=a.lstrip()
	a=a.split('x')
	a=str(a[0])
	# removes spaces and assigns value to b
	b.lstrip()
	b=b.split('2')
	b=str(b[1])
	# removes spaces and assigns value to c
	c=c.lstrip()
	c=str(c)
	# validates if the assigned values are alphabets
	if (a.isalpha()==True):
		print("invalid input of value a")
	elif (b.isalpha()==True):
		print("invalid input of value b")
	elif (c.isalpha()==True):
		print("invalid input of value c")
	else:
		a=int(a)
		b=int(b)
		c=int(c)
		result=[a,b,c]
		return result

def calculateroot():
	'''
	This function takes the input of a quadratic equation and 
	finds the roots of equation
	'''
	equation=input("enter the quadratic equation ")
	# assign the output of validation function to the variable(validationOutput)
	validationOutput=(validate(equation))
	a=validationOutput[0]
	b=validationOutput[1]
	c=validationOutput[2]
	root1=(-b+determinant(a,b,c))/(2*a)
	root2=(-b-determinant(a,b,c))/(2*a)
	# displays the roots
	print("root1 of the equation is : ",root1)
	print("root2 of the equation is : ",root2)
if __name__=='__main__':
	calculateroot()
