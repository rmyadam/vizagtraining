'''
This program finds the transpose and rotates the given matrix into clockwise 
	or anticlockwose 
	@Rithvik
'''


def transpose(matrix):
	'''
	This function gets the transpose of the given matrix
	param : matrix(List)
	'''
	# initialising an empty matrix of dimensions of input matrix
	resultMatrix=[[],[],[]]
	# traversing in range of lenght of row 
	for i in range(len(matrix)):
		# traversing in range of lenght of column
		for j in range(len(matrix[0])):
			# appending the elements into the empty matrix
			resultMatrix[i].append(matrix[j][i])
	return resultMatrix

def rotate(matrix,direction):
	'''
	This function rotates the given input matrix into clockwise 
		or anticlockwise based on the input given 
	param : matrix(List), direction(Int)
	'''
	# finds the transpose of matrix and assigns result to variable(transposedMatrix)
	transposedMatrix=(transpose(matrix))
	# checks the value of variable(direction)
	if direction ==1 :
		# reverses the transposed matrix
		resultMatrix=transposedMatrix[::-1]
		return(resultMatrix)
	else:
		# initialise an empty matrix
		resultMatrix=[[],[],[]]
		# reverses the lists in the transposed matrix
		for j in range(len(transposedMatrix)):
			row=transposedMatrix[j]
			row=row[::-1]
			transposedMatrix[j]=row
		resultMatrix=transposedMatrix
		return resultMatrix


def rotateMatrix():
	'''
	The function finds the transpose of given matrix and rotates
		clockwise or anti clockwise 
	'''

	matrix=[[1,2,3],[4,5,6],[7,8,9]]
	transposedMatrix=transpose(matrix)
	# displays the resultant transpose matrix
	print("The transposed matrix is :",transposedMatrix)
	# displays options for rotation of matrix
	print("The options for direction of rotation")
	print("\t\t\t 1.Anticlockwise\n\t\t\t2.Clockwise")
	direction=int(input("Enter the option : "))
	# displays the rotated matrix
	print("The rotated matrix is",rotate(matrix,direction))

if __name__=='__main__':
	rotateMatrix() 