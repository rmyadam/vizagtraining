'''
This program renames all the files that are present in specified location
@Rithvik
'''
# import os and glob module for furthur use in program
import os,glob
import sys

def renameFiles(param):
    '''
    This function renames all the names of files in a particular location
    input:path(Str),prefix(Str),postfix(Int),extension(Str)
    output: renames all file names in myfolder directory
    '''
    # take input for path,prefix,postfix,extension
    print(param)
    path = str(param[1])
    prefix = param[2].split("=")
    prefix = prefix[1]
    postfix = param[3].split("=")
    postfix = int(postfix[1]) 
    extension = param[4].split("=")
    extension = extension[1]
    # opens the folder(myfolder) location of created files
    os.chdir(r"C:\Users\rmyadam\Desktop\vizagtraining\\"+path)
    # creating an iterator with all files having .txt extension
    directory = glob.glob('*.txt')
    # iterating the range of postfix and files in directory simultaneously
    for file,i in zip((directory),range(postfix,postfix+101)):
        # gets the path of files in directory and creates new path(newFileLocation)
        #   checks if file named as newFileName already exists or not 
        path = (os.path.abspath(file))
        fileLocation = os.path.dirname(path)
        # the new name which has to be replaced for created files 
        newFileName = "\\"+prefix+str(i)+extension
        # the location at which the files are to be renamed
        newFileLocation = (fileLocation+newFileName)
        exist = os.path.exists(newFileLocation)
        # if the file named as newFileName exists gives options either to overwrite
        #   the file with the same name of other name 
        if exist == True:
            # displays file name which is already existing 
            print("The which already exists is : ",newFileName)
            print("1.rename the file with other name \n2.Leave the file as it is ")
            option = int(input("Enter the option "))
            # option 1 is for renaming file name with other name
            if option ==1:
                existFileName = input("Enter the name of file for renaming")
                existFileLocation = fileLocation +"/"+ existFileName + extension
                os.rename(path,existFileLocation)
            # option 2 is leaving the file as it is
            else:
                pass
        # renames the files to newFilename 
        else:
            os.rename(path,newFileLocation)
    print("The files are renamed successfully")
if __name__=='__main__':
    renameFiles(sys.argv)
