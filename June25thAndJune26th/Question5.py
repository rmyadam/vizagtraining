import cv2 as cv
import matplotlib.pyplot as plt

def sizeChange():
    '''
    This function reads a image increases its size by 2 times and 
        decreases its size by half 
    input: image.png(image)
    output: diminishedImage(image),enlargedImage(image)  
    '''
    # reading an image in form of numpy array into a variable image
    image = cv.imread('reasources/image.png')
    # changing the height and width of image by half and resizing it
    #  by using method(resize)
    diminishedImage = cv.resize(image,(int(image.shape[0]*0.5),int(image.shape[1]*0.5)))
    # chnaging the height and width of image by two times and resizing
    #  it by using method(resize) 
    enlargedImage = cv.resize(image,(int(image.shape[0]*2),int(image.shape[1]*2)))
    # creating subplots in matrix having one row and three columns 
    figure,sub = plt.subplots(1,3)
    # padding each subplot by 2 spaces and naming the window and
    # subplots  
    figure.tight_layout(pad=2.0)
    figure.canvas.set_window_title("Size change")
    sub[0].set_title("original image")
    sub[1].set_title("diminished image")
    sub[2].set_title("enlarged image")
    # displaying the respective images 
    sub[0].imshow(image)
    sub[1].imshow(diminishedImage)
    sub[2].imshow(enlargedImage)
    # saving the plot 
    plt.savefig("June25thAndJune26th/Question5_solution.png")
    plt.show()

if __name__=='__main__':
    sizeChange()
