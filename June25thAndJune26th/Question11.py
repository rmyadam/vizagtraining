'''
This program finds the circles in an image and highlights
 the circles
 @Rithvik
'''

# import pyplot from matplotlib,openCV and numpy
import numpy as np 
import matplotlib.pyplot as plt 
import cv2 as cv 

def findCircle():
    '''
    This function finds all the circles and highlights them 
    input: circles.png(image)
    output: resultImage(image)
    '''
    # reading the image in form of a numpy array 
    image = cv.imread('reasources/circles.png')
    # copying the address of input image to new variable
    resultImage = image.copy()
    # find the gray image of given image(image) 
    grayImage = cv.cvtColor(image,cv.COLOR_BGR2GRAY)
    # find the coordinates and radius of the cirles in image(grayImage)
    circleList = cv.HoughCircles(grayImage,cv.HOUGH_GRADIENT,1.3,50)
    number = len(circleList[0])
    # checking whether the image has circles 
    if circleList is not None:
        # rounding up values of coordinates and radius and higlight the cricles
        # in the same image 
        circleList = np.round(circleList[0,:]).astype("int")
        for(xcordinate,yCordinate,radius) in circleList:
            cv.circle(resultImage,(xcordinate,yCordinate),radius,(0,255,0),2)
    # writing the number of circles on the image 
    cv.putText(resultImage,"circles:"+str(number),(0,150),cv.FONT_HERSHEY_COMPLEX,
                        1,(255,0,0),1,cv.LINE_AA)
    # showing result image using matplotlib
    plt.imshow(resultImage)
    # saving the plot 
    plt.savefig("June25thAndJune26th/Question11_solution.png")
    plt.show()

if __name__=='__main__':
    findCircle()