'''
This program finds the edges in gray image and displays it 
@Rithvik 
'''

# import pyplot from matplotlib,openCV and numpy
import numpy as np
import matplotlib.pyplot as plt 
import cv2 as cv

def findEdge():
    '''
    This function finds the edges in a gray image and displays it
    input: image.png(image)
    output: edgeImage(image)  
    '''
    # reading an image in form of numpy array
    image = cv.imread('reasources/image.png')
    # converting the given image into gray image 
    grayImage = cv.cvtColor(image,cv.COLOR_BGR2GRAY)
    # finds the edges in image and displays it by Canny method 
    edgeImage = cv.Canny(grayImage,100,100)
    # divide into subplots,naming the window and subplots 
    figure,sub = plt.subplots(1,3)
    figure.tight_layout(pad=2.0)
    figure.canvas.set_window_title("Edge Image") 
    sub[0].set_title("Original Image")
    sub[1].set_title("Gray Image")
    sub[2].set_title("Edge Image")
    # showing the subplots of respective images
    sub[0].imshow(image)
    sub[1].imshow(grayImage,cmap='gray')
    sub[2].imshow(edgeImage)
    # saving the plot 
    plt.savefig('June25thAndJune26th/Question10_solution.png')
    plt.show()

if __name__=='__main__':
    findEdge()