'''
This program reads an image and displays it by using opencv
 and matplotlib module 
  @Rithvik 
'''

# import pyplot from matplotlib,import numpy and open cv   
import matplotlib.pyplot as plt 
import numpy as np 
import cv2 as cv 

def displayMatplotImage():
    '''
    This method reads the image and displays using matplotlib module 
    input: image.png(image)
    output: displays the image 
    '''
    # reading the image in form of numpy array to variable 
    # matplotImage
    matplotImage = plt.imread('reasources/image.png')
    # setting up the window name as "matplot image"
    figure = plt.figure()
    figure.canvas.set_window_title("matplot image")
    # naming the tiltle of image as "bike"
    plt.title('bike')
    # display the image 
    plt.imshow(matplotImage)
    # save the plot 
    plt.savefig('June25thAndJune26th/Question3_solution1.png')
    plt.show()

def displayOpenCvImage():
    '''
    This method reads the image and displays it by using open cv module 
    input: image.png(image)
    output: displays the image 
    '''
    # reading the image in form of a numpy array into
    # into a variable openCvImage
    openCvImage = cv.imread('reasources/image.png')
    # displaying a window which fits the image 
    cv.imshow('OpenCv Image',openCvImage)
    # saving the image in same folder 
    cv.imwrite('June25thAndJune26th/Question3_solution2.png',openCvImage)
    # delays output by 0 milliseconds
    cv.waitKey(0)
    # destroys any windows if open 
    cv.destroyAllWindows()

if __name__=='__main__':
    displayMatplotImage()
    displayOpenCvImage()