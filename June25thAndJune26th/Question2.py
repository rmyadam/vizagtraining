'''
This program reads an image and writes the image by using open cv 
 module
 @Rithvik 
'''
# importing open cv module 
import cv2 as cv 

def readImage():
    '''
    This function is used to read, changes the image name and 
    writes the image in a location  
    input: image.png(image)
    output : changes name of image amd writes it 
    '''
    # reading the repective image into the array named image
    image = cv.imread('reasources/image.png')
    # writing the same image into the foler by imwrite method 
    cv.imwrite('June25thAndJune26th/Question2_solution.png',image)

if __name__=="__main__": 
    readImage()