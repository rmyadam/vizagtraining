'''
This program coverts a colour image to gray image and displays 
 it by using the open cv module 
 @Rithvik
'''
# import open cv module 
import cv2 as cv
import numpy as np 
import matplotlib.pyplot as plt

def convertGrey():
    '''
    This function reads a image converts into a gray image and 
        displays it 
    input: image.png(image)
    output: grayImage(image)
    '''
    # reading image in form a numpy array into variable image
    image = cv.imread('reasources/image.png')
    # convert input image to gray colour image 
    grayImage =  cv.cvtColor(image,cv.COLOR_BGR2GRAY)
    # showing the grayscale image by matplotlib
    plt.imshow(grayImage,cmap='gray')
    # saving the gray scale image in same folder 
    plt.savefig("June25thAndJune26th/Question4_solution1.png")
    plt.show()
    # show the gray scale image by openCV
    cv.imshow('grey image',grayImage)
    # saving the resultant images in same folder 
    cv.imwrite('June25thAndJune26th/Question4_solution2.png',grayImage)
    # delay output by 0 milliseconds 
    cv.waitKey(0)
    # destroy if any windows are running 
    cv.destroyAllWindows()

if __name__=="__main__":
    convertGrey()