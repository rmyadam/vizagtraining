'''
This program finds the mirror image of a image and 
 displays it
 @Rithvik
'''

# import pyplot from matplolib,open cv and numpy
import cv2 as cv 
import numpy as np
import matplotlib.pyplot as plt 

def mirrorImage():
    '''
    This function finds the mirror image of input image 
    and diplays it 
    input: image.png(image)
    output: displays both the original image and mirroe image
    '''
    # reading the image in form of numpy array 
    image = cv.imread('reasources/image.png')
    # flipping the image to get mirror image
    lateralImage = cv.flip(image,1)
    # dividing subplots and naming the subplots 
    figure,sub = plt.subplots(1,2)
    figure.canvas.set_window_title("lateral inversion ")
    sub[0].set_title("original image")
    sub[1].set_title("mirror image")
    # showing both the images in respective subplots 
    sub[0].imshow(image)
    sub[1].imshow(lateralImage)
    # saving the plot 
    plt.savefig("June25thAndJune26th/Question8_solution.png")
    plt.show()

if __name__=="__main__":
    mirrorImage()