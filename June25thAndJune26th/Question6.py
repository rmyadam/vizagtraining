'''
This program plots rgb histogram of an image 
@Rithvik
'''

# import pyplot from matplotlib,open cv and numpy 
import cv2 as cv 
import matplotlib.pyplot as plt 
import numpy as np 

def colorHistogram():
    '''
    This function reads a image and plots rgb histogram of it 
    input: image.png(image)
    output: plots a histogram 
    '''
    # reads an image in form of numpy array 
    image = cv.imread('reasources/image.png')
    # divides subplots and naming the window
    figure,sub = plt.subplots(1,3)
    figure.canvas.set_window_title("Histogram")
    figure.tight_layout(pad=2.0)
    # creating the histograms for respective colours  
    redHistogram = cv.calcHist([image],[0],None,[256],[0,256])
    blueHistogram = cv.calcHist([image],[1],None,[256],[0,256])
    greenHistogram = cv.calcHist([image],[2],None,[256],[0,256])
    # naming the subplots according to colours
    sub[0].set_title("blue histogram")
    sub[1].set_title("green histogram")
    sub[2].set_title("red histogram")
    # plotting the histograms for respective colours 
    sub[0].plot(redHistogram,color='b')
    sub[1].plot(blueHistogram,color='g')
    sub[2].plot(greenHistogram,color='r')
    # saving the plot
    plt.savefig("June25thAndJune26th/Question6_solution.png")
    # displaying the histograms 
    plt.show()

if __name__=="__main__":
    colorHistogram()