'''
This programs uses different methods to blur an image and displays them
@Rithvik 
'''
# import pyplot from matplotlib, openCV and numpy
import cv2 as cv 
import matplotlib.pyplot as plt 
import numpy as np 


def imageBlur():
    '''
    This function finds the blurred images of given input image by
        various methods and displays them 
    input: image.png(image)
    output: avgBlurImage(image),gausBlurImage(image),bilBlurImage(image),
                medBlurImage(image)
    '''
    # reading an image in form of numpy array 
    image = cv.imread('reasources/image.png')
    # finds the blurred image by blur method 
    avgBlurImage = cv.blur(image,(10,10))
    # finds the blurred image by GaussianBlur method 
    gausBlurImage = cv.GaussianBlur(image,(13,13),7)
    # finds the blurred image by medianBlur method 
    medBlurImage = cv.medianBlur(image,15)
    # finds the blurred image by bilateralFilter method 
    bilBlurImage = cv.bilateralFilter(image,75,100,100)
    # divides the subplots,name window and subplots  
    figure,sub = plt.subplots(2,2)
    figure.canvas.set_window_title("Blurred Images") 
    figure.tight_layout(pad=2.0)
    sub[0,0].set_title("Blur Image")
    sub[0,1].set_title("Gaussian Blur Image")
    sub[1,0].set_title("Median Blur Image")
    sub[1,1].set_title("Bilateral Filter Image")
    # display the various blurred images 
    sub[0,0].imshow(avgBlurImage)
    sub[0,1].imshow(gausBlurImage)
    sub[1,0].imshow(medBlurImage)
    sub[1,1].imshow(bilBlurImage)
    # saving the plot 
    plt.savefig("June25thAndJune26th/Question9_solution.png")
    plt.show()

if __name__=="__main__":
    imageBlur()