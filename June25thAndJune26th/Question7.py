'''
This program rotates the given image and displays it
@Rithvik 
'''

# import pyplot from matplotlib,openCV and numpy 
import cv2 as cv 
import matplotlib.pyplot as plt 
import numpy as np 

def rotateImage(image,angle):
    '''
    This function will read the dimension of the given image and 
    rotate it according to given angle
    param: image(numpy array),angle(Int)
    output: rotatedImage(image)
    '''
    # reading the dimentsions of the given image
    height,width = image.shape[:2]
    # rotating the image according to the angle parameter received by function 
    rotateMatrix = cv.getRotationMatrix2D((height//2,width//2),angle,1)
    rotatedImage = cv.warpAffine(image,rotateMatrix,(height,width))
    # show the resultant image(rotatedImage)
    plt.imshow(rotatedImage)
    # saving the plot 
    plt.savefig("June25thAndJune26th/Question7_solution.png")
    plt.show()

def rotateMethod():
    '''
    This function reads the image takes input for angle 
    and rotates given image 
    '''
    # reads the image into a variable(image)
    image = cv.imread('reasources/image.png')
    # takes input for angle 
    angle = int(input("Enter the angle which the image should rotate: "))
    rotateImage(image,angle)

if __name__=='__main__':
    rotateMethod()



