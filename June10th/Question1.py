'''
This program exhibits the python inbuilt methods of list
@Rithvik
'''
# importing random and itertools module for furthur use in program
import random
import itertools

def findEmptyList():
	'''
	This method finds whether the list is empty or not
	input: list1(list)
	output: prints the list is empty if list1 is empty
	'''
	# initlization of list
	list1=[]
	# find length and if zero prints the statement
	if len(list1)==0:
		print("The list is empty")
	# if the list1 has elements then it prints that list is not empty
	else:
		print ("The list is not empty")

def findNumber():
	'''
	This program finds the maximum number , minimum number, sum and product of all
		elements in a list
	input:list1(list)
	output:maxValue(Int),minValue(Int),sumValue(Int),productValue(Int)
	'''
	# input is given as list1 (list)
	list1=[1,2,7,4,7,43,65,9,7]
	# The maximum and minimum values are found out by min and max functions
	maxValue=max(list1)
	minValue=min(list1)
	print("The maximum value in list is : ",maxValue)
	print("The minimum value in list is : ",minValue)
	# a viriable (sumValue) is initialized to zero
	sumValue=0
	# a variable (prodcutValue) is initialized to one
	productValue=1
	# iterating through out the list1 the cumulative sum and product is found
	for value in list1:
		sumValue=sumValue + value
		productValue=productValue * value
	print("The sum of all elements in list are : ",sumValue)
	print("The product of all elements in list are : ",productValue)

def findString():
	'''
	This function counts the number of elements in a list that have their
		length greater than 2 and whose first character and last character are same
	input: list1(list)
	output: count(Int)
	'''
	# taking input list1(list)
	list1 = ['star','abc','bbc','cd','ddd',"jkj"]
	# initializing a variable count to zero
	count = 0
	# iterating through the list checking for length of each element and
	#   checking whether the first and last character is same,incrementing count
	for element in list1:
		if len(element) >2 or element[0] == element[len(element)-1]:
			count=count+1
	print("The count of elements whose length is greater than 2 and whose first and last char is same:",count)

def sortList():
	'''
	This function takes list of tuples as input and sorts according to the second value
		of each tuple
	input: list1(list of tuples)
	output: resultList(list of tuples)
	'''
	# takes list1(list of tuples) as input
	list1 = [(3,4),(1,2),(8,3),(1,10),(3,7)]
	# resultList(list of tuples) is sorted
	resultList = sorted(list1,key=lambda x:x[1])
	print("The sorted list of tuples is : ",resultList)

def duplicateList():
	'''
	This function removes dulicates from the list
	input: list(list)
	output: resultList(list)
	'''
	# take in input list1(list)
	list1 = [1,1,1,4,5,4,6,7,7,8,8,9]
	# convert the list into setto remove duplicates
	resultList = set(list1)
	# convert the set back to list
	resultList = list(resultList)
	print("The resulttant list without duplicates is :",resultList)

def duplicateDict():
	'''
	This function removes duplicate dictionary from list of dictionaries
	input:list1(list of dictionaries)
	output: resultList(list of dictionaries)
	'''
	# list1 is taken as input
	list1=[{"rithvik":1},{"john":2},{"mohan":4},{"rithvik":1}]
	# uses list comprehension and enumerat function to check for duplicates and removes them
	resultList=[dictvalue for index,dictvalue in enumerate(list1) if dictvalue not in list1[index+1:]]
	print("The resultant list without duplicate dictionaries are : ",resultList)

def commonItem():
	'''
	This function checks whether the two lists have a common item among them
	input: list1(list),list2(list)
	output: prints if they have common element
	'''
	# input taken list1(list),list2(list)
	list1 = [2,4,3,5,6,5,6,7,65]
	list2 = [2,4,3,5,56,6]
	# both the list are converted into sets as set1 and set2
	set1 = set(list1)
	set2 = set(list2)
	# disjoint of sets is founnd and stored in result
	result= set1.isdisjoint(set2)
	# the print statements are printed according to result obtained
	if result==False:
		print("The two lists have common element")
	else:
		print("The two lists dont have a common element")

def shuffleList():
	'''
	This function shuffles the inputList
	input: list1(list)
	output: list1(list)
	'''
	# input taken list1(list)
	list1=[1,45,43,5,4,65,6,75,6]
	# shuffle method in random module is used to shuffle the elements
	random.shuffle(list1)
	print("The shuffled list is : ",list1)

def permutateList():
	'''
	This function is used to find possible permutations of elements in list
	input: list1(list)
	output: resultList(list)
	'''
	# input taken list1(list)
	list1 = [1,3,5,2]
	# permutations are found by permutations method in itertools and result
	#   is stored in resultList
	resultList = itertools.permutations(list1)
	# iterating through the resultList to print different permutations
	print("The permutations are as follows: ")
	for l in resultList:
		print(l)

def makeString():
	'''
	This function joins every element of list to form a string
	input: list1(list)
	output: resultString(Str)
	'''
	# input taken list1(list)
	list1=['ri','th','vi','k']
	# join method is used to merge all elements of list into a string and
	#   stored in resultString
	resultString=''.join(list1)
	print("The string by concatinating characters from list is :",resultString)

def findIndex():
	'''
	This fucntion finds the index of a particular element in the list
	input: list1(list)
	output:elementIndex(Int)
	'''
	# take input list1(list)
	list1 = [2,4,5,4,56,56,56,6,567]
	# the index of element 4 is found and stored in elementIndex
	elementIndex = list1.index(4)
	print("The index of the element 4 is :",elementIndex)

def flattenList():
	'''
	This function is used to flatten a list of lists
	input:list1(list)
	output:resultList(list)
	'''
	# take input list1
	list1 = [[1,2,3,32,4],[1,2,3,43]]
	# chain method from itertools module is used to flatten list1
	#   result obtained is stored in resultList
	resultList = (itertools.chain.from_iterable(list1))
	print("The flattened list is : ",list(resultList))

def selectRandom():
	'''
	This function is used to pick a random element from the list
	input:list1(list)
	output:element(Int)
	'''
	# take input list1
	list1 = [1,3,4,3,6,7,5,7,5]
	# to pick a random element choice method from rondom module is used
	element = random.choice(list1)
	print("The random element accessed from list is : ",element)

def secondMax():
	'''
	This function is used to find out second maximum number in a list
	input: list1(list)
	output: element(Int)
	'''
	# take input list1
	list1 = [2,54,6,6,75,6,5,75,576,]
	# set method is used to convert list1 to set1(set)
	set1 = set(list1)
	# maximum element from set is removed
	set1.remove(max(list1))
	# then again the maximum element is stored in element
	element = max(set1)
	print("The second largest element in list is :", element)

def repeatList():
	'''
	This function is used to remove the repeating elements and return
		a nonrepeating list
	input:list1(list)
	output: resultlist(list)
	'''
	# take input list1()
	list1=[1,3,4,453,5,54,5,5,5]
	# set method is used to remove duplicates
	set1=set(list1)
	# set1 is converted into a list and stored in resultList
	resultList=list(set1)
	print("The non repeating list is : ",resultList)

def findFrequency():
	'''
	This function finds out the frequency of each element and returns a dictionary
		containing keys as elements and values as frequency
	input:list1(list)
	output:frequency(dict)
	'''
	# take input list1(list)
	list1=[1,2,32,3,23,32,5,4,6,4]
	# initialize a dictionary frequency
	frequency={}
	# iterating through the list1
	for element in list1:
		# if element is present in dictionary(frequency) increment the
		#   value by 1
		if element in frequency:
			frequency[element] +=1
		# else set the value as 1
		else:
			frequency[element] = 1
	print("The frquency of each element in list is given by : ",frequency)

def findCommonItem():
	'''
	This function finds the common elements between two lists and
		returns the list containing those common items
	input: list1(list),list2(list)
	output: resultList(list)
	'''
	# take input list1(list) and list2(list)
	list1 = [1,5,4,6,4,5,4,3,5]
	list2 = [3,54,3,4,5,4,6,5,8,6]
	# convert each list into respective sets set1 and set2
	set1 = set(list1)
	set2 = set(list2)
	# find the intersection of two sets and store in set3
	set3 = set1 & set2
	# convert the set3 (set) into list
	resultList = list(set3)
	print("The common elements of two lists are: ",resultList)

def formInteger():
	'''
	This function creates a number out of all the elements in a list
	input:list1(list)
	output: integer(Int)
	'''
	# take input list1(list)
	list1=[3,4,5,4]
	list1=list1[::-1]
	# initialize a variable integer as zero
	integer=0
	# iterate through the elements and multiply each element with
	#   its index to power of 10
	for i in range(len(list1)):
		integer = integer + (10**i)*list1[i]
	print("The integer formed by individual numbers in list are : ",integer)

def characterList():
	'''
	This function divides a string into a list of characters
	input:string(Str)
	output:list1(list)
	'''
	# string is taken as input
	string = 'he works in pharmacy'
	# list1 is assigned by converting the string to a list
	list1 = list(string)
	print("The list of characters is : ",list1)

def iterateTwoLists():
	'''
	This function iterates two lists at a time
	input: list1(list),list2(list)
	output: prints two lists at a time
	'''
	# takes input list1 and list2
	list1=[1,2,3,4,5]
	list2=["name","employeeid","email","password","insurance"]
	# use zip function to iterate two lists at a time
	for (element1,element2) in zip(list1,list2):
		print("The two elements from two lists are: ")
		print(element1,element2)

def consecutiveDuplicates():
	'''
	This function groups two consecutive duplicates into a list
	input: list1(list)
	output: resultList(list)
	'''
	# take input list1
	list1 = [1,2,2,3,3,4,4,5,6]
	# the resultList is created by list comprehension by using groupby method
	#   from itertools module
	resultList = [list(group) for key,group in itertools.groupby(list1)]
	print("The list of consecutive duplicates is : ",resultList)

def insertElement():
	'''
	This function is used to insert a particular element in a particular index
		of a list
	input:list1(list)
	output:list1(list) with the element added in it
	'''
	# take input list1
	list1 = [1,2,4,3,4,4,5,6,5,76]
	# use insert method to insert the element
	list1.insert(4,45)
	print("The list after inserting element in index 4 is: ",list1)

def addElement():
	'''
	This function is used to add and element after each element in a
		list
	input: list1(list)
	output: list1(list)
	'''
	# take input list1
	list1 = [1,3,4,3,5,6,4]
	# iterating through the list and adding '6' to end of every element
	for i in range(1,len(list1),2):
		list1.insert(i,6)
	list1.append(6)
	print("The resultant list after adding an element after every element : ",list1)

def searchElement():
	'''
	This function is used to search a element
	input: list1(list)
	output: prints the statement if element is present
	'''
	list1 = [1,3,3,4,5,4,76,5,6]
	element = 6
	if element in list1:
		print("The particular element is present ")

def removeEvenElements():
	'''
	This function is used to remove all the even numbers from a list
	input:list1(list)
	output:list1(list)
	'''
	# take input list1
	list1=[1,4,3,5,5,7,6,7,8,6,88,54]
	# iterating through elements in list1 and if element is
	#   divisible by 2 removing it by remove method
	for element in list1:
		if (element % 2==0):
			list1.remove(element)
	print("The list after removing even numbers are " , list1)

def numberConcatinate():
	'''
	This function is used to concatinate a numbers with each element
		in the specified range
	input:list1(list)
	output:resultList(list)
	'''
	# take input list1
	list1 = ['p','q']
	# number is the specified range
	number = 5
	# use list comprehension and string formating to concatinate element
	#    with the number in range
	resultList = ['{}{}'.format(x,y) for y in range(1,number+1) for x in list1]
	print("The resultant list after concatinating each element with a number in range :",resultList)

def subList():
	'''
	This function is used to create possible combination of sublists from the
		given list
	input:list1(list)
	output:resultList(list)
	'''
	# take input list1
	list1=['x','y','z']
	# initialization of empty list
	resultList = []
	# use combinations method from itertools to create combinations from
	#   input list and covert the combinations to list, append it to
	#   resultList
	for element in range((len(list1)+1)):
		temporaryList = [list(item) for item in itertools.combinations(list1,element)]
		if len(temporaryList) > 0:
			resultList.extend(temporaryList)
	print("The resultant List after dividing a list into sublists is as follows : ",resultList)


def listMethods():
	'''
	This method calls all the above defined methods
	'''
	findEmptyList()
	findNumber()
	findString()
	sortList()
	duplicateList()
	duplicateDict()
	commonItem()
	shuffleList()
	permutateList()
	makeString()
	findIndex()
	flattenList()
	selectRandom()
	secondMax()
	repeatList()
	findFrequency()
	findCommonItem()
	formInteger()
	characterList()
	iterateTwoLists()
	consecutiveDuplicates()
	insertElement()
	addElement()
	searchElement()
	removeEvenElements()
	numberConcatinate()
	subList()

if __name__=='__main__':
	listMethods()