"""
This program contains all the exceptions of selenium 
@Rithvik 
"""

# importing required modules 
from selenium import webdriver
from selenium.common.exceptions import *
from selenium.webdriver.support.ui import Select
from selenium.webdriver import ActionChains


def exception():
    """
    This function contains try and except blocks for few exceptions in 
    selenium 
    """

    # creation of webdriver object 
    driver = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\August12\chromedriver_win32\chromedriver.exe")
    # maximizing window and getting response from website
    driver.maximize_window()
    driver.get("https://stackoverflow.com/")

    # initializing the count as 1
    count = 1

    # iterating by while loop 
    while (True):
        # defining try block 
        try:

            if count == 1:
                # creating NoSuchElement exception by not locating the web element 
                driver.find_element_by_id("wich")

            elif count==2:
                # creating NoSuchFrameException by 
                # switching to a frame which is not present 
                driver.switch_to.frame(1)

            elif count==3:
                # creating NoAlertPresentException exception by 
                # switching to a frame which is not present 
                driver.switch_to_alert()
                
            elif count==4:
                # creating InvalidSwitchToTargetException by 
                # switching to a frame which is not present 
                driver.switch_to_frame(1)

            elif count==5:
                # creating ElementNotInteractableException by 
                # clicking the element which is not clickable 
                driver.find_element_by_id("notify-container").click()
            
            elif count==6:
                # creating UnexpectedTagNameException by 
                # using the Select for a web element
                divTag = driver.find_element_by_id("notify-container")
                Select(divTag)

            elif count==7:
                # creating MoveTargetOutOfBoundsException by using action chains 
                # hovering to different offset
                action = ActionChains(driver)             
                action.move_by_offset(500,1000)
                action.perform()

            elif count==8:
                # creating no such element exception by using heading tag
                # and using click method 
                driver.find_element_by_partial_link_text("w90 mx-auto p-ff-roboto-slab-bold fs-display2 ta-center mb16").click()
                
            elif count==9:
                # creating InvalidElementStateException by using text box
                x = driver.find_element_by_partial_link_text("Log in")
                x.clear()
                
            elif count==10:
                # creating WebDriverException by using driver 
                # object 
                webdriver.Edge(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\August12\chromedriver_win32\chromedriver.exe")
                
            elif count==11:
                # creating TimeoutException by setting the 
                # page load time as zero 
                driver.set_page_load_timeout(0)
                driver.get("https://www.freckle.com/")

            elif count==12:
                # breaking the loop 
                break 

        # NoSuchElementException block 
        except NoSuchElementException as ex:
            # printing the exception message and incrementing count 
            print(ex.msg)
            count = count + 1

        # NoSuchFrameException block
        except NoSuchFrameException as ex:
            # printing the exception message and incrementing count 
            print(ex.msg)
            count = count + 1

        # NoAlertPresentException block
        except NoAlertPresentException as ex:
            # printing the exception message and incrementing count 
            print(ex.msg)
            count = count + 1

        # InvalidSwitchToTargetException block
        except InvalidSwitchToTargetException as ex:
            # printing the exception message and incrementing count 
            print(ex.msg)
            count = count + 1

        # ElementNotInteractableException block
        except ElementNotInteractableException as ex:
            # printing the exception message and incrementing count 
            print(ex.msg)
            count = count + 1

        # UnexpectedTagNameException block
        except UnexpectedTagNameException as ex:
            # printing the exception message and incrementing count 
            print(ex.msg) 
            count = count + 1      

        #  MoveTargetOutOfBoundsException block
        except MoveTargetOutOfBoundsException as ex:
            # printing the exception message and incrementing count 
            print(ex.msg)
            count = count + 1     

        #   InvalidElementStateException block
        except InvalidElementStateException as ex:
            # printing the exception message and incrementing count 
            print(ex.msg)
            count = count + 1     

        #   WebDriverException block
        except WebDriverException as ex:
            # printing the exception message and incrementing count 
            print(ex.msg)
            count = count + 1 

        #   TimeoutException block
        except TimeoutException as ex:
            # printing the exception message and incrementing count 
            print(ex.msg)
            count = count + 1 

    # closing the browser 
    driver.close()

if __name__=="__main__":
    exception()