"""
This program runs the test cases imported from files and generates 
html reports 
@author Rithvik 
"""


import time 
import HtmlTestRunner
import unittest
from selenium import webdriver
from Login import TestLoginPage
from SignUp import TestSignUpPage


    
def Aligntests():
    """
    This function runs the tests and generates html reports 
    """

    # creating a loader object 
    test_load = unittest.TestLoader()
    
    # loading the test cases in objects 
    t1 = test_load.loadTestsFromTestCase(TestLoginPage)  
    t2 = test_load.loadTestsFromTestCase(TestSignUpPage)
    
    # creating a suite object   
    suite = unittest.TestSuite()
    # adddding the load objects in the suite object 
    suite.addTest(t1)
    suite.addTests(t2)
    # running the testcases and generating report 
    testRunner = HtmlTestRunner.HTMLTestRunner(output=r'html_dir',report_title="StackOverflow testcases")
    testRunner.run(suite)

if __name__=="__main__":
    Aligntests()