"""
This program contains all test cases of Sign up
@author Ritvhik 
"""


import time 
import unittest
from driver import *
from openpyxl import *
from webelements import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys


# declaring a global variable driver 
global driver 


class TestSignUpPage(unittest.TestCase):

    # class method which is executed before all test cases 
    @classmethod
    def setUpClass(cls):
        """
        This method will redirect the page to login page and 
        get the testsheet from excel document 
        """
        # a class level driver variable initalized to driver imported 
        cls.driver = driver
        cls.driver.maximize_window()

        # creating a loader object of excel document 
        loadobj = load_workbook(r"Selenium Training\August19\TestData.xlsx")
        # derive test sheet from loader object 
        cls.sheet = loadobj["RegistrationCases"]

        # object creation for login class to redirect to login page  
        cls.element = Signup(cls.driver)
        cls.element.changeToSignup()


    def testSignUpEmail(self):
        """
        This method has test cases for email field in web page 
        """
        # creating a object of Sign up class to fetch elements 
        self.element = Signup(self.driver)
        # getting email webelement 
        email = self.element.getEmail()

        # getting values from excel sheet from a particular column 
        for i in range(2,6):
            # clearing email field and entering the values 
            email.clear()
            email.send_keys(self.sheet.cell(row=i,column=3).value)
            time.sleep(3)
            # asserts statement for email field 
            self.assertEqual(email.get_attribute("value"),self.sheet.cell(row=i,column=3).value)
            self.assertTrue(email.is_displayed())


    def testDisplayName(self):
        """
        This method has test cases for display name field in web page
        """
        # creating a object of sign up class to fetch elements 
        self.element = Signup(self.driver)
        # getting display name webelement 
        name = self.element.getDisplayName()

        # getting values from excel sheet from a particular column 
        for i in range(2,6):
            # clearing display name field and entering the values 
            name.clear()
            name.send_keys(self.sheet.cell(row=i,column=2).value)
            time.sleep(3)
            # asserts statement for display name field 
            self.assertEqual(name.get_attribute("value"),self.sheet.cell(row=i,column=2).value)
            self.assertTrue(name.is_displayed())


    def testPassword(self):
        """
        This method has test cases for password field in web page 
        """
        # creating a object of Sign up class to fetch elements 
        self.element = Signup(self.driver)
        # getting password webelement 
        password = self.element.getPassword()

        # getting values from excel sheet from a particular column 
        for i in range(2,6):
            # clearing password field and entering the values 
            password.clear()
            password.send_keys(self.sheet.cell(row=i,column=4).value)
            time.sleep(3)
            # asserts statement for password field 
            self.assertEqual(password.get_attribute("value"),self.sheet.cell(row=i,column=4).value)
            self.assertTrue(password.is_displayed())

    def tearDown(self):
        """
        This method gets executed after each test case 
        """
        # clearing the fields after every test case 
        self.driver.find_element_by_id("email").clear()
        self.driver.find_element_by_id("display-name").clear()
        self.driver.find_element_by_id("password").clear()


    @classmethod
    def tearDownClass(cls):
        time.sleep(6)
        cls.driver.quit()

if __name__=="__main__":
    unittest.main()