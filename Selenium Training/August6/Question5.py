"""
This program contains all the methods of drop box performed 
in selenium 
@Rithvik 
"""

# importing all the reuired modules 
from selenium import webdriver 
import time 
from selenium.webdriver.support.ui import Select


# creation of webdriver object 
driver = webdriver.Chrome(r"C:\Users\RAVITEJA\Documents\vizagtraining\Selenium Training\August6\chromedriver_win32\chromedriver.exe")
# getting the response and maximizing the window of website 
driver.get("file:///C:/Users/RAVITEJA/Documents/vizagtraining/Selenium%20Training/August6/Question5.html")
driver.maximize_window()
time.sleep(3)

# locating the drop drown web element 
listBox = driver.find_element_by_xpath("//select[@id='cars']")

# creation of dropdown object 
dropDown = Select(listBox)

# selecting the option by value 
dropDown.select_by_value("audi")

time.sleep(3)

# selecting the option by index 
dropDown.select_by_index(2)
time.sleep(3)

# selecting the option by visible text
dropDown.select_by_visible_text("Saab")
time.sleep(3)

# printing all the options in drop down box 
for x in dropDown.options:
    print(x.text)


# printing the visibility of the drop down box 
print(driver.find_element_by_xpath("//select[@id='cars']").is_displayed())

# printing whether drop down box is enabled 
print(driver.find_element_by_xpath("//select[@id='cars']").is_enabled())

# printing the name of the drop down box  
print(driver.find_element_by_xpath("//select[@id='cars']").get_attribute("name"))

# printing the type of the drop down box  
print(driver.find_element_by_xpath("//select[@id='cars']").get_property("type"))

# printing whether drop down box is selected   
print(driver.find_element_by_xpath("//select[@id='cars']").is_selected())

# printing the tag name of the drop down box  
print(driver.find_element_by_xpath("//select[@id='cars']").tag_name)

# printing the location of the drop down box  
print(driver.find_element_by_xpath("//select[@id='cars']").location)

# printing the position of the drop down box  
print(driver.find_element_by_xpath("//select[@id='cars']").rect)

# printing the size of the drop down box  
print(driver.find_element_by_xpath("//select[@id='cars']").size)

# printing the font-size of the drop down box  
print(driver.find_element_by_xpath("//select[@id='cars']").value_of_css_property("font-size"))

# printing the parent object of the drop down box  
print(driver.find_element_by_xpath("//select[@id='cars']").parent)

# closing the browser 
driver.close()