"""
This program contains all methods of checkbox that 
are performed in selenium 
@Rithvik
"""

# importing the required modules 
from selenium import webdriver 
import time 


# creation of webdriver object named as driver 
driver = webdriver.Chrome(r"C:\Users\RAVITEJA\Documents\vizagtraining\Selenium Training\August6\chromedriver_win32\chromedriver.exe") 
# getting the response from website and maximising it 
driver.get("file:///C:/Users/RAVITEJA/Documents/vizagtraining/Selenium%20Training/August6/Question4.html")
driver.maximize_window()
time.sleep(3)

# print the visibility of the check box 
print(driver.find_element_by_xpath("//input[@value='Orange']").is_displayed())

# print whether check box is enable  
print(driver.find_element_by_xpath("//input[@value='Orange']").is_enabled())

# print the name of the check box 
print(driver.find_element_by_xpath("//input[@value='Orange']").get_attribute("name"))

# print the type of the check box 
print(driver.find_element_by_xpath("//input[@value='Orange']").get_property("type"))

# print whether the check box is selected  
print(driver.find_element_by_xpath("//input[@value='Orange']").is_selected())

# submit the form which contains the check box 
driver.find_element_by_xpath("//input[@value='Orange']").submit()

# print the tag name of the check box 
print(driver.find_element_by_xpath("//input[@value='Orange']").tag_name)

# print the location of the check box 
print(driver.find_element_by_xpath("//input[@value='Orange']").location)

# print the position of the check box 
print(driver.find_element_by_xpath("//input[@value='Orange']").rect)

# print the size of the check box 
print(driver.find_element_by_xpath("//input[@value='Orange']").size)

# print the font-size of the check box 
print(driver.find_element_by_xpath("//input[@value='Orange']").value_of_css_property("font-size"))

# print the parent object of the check box 
print(driver.find_element_by_xpath("//input[@value='Orange']").parent)

# clicking the check box 
driver.find_element_by_xpath("//input[@value='Orange']").click()

time.sleep(3)

# closing the browser 
driver.close()