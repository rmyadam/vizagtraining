"""
This program contains all the methods performed on radio 
button in selenium 
@Rithvik 
"""

# importing the required modules 
from selenium import webdriver 
import time 


# creation of webdriver object 
driver = webdriver.Chrome(r"C:\Users\RAVITEJA\Documents\vizagtraining\Selenium Training\August6\chromedriver_win32\chromedriver.exe") 
# getting response and maximizing the window of website 
driver.get("file:///C:/Users/RAVITEJA/Documents/vizagtraining/Selenium%20Training/August6/Question3.html")
driver.maximize_window()
time.sleep(3)


# printing the visibility of the radio button 
print(driver.find_element_by_xpath("//*[@id='male']").is_displayed())

# printing whether radio button is enabled  
print(driver.find_element_by_xpath("//*[@id='male']").is_enabled())
 
# printing the id of the radio button 
print(driver.find_element_by_xpath("//*[@id='male']").get_attribute("id"))

# printing the checked property of the radio button 
print(driver.find_element_by_xpath("//*[@id='male']").get_property("checked"))

# printing the tag name of the radio button 
print(driver.find_element_by_xpath("//*[@id='male']").tag_name)

# printing the location of the radio button 
print(driver.find_element_by_xpath("//*[@id='male']").location)

# printing the position of the radio button 
print(driver.find_element_by_xpath("//*[@id='male']").rect)

# printing the size of the radio button 
print(driver.find_element_by_xpath("//*[@id='male']").size)

# printing the font-size of the radio button 
print(driver.find_element_by_xpath("//*[@id='male']").value_of_css_property("font-size"))

# printing the parent object of the radio button 
print(driver.find_element_by_xpath("//*[@id='male']").parent)

# clicking the radio button 
driver.find_element_by_xpath("//*[@id='male']").click()

time.sleep(3)

# closing the browser 
driver.close()