"""
This program contains the all the methods of the text box 
performed in selenium 
@Rithvik 
"""

# importing the required modules 
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time
import gettext


# creating the driver object 
driver = webdriver.Chrome(r"C:\Users\RAVITEJA\Documents\vizagtraining\Selenium Training\August6\chromedriver_win32\chromedriver.exe") 
# getting the response from the website 
driver.get("https://classroom.freckle.com/#/signup")
# maximizing the window 
driver.maximize_window()

time.sleep(3)

# sending keys into the input field
driver.find_element_by_xpath("//input[@id='first-name']").send_keys("Rithvik")
time.sleep(3)

# checking for visibilty of input field by is displayed 
print(driver.find_element_by_xpath("//input[@id='first-name']").is_displayed())

# check whether the field is enable or not 
print(driver.find_element_by_xpath("//input[@id='first-name']").is_enabled())

# printing the value of the text box  
print(driver.find_element_by_xpath("//input[@id='first-name']").get_attribute("value"))

# printing the type of the text box 
print(driver.find_element_by_xpath("//input[@id='first-name']").get_property("type"))

# check whether the text box is selected or not 
print(driver.find_element_by_xpath("//input[@id='first-name']").is_selected())

# submitting the form which contains this text box 
driver.find_element_by_xpath("//input[@id='first-name']").submit()
time.sleep(3)

# sending the keys to text box
driver.find_element_by_xpath("//input[@id='first-name']").send_keys("Myadam")
time.sleep(3)

# printing the tag name of text box 
print(driver.find_element_by_xpath("//input[@id='first-name']").tag_name)

# printing the location of text box 
print(driver.find_element_by_xpath("//input[@id='first-name']").location)

# printing the location of text box 
print(driver.find_element_by_xpath("//input[@id='first-name']").rect)

# printing the size of text box 
print(driver.find_element_by_xpath("//input[@id='first-name']").size)

# printing the text of text box 
print(driver.find_element_by_xpath("//input[@id='first-name']").text)

# printing the font size of text box 
print(driver.find_element_by_xpath("//input[@id='first-name']").value_of_css_property("font-size"))

# printing the parent object of text box 
print(driver.find_element_by_xpath("//input[@id='first-name']").parent)

# clearing the text box field 
driver.find_element_by_xpath("//input[@id='first-name']").clear()

time.sleep(3)

driver.close()