"""
This program depicts the switching of the frame 
in selenium 
@Rithvik 
"""

# importing the required modules 
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys


# creation of webdriver oibject 
driver = webdriver.Chrome(r"C:\Users\RAVITEJA\Documents\vizagtraining\Selenium Training\August6\chromedriver_win32\chromedriver.exe")
# getting response and maximizing the window of website 
driver.get("file:///C:/Users/RAVITEJA/Documents/vizagtraining/Selenium%20Training/August6/Question8.html")
driver.maximize_window()
time.sleep(3)

# switch to the frame by name 
driver.switch_to_frame("RED")

# sending keys to the from field in the frame 
driver.find_element_by_xpath("//input[@id='src']").send_keys("vishakapatnam")
time.sleep(3)

# sending keys to destination field in frame 
driver.find_element_by_xpath("//input[@class='db'][@id='dest']").send_keys("hyderabad")
time.sleep(3)

# closing the browser 
driver.close()