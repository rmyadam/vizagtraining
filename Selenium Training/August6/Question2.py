"""
This program contains all methods on a button field performed 
in selenium 
@Rithvik
"""

# importing the modules required 
from selenium import webdriver 
import time 


# creating the webdriver object named as driver 
driver = webdriver.Chrome(r"C:\Users\RAVITEJA\Documents\vizagtraining\Selenium Training\August6\chromedriver_win32\chromedriver.exe") 

# getting reponse from website and maximizing
driver.get("https://www.redbus.in/")
driver.maximize_window()
time.sleep(3)

# creating object of from field and sending keys 
driver.find_element_by_xpath("//input[@id='src']").send_keys("vishakapatnam")
time.sleep(3)

# creating the object of the destination field and send keys 
driver.find_element_by_xpath("//input[@class='db'][@id='dest']").send_keys("hyderabad")
time.sleep(3)

# creating object of date and click the date 
driver.find_element_by_xpath("//input[@id='onward_cal']").click()

# creating the object of the current date and click 
driver.find_element_by_xpath("//td[contains(text(),'25')]").click()
time.sleep(3)

# print the visibilty of the field  
print(driver.find_element_by_xpath("//BUTTON[@id='search_btn']").is_displayed())

# print whether the field is enable 
print(driver.find_element_by_xpath("//BUTTON[@id='search_btn']").is_enabled())
 
# printing the id of the button field  
print(driver.find_element_by_xpath("//BUTTON[@id='search_btn']").get_attribute("id"))

# printing the type of the button field  
print(driver.find_element_by_xpath("//BUTTON[@id='search_btn']").get_property("type"))

# printing the tagname of the button field  
print(driver.find_element_by_xpath("//BUTTON[@id='search_btn']").tag_name)

# printing the location of the button field  
print(driver.find_element_by_xpath("//BUTTON[@id='search_btn']").location)

# printing the position of the button field  
print(driver.find_element_by_xpath("//BUTTON[@id='search_btn']").rect)

# printing the size of the button field  
print(driver.find_element_by_xpath("//BUTTON[@id='search_btn']").size)

# printing the text of the button field  
print(driver.find_element_by_xpath("//BUTTON[@id='search_btn']").text)

# printing the font-size of the button field  
print(driver.find_element_by_xpath("//BUTTON[@id='search_btn']").value_of_css_property("font-size"))

# printing the parent object of the button field  
print(driver.find_element_by_xpath("//BUTTON[@id='search_btn']").parent)

# clicking the search button 
driver.find_element_by_xpath("//BUTTON[@id='search_btn']").click()

# going back in the website 
driver.back()

time.sleep(3)

# quitting the browser 
driver.quit()