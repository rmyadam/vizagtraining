"""
This program contains the methods of alerts 
performed in selenium 
@Rithvik 
"""

# importing required modules 
from selenium import webdriver 
import time 
from selenium.webdriver.support.ui import Select


# creation of the webdriver object 
driver = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\August12\chromedriver_win32\chromedriver.exe")
# getting the response and maximizing the window 
driver.get(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\August6\Question7.html")
driver.maximize_window()
time.sleep(3)

# clicking the try it button 
driver.find_element_by_xpath("/html/body/button").click()
time.sleep(3)

# creation of alert object 
alert = driver.switch_to_alert()
time.sleep(3)

# accepting the alert 
alert.accept()
time.sleep(3)

# clicking the try it button 
driver.find_element_by_xpath("/html/body/button").click()
time.sleep(3)

# printing the text of alert 
print(alert.text)

# dismissing the alert 
alert.dismiss()
time.sleep(3)

# closing the browser 
driver.close()

