"""
This program contains all the find elements methods 
performed in selenium 
@Rithvik
"""

# importing the required modules 
from selenium import webdriver 
import time 


# creation of webdriver object 
driver = webdriver.Chrome(r"C:\Users\RAVITEJA\Documents\vizagtraining\Selenium Training\August6\chromedriver_win32\chromedriver.exe")
# getting reponse and maximizing the window
driver.get("https://www.zomato.com/visakhapatnam")  
driver.maximize_window()
time.sleep(3)

# locating the elements by css selector and printing
# the alt property of them
css = driver.find_elements_by_css_selector(".ldWWwX .s1isp7-4")
print("The css object has :")
for i in css:
    print(i.get_attribute("alt"))

# locating the elements by id and printing
# the text of them
idObj = driver.find_elements_by_id("root")
print("The id object has: ")
for i in idObj:
    print(i.text)

# locating the elements by class name  and printing
# the text of them
classObj = driver.find_elements_by_class_name("title")
print("The class object has:")
for i in classObj:
    print(i.text)

# locating the elements by class name  and printing
# the text of them
nameObj = driver.find_elements_by_name("radio")
print(nameObj)
print("The name object has :")
for i in nameObj:
    print(i.text)

# locating the elements by partial link text  and printing
# the text of them
partialLink = driver.find_elements_by_partial_link_text("App")
print("The partial Link onject has :")
for i in partialLink:
    print(i.text)

# locating the elements by xpath  and printing
# the value of them
xpath1 = driver.find_elements_by_xpath("//input[@name='radio']")
for i in xpath1:
    print(i.get_attribute("value"))

# locating the elements by tag name  and printing
# the tabindex of them
button = driver.find_elements_by_tag_name("button")
for i in button:
    print(i.get_attribute("tabindex"))

# locating the elements by tag name  and printing
# the text of them
linkText = driver.find_elements_by_link_text("Manipal")
for i in linkText:
    print(i.text)


time.sleep(3)

# closing the browser 
driver.close()