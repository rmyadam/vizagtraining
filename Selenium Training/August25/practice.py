"""
This program contains all test cases of login page 
@author Ritvhik 
"""
import logging 
import HtmlTestRunner
import time 
import unittest
from driver import *
from openpyxl import *
from webelements import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# declaring a global variable driver 
global driver 
logger = logging.getLogger()
logger.setLevel(logging.DEBUG)


class TestLoginPage(unittest.TestCase):

    # class method which is executed before all test cases 
    @classmethod
    def setUpClass(cls):
        """
        This method will redirect the page to login page and 
        get the testsheet from excel document 
        """
        # a class level driver variable initalized to driver imported 
        cls.driver = driver
        cls.driver.maximize_window()
        
        # creating a loader object of excel document 
        loadobj = load_workbook(r"Selenium Training\August19\TestData.xlsx")
        # derive test sheet from loader object 
        cls.sheet = loadobj["LoginCases"]
        
        # object creation for login class to redirect to login page  
        cls.element = Login(cls.driver)
        cls.element.changeToLogin()
        logger.info("The setupclass is executed")
    
 
    def testLoginEmail(self):
        """
        This method has test cases for email field in web page 
        """
        # creating a object of login class to fetch elements 
        self.element = Login(self.driver)
        # getting email webelement 
        email = self.element.getEmail()

        # getting values from excel sheet from a particular column 
        for i in range(2,6):
            # clearing email field and entering the values 
            email.clear()
            email.send_keys(self.sheet.cell(row=i,column=2).value)
            time.sleep(3)
            # asserts statement for email field 
            self.assertEqual(email.get_attribute("value"),self.sheet.cell(row=i,column=2).value)
            self.assertTrue(email.is_displayed())
        logger.debug("The email  is tested")

    def testPassword(self):
        """
        This method has test cases for password field in web page 
        """
        # creating a object of login class to fetch elements 
        self.element = Login(self.driver)
        # getting password webelement 
        password = self.element.getPassword()

        # getting values from excel sheet from a particular column 
        for i in range(2,6):
            # clearing password field and entering the values 
            password.clear()
            password.send_keys(self.sheet.cell(row=i,column=2).value)
            time.sleep(3)
            # asserts statement for password field 
            self.assertEqual(password.get_attribute("value"),self.sheet.cell(row=i,column=2).value)
            self.assertTrue(password.is_displayed())
        logger.debug("The password is tested")


    def tearDown(self):
        """
        This method gets executed after each testcase 
        """
        # clearing the fields after every testcase 
        self.driver.find_element_by_id("email").clear()
        self.driver.find_element_by_id("password").clear()

if __name__=="__main__":
    test_load = unittest.TestLoader()
    t1 = test_load.loadTestsFromTestCase(TestLoginPage)
    suite = unittest.TestSuite()
    suite.addTests(t1)
    runner = HtmlTestRunner.HTMLTestRunner(output=r"Selenium Training\August25\html_dir")
    runner .run(suite)
