"""
This program contains the methods which returns webelements 
@author Rithvik 
"""


class Login:
    """
    This class contains methods whhich returns webelements of login 
    page 
    """

    def __init__(self,driver):
        # initializing a instance variable driver with driver parsed 
        self.driver = driver 

    def changeToLogin(self):
        # this method redirects the url to login page by clicking login button 
        self.driver.find_element_by_xpath("//a[text()='Log in']").click()

    def getEmail(self):
        # this method returns email field 
        email = self.driver.find_element_by_id("email")
        return email 

    def getPassword(self):
        # this method returns password field 
        password = self.driver.find_element_by_id("password")
        return password 

class Signup:
    """
    This class contains methods whhich returns webelements of login 
    page 
    """

    def __init__(self,driver):
        # initializing a instance variable driver with driver parsed 
        self.driver = driver 

    def changeToSignup(self):
        # this method redirects the url to Sign up page by clicking Sign up button 
        self.driver.find_element_by_xpath("//a[text()='Sign up']").click()

    def getDisplayName(self):
        # this method returns display name field 
        displayname  = self.driver.find_element_by_id("display-name")
        return displayname 

    def getEmail(self):
        # this method returns email field 
        email = self.driver.find_element_by_id("email")
        return email 
    
    def getPassword(self):
        # this method returns password field 
        password = self.driver.find_element_by_id("password")
        return password 