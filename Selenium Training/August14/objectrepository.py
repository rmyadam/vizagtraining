"""
This program is a replica of web element repository 
@author Rithvik 
"""

# importing the modules needed 
from selenium import webdriver
import time 


class TestData():
    """
    This class consists the methods which return the web objects 
    """

    # declaring a global variable 
    global driver
    options = webdriver.ChromeOptions() 
    options.add_experimental_option("excludeSwitches", ["enable-logging"])

    # creation of webdriver object and opening the site 
    driver = webdriver.Chrome(options=options, executable_path=r'C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\August14\chromedriver_win32\chromedriver.exe')
    driver.get("https://stackoverflow.com/users/login?ssrc=head&returnurl=https%3a%2f%2fstackoverflow.com%2f")
    driver.maximize_window()


    def getEmailId(self):
        """
        This method returns the email web element of web page  
        """
        # locating email web element from website 
        email = driver.find_element_by_id("email")
        # returing web element 
        return email

    def getPassword(self):
        """
        This method returns the password web element of web page 
        """
        # locating password web element of web page 
        password = driver.find_element_by_id("password")
        # returning web element
        return password