"""
This program uses excel file to get the test data and uses object repository 
to get web elements 
"""

# importing required modules
from openpyxl import *
from objectrepository import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time 
from selenium.webdriver.common.action_chains import ActionChains
import unittest


class TestAutomation(unittest.TestCase):

    def testStackOverflow(self):
        """
        This function gets the test data from a test case excel file and retrieves web elements 
        from object repository file and performs actions on fields in web page 
        """

        # creating a loader object by using openpyxl module to derive data from excel file 
        loadObj =  load_workbook(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\August14\testcase.xlsx")
        workbook = loadObj.active

        # locating the cell which contains email data 
        emailKeys = workbook.cell(row = 2,column =5)
        # locating the cell which contains password data 
        passwordKeys = workbook.cell(row=2,column=6)

        # creating the object of class TestData 
        data = TestData()

        # retrieving the web element(email field) from web page  
        email = data.getEmailId()
        # sending keys into email field 
        email.send_keys(str(emailKeys.value))
        time.sleep(3)

        # retrieving web element(password field) from web page
        password = data.getPassword()
        # sending keys into password field 
        password.send_keys(str(passwordKeys.value))
        time.sleep(3)
        
        # clicking the login button 
        driver.find_element_by_id('submit-button').click()
        time.sleep(6)

        # asserting the current url with login page url 
        self.assertNotEqual(driver.current_url,"https://stackoverflow.com/users/login?ssrc=head&returnurl=https%3a%2f%2fstackoverflow.com%2f")

        # quit the browser 
        driver.quit()

if __name__=="__main__":
    unittest.main()

