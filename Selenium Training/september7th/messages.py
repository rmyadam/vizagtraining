from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import time 
import unittest
from driver import driver 
from webelements import Messages
global driver 

class MessagesPage(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls.driver = driver 
        cls.element = Messages(cls.driver)
        cls.element.clickMessages()

    def testUser1(self):
        # entering user name in  searcch box 
        user  = self.element.connectionSearch()
        user.send_keys(self.element.messagesData(4,2))
        # list o user s is given 
        userList =  self.element.connectionList()
        # iterating through list aND
        for record in userList:
            if record.text ==self.element.messagesData(5,2):
                record.click()
        # MSG BIX RETURNED 
        box  = self.element.messageBox()
        box.send_keys(self.element.messagesData(6,2))
        
        # send button  is returned 
        self.element.sendBtn().click()

        self.element.clearConnectionSearch()


    
    def testUser2(self):
        # entering user name in  searcch box 
        user  = self.element.connectionSearch()
        user.send_keys(self.element.messagesData(10,2))
        # list o user s is given 
        userList =  self.element.connectionList()
        # iterating through list aND
        for record in userList:
            if record.text ==self.element.messagesData(11,2):
                record.click()
        # MSG BIX RETURNED 
        box  = self.element.messageBox()
        box.send_keys(self.element.messagesData(12,2))
        # send button  is returned 
        self.element.sendBtn().click()

        self.element.clearConnectionSearch()


    def testUser3(self):
        # entering user name in  searcch box 
        user  = self.element.connectionSearch()
        user.send_keys(self.element.messagesData(16,2))
        # list o user s is given 
        userList =  self.element.connectionList()
        # iterating through list aND
        for record in userList:
            if record.text ==self.element.messagesData(17,2):
                record.click()
        # MSG BIX RETURNED 
        box  = self.element.messageBox()
        box.send_keys(self.element.messagesData(18,2))
        # send button  is returned 
        self.element.sendBtn().click()

        self.element.clearConnectionSearch()



    @classmethod
    def tearDownClass(cls):
        cls.element.clickHome()


if __name__=="__main__":
    unittest.main()
