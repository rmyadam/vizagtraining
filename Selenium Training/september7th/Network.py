from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import time 
import unittest
from driver import driver 
from webelements import NetworkPage
global driver 

class Network(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls.driver = driver 
        cls.element = NetworkPage(cls.driver)
        cls.element.clickMyNetwork()



    def testUser(self):
        user = self.element.getUser()
        user.click()


        userId = self.element.getUserId()
        self.assertEqual(userId.text,self.element.networkData(3,2))
        

if __name__=="__main__":
     
    unittest.main()