from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time 
import unittest
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from driver import driver 
from webelements import Profile
global driver 

class Graduation(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        time.sleep(3)
        cls.driver = driver
        cls.element = Profile(cls.driver)
        cls.element.clickMyProfile()
        cls.element.addEducation()

    def testAddDetails(self):
        institute = self.element.enterInstitute()
        institute.send_keys(self.element.profileData(37,2))

        course = self.element.enterCourse()
        course.send_keys(self.element.profileData(38,2))

        stream = self.element.enterStream()
        stream.send_keys(self.element.profileData(39,2))

        instituteName = self.element.enterInstituteName()
        instituteName.send_keys(self.element.profileData(40,2))

        percent = self.element.enterPercent()
        percent.send_keys(str(self.element.profileData(41,2)).split('.')[0])

        self.element.confirmEducation()

    def testFields(self):
        qualification =  self.element.gradStream()
        self.assertEqual(qualification.text,self.element.profileData(43,2))

        level = self.element.getGraduation()
        self.assertEqual(level.text,self.element.profileData(44,2))

        status = self.element.gradStatus()

        self.assertEqual(status.text,self.element.profileData(45,2))

        department = self.element.gradDepartment()
        self.assertEqual(department.text,self.element.profileData(46,2))

        collegeName = self.element.gradCollege()
        self.assertEqual(collegeName.text,self.element.profileData(47,2))

        score = self.element.gradScore()
        self.assertEqual(score.text,self.element.profileData(48,2))


    @classmethod
    def tearDownClass(cls):
        time.sleep(3)
        cls.element.editEducation()
        cls.element.deleteEducation()


class Intermediate(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        time.sleep(3)
        cls.driver = driver
        cls.element = Profile(cls.driver)
        cls.element.clickMyProfile()
        cls.element.addEducation()

    def testAddDetails(self):
        institute = self.element.enterInstitute()
        institute.send_keys(self.element.profileData(53,2))

        course = self.element.enterCourse()
        course.send_keys(self.element.profileData(54,2))

        stream = self.element.enterStream()
        stream.send_keys(self.element.profileData(55,2))

        instituteName = self.element.enterInstituteName()
        instituteName.send_keys(self.element.profileData(56,2))

        percent = self.element.enterPercent()
        percent.send_keys(str(self.element.profileData(57,2)).split('.')[0])

        year = self.element.enterYear()
        year.send_keys(str(self.element.profileData(58,2)).split('.')[0])

        self.element.confirmEducation()


    
    def testFields(self):

        qualification =  self.element.gradStream()

        self.assertEqual(qualification.text,self.element.profileData(60,2))

        level = self.element.getGraduation()
        self.assertEqual(level.text,self.element.profileData(61,2))

        status = self.element.gradStatus()

        self.assertEqual(status.text,self.element.profileData(62,2))

        department = self.element.gradDepartment()


        self.assertEqual(department.text,self.element.profileData(63,2))

        collegeName = self.element.gradCollege()

        self.assertEqual(collegeName.text,self.element.profileData(64,2))

        score = self.element.gradScore()


        self.assertEqual(score.text,self.element.profileData(65,2))
    

    @classmethod
    def tearDownClass(cls):
        time.sleep(3)
        cls.element.editEducation()
        cls.element.deleteEducation()




class School(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        time.sleep(3)
        cls.driver = driver
        cls.element = Profile(cls.driver)
        cls.element.clickMyProfile()
        cls.element.addEducation()

    def testAddDetails(self):
        institute = self.element.enterInstitute()
        institute.send_keys(self.element.profileData(70,2))

        course = self.element.enterCourse()
        course.send_keys(self.element.profileData(71,2))

        stream = self.element.enterStream()
        stream.send_keys(self.element.profileData(72,2))

        instituteName = self.element.enterInstituteName()
        instituteName.send_keys(self.element.profileData(73,2))

        percent = self.element.enterPercent()
        percent.send_keys(str(self.element.profileData(74,2)).split('.')[0])

        year = self.element.enterYear()
        year.send_keys(str(self.element.profileData(75,2)).split('.')[0])

        self.element.confirmEducation()



    def testFields(self):

        stream =  self.element.gradStream()
        self.assertEqual(self.element.profileData(77,2),stream.text)

        level = self.element.getGraduation()
        self.assertEqual(self.element.profileData(78,2),level.text)

        status = self.element.gradStatus()
        self.assertEqual(self.element.profileData(79,2),status.text)

        department = self.element.gradDepartment()
        self.assertEqual(self.element.profileData(80,2),department.text)

        collegeName = self.element.gradCollege()

        self.assertEqual(self.element.profileData(81,2),collegeName.text)

        score = self.element.gradScore()

        self.assertEqual(self.element.profileData(82,2),score.text)

    @classmethod
    def tearDownClass(cls):
        time.sleep(3)
        cls.element.editEducation()
        cls.element.deleteEducation()
        cls.element.clickHome()


if __name__=="__main__":
    unittest.main()