from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time 
import unittest
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from driver import driver 
from webelements import Profile
global driver 


class Experience(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls.driver = driver
        cls.element = Profile(cls.driver)        
        cls.element.clickMyProfile()

    def testChangeExperience(self):
        self.element.addWork()
     
        company = self.element.getCompany()
        company.send_keys(self.element.profileData(4,2))
        role = self.element.getRole()
        role.send_keys(self.element.profileData(5,2))
        city = self.element.getCity()
        city.send_keys(self.element.profileData(6,2))
        year = self.element.getYear()
        year.send_keys(str(self.element.profileData(7,2)))

        description = self.element.getDescription()
        
        description.send_keys(self.element.profileData(8,2))
        delete = self.element.reject()
        delete.click()

    def testConfirmExperience(self):
        self.element.addWork()


        company = self.element.getCompany()
        company.send_keys(self.element.profileData(4,2))
        role = self.element.getRole()
        role.send_keys(self.element.profileData(5,2))
        city = self.element.getCity()
        city.send_keys(self.element.profileData(6,2))
        year = self.element.getYear()
        year.clear()

        year.send_keys(str(self.element.profileData(7,2)))

        description = self.element.getDescription()
        description.send_keys(self.element.profileData(8,2))

        check = self.element.confirm()
        check.click()


    def testExpereinceFields(self):
        companyName = self.element.getCompanyName()

        self.assertEqual(companyName.text,self.element.profileData(4,2))
        designation = self.element.getDesignation()


        self.assertEqual(designation.text,self.element.profileData(5,2))
        duration = self.element.getDuration()


        self.assertEqual(duration.text,str(self.element.profileData(7,2)).split('.')[0])
        description  = self.element.getDescriptionText()


        self.assertEqual(description.text,self.element.profileData(8,2))

    @classmethod
    def tearDownClass(cls):
        cls.element.editExperience()
        cls.element.deleteExperience()



class MySkills(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.driver = driver
        cls.element = Profile(cls.driver)
        cls.element.clickMyProfile()



    def testHeading(self):
        heading = self.element.getSkills()
        self.assertEqual(heading.text,self.element.profileData(12,2))

    def testChangeSkills(self):

        self.element.editSkills()
        skillInput  = self.element.skillInput()

        skillInput.click()
        skillInput.send_keys(Keys.CONTROL,"a")
        skillInput.send_keys(Keys.DELETE)
        skillInput.send_keys(self.element.profileData(13,2))
        self.element.confirmSkills()

    def testSkillsFields(self):
        skillsList = self.element.getSkillsList()
        self.assertListEqual(skillsList,self.element.profileData(14,2).split(','))

    
    
class Languages(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.driver = driver
        cls.element = Profile(cls.driver)
        cls.element.clickMyProfile()
        

    def testheading(self):
        heading = self.element.getLanguages()
        self.assertEqual(heading.text,self.element.profileData(18,2))


    def testLanguages(self):
        self.element.editLanguages()
        languages = self.element.enterLanguages()
        languages.click()
        languages.send_keys(Keys.CONTROL,"a")
        languages.send_keys(Keys.DELETE)
        languages.send_keys(self.element.profileData(19,2))
        self.element.confirmLanguages()

    def testLanguagesFields(self):
        languagesList = self.element.getLanguagesList()
        self.assertListEqual(languagesList,self.element.profileData(20,2).split(','))

class Intrests(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls.driver = driver
        cls.element = Profile(cls.driver)
        cls.element.clickMyProfile()
    
    def testHeading(self):
        heading = self.element.getIntrests()
        self.assertEqual(heading.text,self.element.profileData(24,2))

    def testIntrests(self):
        self.element.editIntrests()
        interests = self.element.enterInterests()
        interests.click()
        interests.send_keys(Keys.CONTROL,"a")
        interests.send_keys(Keys.DELETE)
        interests.send_keys(self.element.profileData(25,2))
        self.element.confirmInterests()
    
    def testIntrestsFields(self):
        IntrestsList = self.element.getInterestsList()
        self.assertListEqual(IntrestsList,self.element.profileData(26,2).split(','))

class BioDetails(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls.driver = driver
        cls.element = Profile(cls.driver)
        cls.element.clickMyProfile()
        cls.element.editBio()


    def testChangeDetails(self):
        firstName  = self.element.enterFirstName()
        firstName.clear()
        firstName.send_keys(self.element.profileData(29,2))

        lastName = self.element.enterLastName()
        lastName.clear()

        lastName.send_keys(self.element.profileData(30,2))
        
        bio = self.element.enterBio()
        bio.clear()
        bio.send_keys(self.element.profileData(31,2))

        location = self.element.enterLocation()
        location.clear()
        location.send_keys(self.element.profileData(32,2))


        self.element.confirmBio()

    
    def testDetails(self):

        userName = self.element.getUserName()

        self.assertEqual(userName.text,self.element.profileData(33,2))
        bio = self.element.getuserBio()
        self.assertEqual(bio.text,self.element.profileData(31,2))
        location = self.element.getUserLocation()
        self.assertEqual(location.text,self.element.profileData(32,2))
        
        

if __name__=="__main__":
   
    unittest.main()