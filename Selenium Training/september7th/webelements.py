from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time 
import unittest
from driver import *
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By 


class Navigation:
    def __init__(self,driver):
        self.driver = driver 
        self.wait = WebDriverWait(self.driver,5)

    def clickHome(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//span[text()="Home"]')))
        self.driver.find_element_by_xpath('//span[text()="Home"]').click()

    def clickWebinars(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//span[text()="Webinars"]')))
        self.driver.find_element_by_xpath('//span[text()="Webinars"]').click()

    def clickMyNetwork(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//span[text()="My Network"]')))
        self.driver.find_element_by_xpath('//span[text()="My Network"]').click()

    def clickArticles(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//span[text()="Articles"]')))
        self.driver.find_element_by_xpath('//span[text()="Articles"]').click()

    def clickMessages(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//span[text()="Messages"]')))
        self.driver.find_element_by_xpath('//span[text()="Messages"]').click()

    def clickMyProfile(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//span[text()="My Profile"]')))
        self.driver.find_element_by_xpath('//span[text()="My Profile"]').click()

    def clickSignOut(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//span[text()="Sign out"]')))
        self.driver.find_element_by_xpath('//span[text()="Sign out"]').click()
        
    def closeSignOut(self):
        self.wait.until(EC.presence_of_element_located((By.CSS_SELECTOR,'.\_modal-close-icon')))
        self.driver.find_element_by_css_selector('.\_modal-close-icon').click()


class Home(Navigation):
    
    def __init__(self,driver):
        self.driver = driver 
        self.wait = WebDriverWait(self.driver,10)

    def homeData(self,row,column):
        self.sheet = loadObject["HomeData"]
        return self.sheet.cell(row = row,column = column).value
    
    def webinar1(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,"//span[text()='New Webinars']//ancestor::div[2]//child::img[1]")))
        webinar = self.driver.find_element_by_xpath("//span[text()='New Webinars']//ancestor::div[2]//child::img[1]")
        return webinar

    def webinar2(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,"(//IMG[@src='/static/media/drive_default.cfa224d5.png'])[2]")))
        webinar = self.driver.find_element_by_xpath("(//IMG[@src='/static/media/drive_default.cfa224d5.png'])[2]")
        return webinar

    def college1(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[@id="DriveDetailText-id--heading"]')))
        college = self.driver.find_element_by_xpath('//p[@id="DriveDetailText-id--heading"]')
        return college

    def college2(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[@id="DriveDetailText-id--heading"]')))
        college = self.driver.find_element_by_xpath('//p[@id="DriveDetailText-id--heading"]')
        return college

    def registeredWebinars(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'(//A[@href="/drives"][text()="View All"])[2]')))
        self.driver.find_element_by_xpath('(//A[@href="/drives"][text()="View All"])[2]').click()

    def registeredCollege(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'(//p[@class="Drive-Heading-Text"][text()="Vidya Jyothi Institute of Technology"])[1]')))
        college = self.driver.find_element_by_xpath('(//p[@class="Drive-Heading-Text"][text()="Vidya Jyothi Institute of Technology"])[1]')
        return college 

    def registeredYear(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'(//p[@class="Drive-Subheading-Text"][text()="First Year, B.Tech CSE"])[1]')))
        year = self.driver.find_element_by_xpath('(//p[@class="Drive-Subheading-Text"][text()="First Year, B.Tech CSE"])[1]')
        return year

    def activeWebinars(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'(//div[@class="dashboard-section-heading-container flex-container-row"]//child::a[@href="/drives"])[1]')))
        self.driver.find_element_by_xpath('(//div[@class="dashboard-section-heading-container flex-container-row"]//child::a[@href="/drives"])[1]').click()
    
    def activeColleges(self):
        list1 = []
        time.sleep(3)
        names = self.driver.find_elements_by_xpath("//p[@class='Drive-Heading-Text']")[1:]
        for i in names:
            list1.append(i.text)
        return list1 

    def viewNetwork(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'(//a[@class="viewall-link"][@href="/network"])[1]')))
        self.driver.find_element_by_xpath('(//a[@class="viewall-link"][@href="/network"])[1]').click()

    def clickRequest(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,"(//IMG[@src='/static/media/default-user-new.dc6cdc7f.jpg'])[2]")))
        self.driver.find_element_by_xpath("(//IMG[@src='/static/media/default-user-new.dc6cdc7f.jpg'])[2]").click()

    def requestUser(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,"//P[@id='UserName']")))
        user = self.driver.find_element_by_xpath("//P[@id='UserName']")
        return user 

    def getConnections(self):
        time.sleep(3)
        connections = self.driver.find_elements_by_xpath('//h6[@class="sender-name"]')[1:]
        list1 = []
        for record in connections:
            list1.append(record.text)
        return list1

    def searchBox(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//input[@class="_hj0pr4"]')))
        search = self.driver.find_element_by_xpath('//input[@class="_hj0pr4"]')
        return search


    def clickPuranProfile(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[text()="Puran Kalapala"]')))
        self.driver.find_element_by_xpath('//p[text()="Puran Kalapala"]').click()


    def getPuranName(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,"//p[@id='UserName']")))
        name1 = self.driver.find_element_by_xpath("//p[@id='UserName']")
        return name1


    def getPuranBio(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,"//p[@id='UserBio']")))
        bio = self.driver.find_element_by_xpath("//p[@id='UserBio']")
        return bio 


    def getPuranAddress(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,"//p[@id='UserAddress']")))
        address = self.driver.find_element_by_xpath("//p[@id='UserAddress']")
        return address

    def clickSubhasisProfile(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[text()="subhasis chand"]')))
        self.driver.find_element_by_xpath('//p[text()="subhasis chand"]').click()


    def getSubhasisName(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,"//p[@id='UserName']")))
        name2 = self.driver.find_element_by_xpath("//p[@id='UserName']")
        return name2


    def getProfileName(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,"//p[@id='UserName']")))
        userName = self.driver.find_element_by_xpath('//p[@id="UserName"]')
        return userName


    def getProfileBio(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,"//p[@id ='UserBio']")))
        bio = self.driver.find_element_by_xpath('//p[@id ="UserBio"]')
        return bio


    def getProfileLocation(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,"//p[@id ='UserAddress']")))
        location = self.driver.find_element_by_xpath('//p[@id ="UserAddress"]')
        return location

    def getProfile(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,"//div[@class='_mb51i']//child::img")))
        self.driver.find_element_by_xpath("//div[@class='_mb51i']//child::img").click()





class Articles(Navigation):

    def __init__(self,driver):
        self.driver = driver 
        self.wait = WebDriverWait(self.driver,5)

    def articlesData(self,row,column):
        self.sheet = loadObject["ArticlesData"]
        return self.sheet.cell(row = row,column = column).value

    def articleText(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[@class="article-heading-text"]')))
        text = self.driver.find_element_by_xpath('//p[@class="article-heading-text"]')
        return text 


    def viewArticles(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'(//span[text()="Articles"]//following-sibling::a[@href="/articles"])[1]')))
        self.driver.find_element_by_xpath('(//span[text()="Articles"]//following-sibling::a[@href="/articles"])[1]').click()
        
    def articleHeading(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//div[@class="articles-heading-text"]')))
        heading = self.driver.find_element_by_xpath('//div[@class="articles-heading-text"]')
        return heading 
        
    def articleSubHeading(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//div[@class="row article-brief-wrapper"]//child::p')))
        subHeading = self.driver.find_element_by_xpath('//div[@class="row article-brief-wrapper"]//child::p')
        return subHeading 

    
    def publisherText(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[@class="article-publisher-text"]')))
        text = self.driver.find_element_by_xpath('//p[@class="article-publisher-text"]')
        return text 

    def stampInfoText(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[@class="article-timestamp-text"]')))
        stamp  = self.driver.find_element_by_xpath('//p[@class="article-timestamp-text"]')
        return stamp 



    
    def articleText(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[@class="article-heading-text"]')))
        text = self.driver.find_element_by_xpath('//p[@class="article-heading-text"]')
        return text 

    def viewAllArticles(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,"//A[@class='archivelist-link'][text()='All articles']")))
        articleBtn  = self.driver.find_element_by_xpath("//A[@class='archivelist-link'][text()='All articles']")
        return articleBtn 

    


class Messages(Navigation):

    def __init__(self,driver):
        self.driver = driver 
        self.wait = WebDriverWait(self.driver,5)

    def messagesData(self,row,column):
        self.sheet = loadObject["MessagesData"]
        return self.sheet.cell(row = row,column = column).value
    

    def connectionSearch(self):
        self.wait.until(EC.presence_of_element_located((By.CLASS_NAME,"searchfriend-text-input")))
        user = self.driver.find_element_by_class_name("searchfriend-text-input")
        return user 

    def connectionList(self):
        time.sleep(3)
        llist1 =  self.driver.find_elements_by_class_name("sender-name")
        return llist1

    def messageBox(self):
        self.wait.until(EC.presence_of_element_located((By.NAME,"message")))
        box = self.driver.find_element_by_name("message")
        return box

    def sendBtn(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,"//BUTTON[@tabindex='0'][text()='Send']")))
        button = self.driver.find_element_by_xpath("//BUTTON[@tabindex='0'][text()='Send']")
        return button 

    def clearConnectionSearch(self):
        self.wait.until(EC.presence_of_element_located((By.CLASS_NAME,"searchfriend-text-input")))
        self.driver.find_element_by_class_name("searchfriend-text-input").clear()

class SignOut(Navigation):
    
    def __init__(self,driver):
        self.driver = driver 
        self.wait = WebDriverWait(self.driver,5)
        
    def getalertText(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[text()="Are you sure you want to sign out now?"]')))
        text = self.driver.find_element_by_xpath('//p[text()="Are you sure you want to sign out now?"]').text
        return text

    def closeBtn(self):
        self.wait.until(EC.presence_of_element_located((By.CSS_SELECTOR,'.\_modal-close-icon')))
        btn = self.driver.find_element_by_css_selector('.\_modal-close-icon')
        return btn

class WebinarPage(Navigation):

    def __init__(self,driver):
        self.driver = driver 
        self.wait = WebDriverWait(self.driver,5)

    def webinarData(self,row,column):
        self.sheet = loadObject["WebinarData"]
        return self.sheet.cell(row = row,column = column).value


    def clickWebinar2(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,"//A[@href='/drives/5eb108db577cc16e3ee2fd50'][text()='Read More..']")))
        self.driver.find_element_by_xpath("//A[@href='/drives/5eb108db577cc16e3ee2fd50'][text()='Read More..']").click()
    
    def clickWebinar1(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,"//A[@href='/drives/5eb682c9577cc16e3ee2fd93'][text()='Read More..']")))
        self.driver.find_element_by_xpath("//A[@href='/drives/5eb682c9577cc16e3ee2fd93'][text()='Read More..']").click()
        
    def clickWebinar3(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//a[text()="View Details!"]')))
        self.driver.find_element_by_xpath('//a[text()="View Details!"]').click()
        


    def getFacultyName(self): 
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//td[text()="Faculty name:"]//following-sibling::td')))
        name = self.driver.find_element_by_xpath('//td[text()="Faculty name:"]//following-sibling::td')
        return name 

    def getFacultyEmail(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//td[text()="Faculty member email:"]//following-sibling::td')))
        email = self.driver.find_element_by_xpath('//td[text()="Faculty member email:"]//following-sibling::td')
        return email 
    
    def getPhoneNo(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//td[text()="Phone no:"]//following-sibling::td')))
        phoneNo = self.driver.find_element_by_xpath('//td[text()="Phone no:"]//following-sibling::td')
        return phoneNo

    def getSessionUrl(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//td[text()="Webinar URL:"]//following-sibling::td')))
        url = self.driver.find_element_by_xpath('//td[text()="Webinar URL:"]//following-sibling::td')
        return url

    def getCollegeName(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,"//P[@id='DriveDetailText-id--heading']")))
        college = self.driver.find_element_by_xpath("//P[@id='DriveDetailText-id--heading']")
        return college

    def getYear(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,"//P[@id='DriveDetailText-id--drivesubheading']")))
        year = self.driver.find_element_by_xpath("//P[@id='DriveDetailText-id--drivesubheading']")
        return year

    def WebinarDetails(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'(//p[text()="Webinar Details"]//ancestor::section//child::p)[2]')))
        details = self.driver.find_element_by_xpath('(//p[text()="Webinar Details"]//ancestor::section//child::p)[2]')
        return details
    
    def PreSessionDetails(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'(//p[text()="Pre-session preparation"]//ancestor::section//child::p)[2]')))
        prerequisite = self.driver.find_element_by_xpath('(//p[text()="Pre-session preparation"]//ancestor::section//child::p)[2]')
        return prerequisite        
    
    def Reference(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'(//p[text()="Reference Books or Material"]//ancestor::section//child::p)[2]')))
        reference = self.driver.find_element_by_xpath('(//p[text()="Reference Books or Material"]//ancestor::section//child::p)[2]')
        return reference
        

class Profile(Navigation):

    def __init__(self,driver):
        self.driver = driver 
        self.wait = WebDriverWait(self.driver,10)

    def profileData(self,row,column):
        self.sheet = loadObject["ProfileData"]
        return self.sheet.cell(row = row,column = column).value

    def addWork(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//section[@class="MyProfileSection WorkExperienceSection"]//child::span[@class="fa fa-plus"]')))
        self.driver.find_element_by_xpath('//section[@class="MyProfileSection WorkExperienceSection"]//child::span[@class="fa fa-plus"]').click()

        
    def getCompany(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//input[@id="userexpsection__text--company--edit"]')))

        company = self.driver.find_element_by_xpath('//input[@id="userexpsection__text--company--edit"]')
        return company

    def getRole(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//input[@id="userexpsection__text--designation"]')))
        role = self.driver.find_element_by_xpath('//input[@id="userexpsection__text--designation"]')
        return role

    def getCity(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//input[@id="userexpsection__text--location"]')))
        city = self.driver.find_element_by_xpath('//input[@id="userexpsection__text--location"]')
        return city

    def getYear(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//input[@id="userexpsection__text--duration"]')))
        year = self.driver.find_element_by_xpath('//input[@id="userexpsection__text--duration"]')
        return year

    def getDescription(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//input[@id="userexpsection__text--description"]')))
        description = self.driver.find_element_by_xpath('//input[@id="userexpsection__text--description"]')
        return description

    def reject(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//section[@class="MyProfileSection WorkExperienceSection"]//child::span[@class="fa fa-times UserContactDetailText--editicon"]')))
        reject = self.driver.find_element_by_xpath('//section[@class="MyProfileSection WorkExperienceSection"]//child::span[@class="fa fa-times UserContactDetailText--editicon"]')
        return reject

    def confirm(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//section[@class="MyProfileSection WorkExperienceSection"]//child::span[@class="fa fa-check UserContactDetailText--editicon"]')))
        check = self.driver.find_element_by_xpath('//section[@class="MyProfileSection WorkExperienceSection"]//child::span[@class="fa fa-check UserContactDetailText--editicon"]')
        return check

    def getCompanyName(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[@class="WorkExperienceSectionHeading"]//parent::div//following::div//child::p[@id="userexpsection__text--company"]')))
        companyName = self.driver.find_element_by_xpath('//p[@class="WorkExperienceSectionHeading"]//parent::div//following::div//child::p[@id="userexpsection__text--company"]')
        return companyName

    def getDesignation(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[@class="WorkExperienceSectionHeading"]//parent::div//following::div//child::p[@id="userexpsection__text--designation"]')))
        designation = self.driver.find_element_by_xpath('//p[@class="WorkExperienceSectionHeading"]//parent::div//following::div//child::p[@id="userexpsection__text--designation"]')
        return designation

    def getDuration(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[@class="WorkExperienceSectionHeading"]//parent::div//following::div//child::p[@id="userexpsection__text--duration"]')))
        duration = self.driver.find_element_by_xpath('//p[@class="WorkExperienceSectionHeading"]//parent::div//following::div//child::p[@id="userexpsection__text--duration"]')
        return duration

    def getDescriptionText(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[@class="WorkExperienceSectionHeading"]//parent::div//following::div//child::p[@id="userexpsection__text--description"]')))
        duration = self.driver.find_element_by_xpath('//p[@class="WorkExperienceSectionHeading"]//parent::div//following::div//child::p[@id="userexpsection__text--description"]')
        return duration

    def editExperience(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//section[@class="MyProfileSection WorkExperienceSection"]//child::span[@class="fa fa-pencil-square"]')))
        self.driver.find_element_by_xpath('//section[@class="MyProfileSection WorkExperienceSection"]//child::span[@class="fa fa-pencil-square"]').click()

    def deleteExperience(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//section[@class="MyProfileSection WorkExperienceSection"]//child::span[@class="fa fa-trash UserContactDetailText--editicon"]')))
        self.driver.find_element_by_xpath('//section[@class="MyProfileSection WorkExperienceSection"]//child::span[@class="fa fa-trash UserContactDetailText--editicon"]').click()

    def getSkills(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//section[@class="MyProfileSection"]//child::p[text()="My Skills"]')))
        skills = self.driver.find_element_by_xpath('//section[@class="MyProfileSection"]//child::p[text()="My Skills"]')
        return skills

    def editSkills(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//section[@class="MyProfileSection"]//child::p[text()="My Skills"]')))
        self.driver.find_element_by_xpath('(//span[@class="fa fa-pencil-square"][@id="UserInterestsText--editicon"])[1]').click()

    def skillInput(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//section[@class="MyProfileSection"]//child::input[@id="interests__editfield"]')))
        input = self.driver.find_element_by_xpath('//section[@class="MyProfileSection"]//child::input[@id="interests__editfield"]')
        return input 

    def confirmSkills(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//section[@class="MyProfileSection"]//child::span[@class="fa fa-check"]')))
        self.driver.find_element_by_xpath('//section[@class="MyProfileSection"]//child::span[@class="fa fa-check"]').click()

    def getSkillsList(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,"//p[@class='SkillsetSectionHeading'][text()='My Skills']//parent::div//following-sibling::ul//child::li")))
        objectList = self.driver.find_elements_by_xpath("//p[@class='SkillsetSectionHeading'][text()='My Skills']//parent::div//following-sibling::ul//child::li")
        skillsList = []
        for record in objectList:
            skillsList.append(record.text)
        return skillsList

    def getLanguages(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,"//P[@class='KnownLanguagesSectionHeading'][text()='Languages']")))
        heading = self.driver.find_element_by_xpath("//P[@class='KnownLanguagesSectionHeading'][text()='Languages']")
        return heading

    def editLanguages(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,"//p[text()='Languages']//parent::div//child::span")))
        self.driver.find_element_by_xpath('//p[text()="Languages"]//parent::div//child::span').click()

    def enterLanguages(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,"//p[text()='Languages']//parent::div//following-sibling::div//child::input")))
        languages = self.driver.find_element_by_xpath('//p[text()="Languages"]//parent::div//following-sibling::div//child::input')
        return languages

    def confirmLanguages(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,"//p[text()='Languages']//parent::div//following-sibling::div//child::input")))
        self.driver.find_element_by_xpath('//section[@class="MyProfileSection"]//child::span[@class="fa fa-check"]').click()
        
    def getLanguagesList(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,"//p[text()='Languages']//parent::div/following-sibling::ul/child::li")))

        objectList = self.driver.find_elements_by_xpath('//p[text()="Languages"]//parent::div/following-sibling::ul/child::li')
        languagesList = []
        for record in objectList:
            languagesList.append(record.text)
        return languagesList

    def getIntrests(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,"//P[@class='InterestsSectionHeading'][text()='Interests']")))
        heading = self.driver.find_element_by_xpath("//P[@class='InterestsSectionHeading'][text()='Interests']")
        return heading

    def editIntrests(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[text()="Interests"]//parent::div//child::span')))
        self.driver.find_element_by_xpath('//p[text()="Interests"]//parent::div//child::span').click()

    def enterInterests(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[text()="Interests"]//parent::div//following-sibling::div//child::input')))
        interests = self.driver.find_element_by_xpath('//p[text()="Interests"]//parent::div//following-sibling::div//child::input')
        return interests

    def confirmInterests(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//section[@class="MyProfileSection"]//child::span[@class="fa fa-check"]')))
        
        self.driver.find_element_by_xpath('//section[@class="MyProfileSection"]//child::span[@class="fa fa-check"]').click()

    def getInterestsList(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[text()="Interests"]//parent::div/following-sibling::ul/child::li')))
    
        objectList = self.driver.find_elements_by_xpath('//p[text()="Interests"]//parent::div/following-sibling::ul/child::li')
        IntrestsList = []
        for record in objectList:
            IntrestsList.append(record.text)  
        return IntrestsList 

    def editBio(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//span[@class="fa fa-pencil-square UserContactDetailText--editicon"]')))
        self.driver.find_element_by_xpath('//span[@class="fa fa-pencil-square UserContactDetailText--editicon"]').click()

    def enterFirstName(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//input[@id ="userfirstname__editfield"]')))
        firstName = self.driver.find_element_by_xpath('//input[@id ="userfirstname__editfield"]')
        return firstName

    def enterLastName(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//input[@id ="userlastname__editfield"]')))
        lastName = self.driver.find_element_by_xpath('//input[@id ="userlastname__editfield"]')
        return lastName 

    def enterBio(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//input[@id ="UserBio__editfield"]')))
        bio = self.driver.find_element_by_xpath("//INPUT[@id='UserBio__editfield']")
        return bio 

    def enterLocation(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//input[@id ="UserAddress__editfield"]')))
        address = self.driver.find_element_by_xpath('//input[@id ="UserAddress__editfield"]')
        return address


    def confirmBio(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//span[@class="fa fa-check UserContactDetailText--editicon"]')))        
        self.driver.find_element_by_xpath('//span[@class="fa fa-check UserContactDetailText--editicon"]').click()

    def getUserName(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[@id="UserName"]')))        
        userName = self.driver.find_element_by_xpath('//p[@id="UserName"]')
        return userName

    def getuserBio(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[@id="UserBio"]')))        
        bio = self.driver.find_element_by_xpath('//p[@id ="UserBio"]')
        return bio

    def getUserLocation(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[@id="UserAddress"]'))) 
        location = self.driver.find_element_by_xpath('//p[@id ="UserAddress"]')
        return location

    def addEducation(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[text()="Education"]//parent::div//following::span[@class="fa fa-plus"]'))) 
        self.driver.find_element_by_xpath('//p[text()="Education"]//parent::div//following::span[@class="fa fa-plus"]').click()

    def enterInstitute(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//input[@id="usereducationsection__text--desc"]'))) 
        institute = self.driver.find_element_by_xpath('//input[@id="usereducationsection__text--desc"]')
        return institute

    def enterCourse(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//input[@id="usereducationsection__text--schoolboard"]'))) 
        course = self.driver.find_element_by_xpath('//input[@id="usereducationsection__text--schoolboard"]')
        return course

    def enterStream(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//input[@id="usereducationsection__text--department"]'))) 
        stream = self.driver.find_element_by_xpath('//input[@id="usereducationsection__text--department"]')
        return stream 

    def enterInstituteName(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//input[@id="usereducationsection__text--schoolname"]'))) 
        InsName = self.driver.find_element_by_xpath('//input[@id="usereducationsection__text--schoolname"]')
        return InsName

    def enterPercent(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//input[@placeholder="Your CGPA or P"]'))) 
        percent = self.driver.find_element_by_xpath('//input[@placeholder="Your CGPA or P"]')
        return percent 

    def enterYear(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//input[@placeholder="Your CGPA or P"]'))) 
        self.driver.find_element_by_xpath("//INPUT[@id='usereducationsection__text--duration']").clear()

        year = self.driver.find_element_by_xpath("//INPUT[@id='usereducationsection__text--duration']")
        return year


    def confirmEducation(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//span[@class="fa fa-check UserContactDetailText--editicon"]'))) 
        self.driver.find_element_by_xpath('//span[@class="fa fa-check UserContactDetailText--editicon"]').click()

    def editEducation(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[@id="usereducationsection__text--graduationtext"]//parent::div//following-sibling::span'))) 
        self.driver.find_element_by_xpath('//p[@id="usereducationsection__text--graduationtext"]//parent::div//following-sibling::span').click()

    def deleteEducation(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[text()="Education"]//ancestor::section//child::span[@class="fa fa-trash UserContactDetailText--editicon"]'))) 
        self.driver.find_element_by_xpath('//p[text()="Education"]//ancestor::section//child::span[@class="fa fa-trash UserContactDetailText--editicon"]').click()

    def gradStream(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[@class="EducationSectionHeading"]//ancestor::section//child::p[@id="usereducationsection__text--graduationtext"]'))) 
        stream = self.driver.find_element_by_xpath('//p[@class="EducationSectionHeading"]//ancestor::section//child::p[@id="usereducationsection__text--graduationtext"]')
        return stream

    def getGraduation(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[@class="EducationSectionHeading"]//ancestor::section//child::p[@id="usereducationsection__text--desc"]'))) 
        level = self.driver.find_element_by_xpath('//p[@class="EducationSectionHeading"]//ancestor::section//child::p[@id="usereducationsection__text--desc"]')
        return level 
    
    def gradStatus(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[@class="EducationSectionHeading"]//ancestor::section//child::p[@id="usereducationsection__text--schoolboard"]'))) 
        status = self.driver.find_element_by_xpath('//p[@class="EducationSectionHeading"]//ancestor::section//child::p[@id="usereducationsection__text--schoolboard"]')
        return status

    def gradDepartment(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[@class="EducationSectionHeading"]//ancestor::section//child::p[@id="usereducationsection__text--department"]')))
        department = self.driver.find_element_by_xpath('//p[@class="EducationSectionHeading"]//ancestor::section//child::p[@id="usereducationsection__text--department"]')
        return department 

    def gradCollege(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[@class="EducationSectionHeading"]//ancestor::section//child::p[@id="usereducationsection__text--schoolname"]')))
        collegeName = self.driver.find_element_by_xpath('//p[@class="EducationSectionHeading"]//ancestor::section//child::p[@id="usereducationsection__text--schoolname"]')
        return collegeName

    def gradScore(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[@class="EducationSectionHeading"]//ancestor::section//child::p[@id="usereducationsection__text--duration"]')))
        score = self.driver.find_element_by_xpath('//p[@class="EducationSectionHeading"]//ancestor::section//child::p[@id="usereducationsection__text--duration"]')
        return score

class NetworkPage(Navigation):
    def __init__(self,driver):
        self.driver = driver 
        self.wait = WebDriverWait(self.driver,5)

    def networkData(self,row,column):
        self.sheet = loadObject["NetworkData"]
        return self.sheet.cell(row = row,column = column).value

    def getUser(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//a[text()="BhavaniReddy"]')))
        user  = self.driver.find_element_by_xpath('//a[text()="BhavaniReddy"]')
        return user 
    
    def getUserId(self):
        self.wait.until(EC.presence_of_element_located((By.XPATH,'//p[@id="UserName"]')))
        name = self.driver.find_element_by_xpath('//p[@id="UserName"]')
        return name






    