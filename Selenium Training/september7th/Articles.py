from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time 
import unittest
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By 
from driver import driver 
from webelements import Articles
global driver 


class ArticlePage(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls.driver = driver 
        cls.element = Articles(cls.driver)
        cls.element.viewArticles()

    def testArticles(self):
        self.element.articleHeading().click()
        
        heading  = self.element.articleText()    
        self.assertEqual(heading.text,self.element.articlesData(4,2))
        
        publishInfo = self.element.publisherText()
        self.assertEqual(publishInfo.text,self.element.articlesData(5,2))

        stampInfo = self.element.stampInfoText()
        self.assertEqual(stampInfo.text,self.element.articlesData(6,2))


        subHeading = self.element.articleSubHeading()
        self.assertEqual(subHeading.text,self.element.articlesData(7,2))


        self.element.viewAllArticles().click()

    @classmethod
    def tearDownClass(cls):
        cls.element.clickHome()


if __name__=="__main__":
    unittest.main()