from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time 
import unittest
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By 
from driver import * 
from webelements import WebinarPage
global driver,loadObject



class Webinar2(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls.driver = driver
        cls.element = WebinarPage(cls.driver)
        cls.element.clickWebinars()
        cls.element.clickWebinar2()
        

    def testBasicInfo(self):

        facultyName = self.element.getFacultyName().text
        self.assertEqual(facultyName,self.element.webinarData(5,2))

        facultyEmail= self.element.getFacultyEmail().text
        self.assertEqual(facultyEmail,self.element.webinarData(6,2))
        
        phoneNo = self.element.getPhoneNo().text
        self.assertEqual(phoneNo,str(self.element.webinarData(7,2)).split('.')[0])

        webUrl = self.element.getSessionUrl().text 
        self.assertEqual(webUrl,self.element.webinarData(8,2))

    def testCollegeName(self):
        college = self.element.getCollegeName().text
        self.assertEqual(college,self.element.webinarData(9,2))
        year = self.element.getYear().text
        self.assertEqual(year,self.element.webinarData(10,2))
        


    def testWebinarDetails(self):
        details = self.element.WebinarDetails().text
        self.assertMultiLineEqual(details,self.element.webinarData(11,2))


    def testPresession(self):
        prerequisite = self.element.PreSessionDetails().text
        self.assertMultiLineEqual(prerequisite,self.element.webinarData(12,2))
    
    def testReference(self):
        reference = self.element.Reference().text
        self.assertMultiLineEqual(reference,self.element.webinarData(13,2))


    @classmethod
    def tearDownClass(cls):
        cls.element.clickHome()

class Webinar1(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.driver = driver
        cls.element = WebinarPage(cls.driver)
        cls.element.clickWebinars()
        cls.element.clickWebinar1()

    def testBasicInfo(self):

        facultyName = self.element.getFacultyName().text
        self.assertEqual(facultyName,self.element.webinarData(17,2))

        facultyEmail= self.element.getFacultyEmail().text
        self.assertEqual(facultyEmail,self.element.webinarData(18,2))

        phoneNo = self.element.getPhoneNo().text 
        self.assertEqual(phoneNo,str(self.element.webinarData(19,2)).split('.')[0])
    

    def testCollegeName(self):
        college = self.element.getCollegeName().text
        self.assertEqual(college,self.element.webinarData(21,2))


        year = self.element.getYear().text


        self.assertEqual(year,self.element.webinarData(22,2))

    
    def testWebinarDetails(self):
        details = self.element.WebinarDetails().text
        self.assertMultiLineEqual(details,self.element.webinarData(23,2))

    def testPresession(self):
        prerequisite = self.element.PreSessionDetails().text
        self.assertMultiLineEqual(prerequisite,self.element.webinarData(24,2))
    

    def testReference(self):
        reference = self.element.Reference().text
        self.assertMultiLineEqual(reference,self.element.webinarData(25,2))


    @classmethod
    def tearDownClass(cls):
        cls.element.clickHome()

class Webinar3(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls.driver = driver
        cls.element = WebinarPage(cls.driver)
        cls.element.clickWebinars()
        cls.element.clickWebinar3()
      

    def testBasicInfo(self):
        facultyName = self.element.getFacultyName().text
        self.assertEqual(facultyName,self.element.webinarData(29,2))


        facultyEmail= self.element.getFacultyEmail().text
        self.assertEqual(facultyEmail,self.element.webinarData(30,2))


        phoneNo = self.element.getPhoneNo().text
        self.assertEqual(phoneNo,str(self.element.webinarData(31,2)).split('.')[0])


        webUrl = self.element.getSessionUrl().text 
        self.assertEqual(webUrl,self.element.webinarData(32,2))


    def testCollegeName(self):

        college = self.element.getCollegeName().text
        self.assertEqual(college,self.element.webinarData(33,2))

        year = self.element.getYear().text
        self.assertEqual(year,self.element.webinarData(34,2))
    
    def testWebinarDetails(self):
        details = self.element.WebinarDetails().text
        self.assertMultiLineEqual(details,self.element.webinarData(35,2))

    def testPresession(self):
        prerequisite = self.element.PreSessionDetails().text
        self.assertMultiLineEqual(prerequisite,self.element.webinarData(36,2))
    
    def testReference(self):
        reference = self.element.Reference().text

        self.assertMultiLineEqual(reference,self.element.webinarData(37,2))

    @classmethod
    def tearDownClass(cls):
        cls.element.clickHome()

if __name__=="__main__":
    unittest.main()