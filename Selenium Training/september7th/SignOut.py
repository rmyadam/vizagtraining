from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time 
import unittest
from driver import driver 
from webelements import SignOut
global driver 


class SignOutNavigation(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls.driver = driver 
        cls.element = SignOut(cls.driver)
        
        
    def testSign(self):        
        time.sleep(3)
        self.element.clickSignOut()
        time.sleep(3)

        alertText = self.element.getalertText()
        self.assertEqual(alertText,"Are you sure you want to sign out now?")
        
        self.element.closeBtn().click()


if __name__=="__main__": 
    unittest.main()