from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time 
import unittest

class Profile(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome(r"Selenium Training\september7th\chromedriver_win32\chromedriver.exe")
        cls.driver.get("https://www.yoaspire.com/")
        cls.driver.find_element_by_id("input_5").send_keys("rmyadam@innominds.com")
        cls.driver.find_element_by_id('input_6').send_keys("Python@123")
        cls.driver.find_element_by_xpath('//button[@class="btn signin-btn"][text()="Sign in"]').click()
        time.sleep(3)
        cls.driver.find_element_by_xpath('//span[text()="My Profile"]').click()

    def testGradDetails(self):
        time.sleep(3)
        degree = self.driver.find_element_by_xpath("//P[@id='usereducationsection__text--desc']").text
        self.assertEqual("Bachelor of Engineering",degree)
        degreeType = self.driver.find_element_by_xpath("//P[@id='usereducationsection__text--schoolboard']").text
        self.assertEqual("Completed Btech",degreeType)
        stream = self.driver.find_element_by_xpath("//P[@id='usereducationsection__text--department']").text
        self.assertEqual("In Electrical and Electronics",stream)
        collegeName = self.driver.find_element_by_xpath("//P[@id='usereducationsection__text--schoolname']").text
        self.assertEqual("At Vignana Bharathi Institute of Technology",collegeName)
        score = self.driver.find_element_by_xpath("//P[@id='usereducationsection__text--duration']").text
        self.assertEqual("In 2019 With a score of 65",score)

    def testInter(self):
        time.sleep(3)
        intermediate = self.driver.find_element_by_xpath("(//p[@id='usereducationsection__text--schoolboard'])[2]").text
        self.assertEqual("Completed Intermediate",intermediate)
        stream = self.driver.find_element_by_xpath("(//p[@id='usereducationsection__text--department'])[2]").text
        self.assertEqual("In M.P.C",stream)
        college = self.driver.find_element_by_xpath("(//p[@id='usereducationsection__text--schoolname'])[2]").text
        self.assertEqual("At Sri Chaitanya Junior Kalasala",college)
        score = self.driver.find_element_by_xpath("(//p[@id='usereducationsection__text--duration'])[2]").text
        self.assertEqual("In 2015 With a score of 87",score)

    def testMatriculation(self):
        time.sleep(3)
        matric = self.driver.find_element_by_xpath("(//p[@id='usereducationsection__text--schoolboard'])[3]").text
        self.assertEqual("Completed High School",matric)
        stream = self.driver.find_element_by_xpath("(//p[@id='usereducationsection__text--department'])[3]").text
        self.assertEqual("In Central Board of Secondary Education",stream)
        school = self.driver.find_element_by_xpath("(//p[@id='usereducationsection__text--schoolname'])[3]").text
        self.assertEqual("At Ken-Crest International School",school)
        year = self.driver.find_element_by_xpath("(//p[@id='usereducationsection__text--duration'])[3]").text
        self.assertEqual("In 2013",year)

    def testLanguages(self):
        time.sleep(3)
        languages = self.driver.find_elements_by_xpath('(//ul[@class="ProfileSection__list"])[2]//child::li[@class="MyProfileSection__container--listitems"]')
        list1 = []
        for record in languages:
            list1.append(record.text)
        self.assertListEqual(['English', 'Telugu', 'Hindi'],list1)

    def testIntrests(self):
        time.sleep(3)
        intrests = self.driver.find_elements_by_xpath('(//ul[@class="ProfileSection__list"])[3]//child::li[@class="MyProfileSection__container--listitems"]')
        list1 = []
        for record in intrests:
            list1.append(record.text)  
        self.assertListEqual(['Football', 'Cricket', 'Books', 'Standupcomedy'],list1)
    
    def testSkills(self):
        time.sleep(3)
        skills = self.driver.find_elements_by_xpath('(//ul[@class="ProfileSection__list"])[1]//child::li[@class="MyProfileSection__container--listitems"]')
        list1 = []
        for record in skills:
            list1.append(record.text) 
        self.assertListEqual(['Python', 'Django', 'CoreJava'],list1)

    def testEmail(self):
        time.sleep(3)
        email = self.driver.find_element_by_xpath('//P[@id="UserEmail"]').text
        self.assertEqual("rmyadam@innominds.com",email)
        

if __name__=="__main__":
    unittest.main()
