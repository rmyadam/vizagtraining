from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time 
import unittest
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By 
from driver import driver 
from webelements import Home
global driver 



class HomePage(unittest.TestCase):
    
    @classmethod
    def setUpClass(cls):
        cls.driver = driver 
        cls.element = Home(cls.driver)
        cls.wait = WebDriverWait(cls.driver,10)
        cls.element.clickHome()


    def testwebinars(self):
        self.element.webinar1().click()
        college1 =  self.element.college1()       
        self.assertEqual(college1.text,self.element.homeData(4,2))
        self.element.clickHome()
        self.element.webinar2().click()
        college2 =  self.element.college2()       
        self.assertEqual(college2.text,self.element.homeData(5,2))
        
        


    def testRegisteredWebinars(self):

        
        # views all registered webinars 
        self.element.registeredWebinars()

        college = self.element.registeredCollege()
        self.assertEqual(college.text,self.element.homeData(9,2))
        year = self.element.registeredYear()
        self.assertEqual(year.text,self.element.homeData(10,2))
    
    def testActiveWebinars(self):
        self.element.activeWebinars()
        list1 = self.element.activeColleges()
        self.assertEqual(list1,self.element.homeData(14,2).split(','))
        
    def testNetwork(self):
        # requests
        self.element.viewNetwork()

        self.element.clickRequest()
        user = self.element.requestUser()
        self.assertEqual(user.text,self.element.homeData(17,3))
        
        self.driver.back()
        # connections 
        connectionList = self.element.getConnections()
        self.assertListEqual(connectionList,self.element.homeData(18,3).split(','))
        


        
    def testnavigation(self):
        
        self.element.clickWebinars()

        self.element.clickHome()
 
        self.element.clickMyNetwork()
       
        self.element.clickArticles()
       
        self.element.clickMessages()
        
        self.element.clickMyProfile()
        
        self.element.clickSignOut()
        
        self.element.closeSignOut()




    def testSearchBox(self):
        search  = self.element.searchBox()

        search.send_keys(self.element.homeData(22,2))


        self.element.clickPuranProfile()

        name1 = self.element.getPuranName()
        self.assertEqual(name1.text,self.element.homeData(23,2))
        
        bio = self.element.getPuranBio()
        self.assertEqual(bio.text,self.element.homeData(24,2))


        address = self.element.getPuranAddress()
        self.assertEqual(address.text,self.element.homeData(25,2))


        # subhasis
        search  = self.element.searchBox()
        search.send_keys(self.element.homeData(27,2))

        self.element.clickSubhasisProfile()

        name2 = self.element.getSubhasisName()
        self.assertEqual(name2.text,self.element.homeData(28,2))




    def testProfile(self):
        

        self.element.getProfile()

        userName = self.element.getProfileName()
        self.assertEqual(userName.text,self.element.homeData(31,2))

        bio = self.element.getProfileBio()
        self.assertEqual(bio.text,self.element.homeData(32,2))

        location = self.element.getProfileLocation()      
        self.assertEqual(location.text,self.element.homeData(33,2))

     
        
    def tearDown(self):
       self.element.clickHome()



if __name__=="__main__":
    unittest.main()