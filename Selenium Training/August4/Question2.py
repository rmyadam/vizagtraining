'''
This program contains all the browser commands of a website 
@Rithvik 
'''

# importing all the modules
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

# creation of driver object 
driver = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\Selenium Training\August4\chromedriver_win32\chromedriver.exe")
# get the response from the site 
driver.get("https://www.redbus.in/")

# window sets the full screen 
driver.fullscreen_window()
time.sleep(3)

# maximises the window 
driver.maximize_window()
time.sleep(3)

# minimises the screen 
driver.minimize_window()
time.sleep(3)

# set the time out of the screen 
driver.set_script_timeout(100)
time.sleep(3)

# setting the position of the window 
driver.set_window_position(4,8)
time.sleep(3)

# setting the window position and size 
driver.set_window_rect(5,10,1000,1000)
time.sleep(3)

# going back in the site 
driver.back()
time.sleep(3)

# going forward in site 
driver.forward()
time.sleep(3)

# refreshing the page 
driver.refresh()

# getting the size of window and printing it 
size = driver.get_window_size()
print("The size of window is :",size)

# getting position of window and printing it 
position = driver.get_window_position()
print("The position of window is :",position)

# getting the title of the website and printing it
title = driver.title
print("The title of page is :",title)

# getting the current url and printing it 
currentUrl = driver.current_url
print("The current Url of page is :",currentUrl)
time.sleep(3)

# closing the window 
driver.close()