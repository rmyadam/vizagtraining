'''
This program executes the test suites of different browsers simultaneously 
@Rithvik  
'''

# importing the test suites from the appropriate files 
from Question1 import stack
from Question1Func2 import red
import threading

def main():
    '''
    This function executes the threads simultaneously to run the sripts at
    a time 
    '''

    # creation of the two  threads 
    thread1 = threading.Thread(target=stack)
    thread2 = threading.Thread(target=red)

    # execution of two threads
    thread1.start()
    thread2.start()
    thread1.join()
    thread2.join()
main()
