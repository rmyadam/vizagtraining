"""
Program contains all the locators of the web elements
@Rithvik 
"""

# importing the modules 
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By 
import time

# creation of browser driver 
driver = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\August4\chromedriver_win32\chromedriver.exe")
# maximise the window and getting response from site 
driver.maximize_window()
driver.get("https://stackoverflow.com/")

# locating the login button object by link text and send keys 
login=driver.find_element_by_link_text('Log in')
login.click()
time.sleep(3)

# locating the search field object by class name and send keys 
driver.find_element_by_class_name('s-input').send_keys("data flow")
time.sleep(3)

# locating the email field object by id and send keys 
driver.find_element_by_id("email").send_keys("rithvik@gmail.com")
time.sleep(3)

# locating the password field object by name and send keys 
driver.find_element_by_name("password").send_keys("rithvik123")
time.sleep(3)

# locating the Login field object by partial link text and click 
driver.find_element_by_partial_link_text("Log").click()
time.sleep(3)

# going back in site 
driver.back()
driver.back()
time.sleep(3)

# locating the For developers field object by xpath and click 
driver.find_element_by_xpath("//a[contains(text(),'For developers')]").click()
time.sleep(3)

# locating the sign in button object by css selector and click 
signBtn = driver.find_element_by_css_selector("body > header > div > ol.overflow-x-auto.ml-auto.-secondary.grid.ai-center.list-reset.h100.user-logged-out > li.-ctas > a.login-link.s-btn.s-btn__primary.py8")
signBtn.click()
time.sleep(3)

# locating the sign in button object by find element and click  
driver.find_element(By.ID,"search").click()
time.sleep(3)

# closing the browser
driver.close()
