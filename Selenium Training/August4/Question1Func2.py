"""
This program contains the fields for testing in a chrome browser 
@Rithvik 
"""

# importing required modules 
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time


def red():
    '''
    This function contains the methods by which few fields are tested  
    '''

    # creation of driver object by name (driver)
    driver = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\August4\chromedriver_win32\chromedriver.exe")
    # maximising the window 
    driver.maximize_window()
    # getting the response from required website 
    driver.get("https://www.redbus.in/")

    # creation of object of From field and sending keys 
    fromLabel = driver.find_element_by_xpath("//input[@id='src']")
    fromLabel.send_keys("vishakapatnam")
    time.sleep(3)

    # creation of object of the Destination field and sending keys
    destLabel = driver.find_element_by_xpath("//input[@class='db'][@id='dest']")
    destLabel.send_keys("hyderabad")
    time.sleep(3)

    # creation of object of date field and clicking the input 
    date = driver.find_element_by_xpath("//input[@id='onward_cal']")
    date.click()

    # clicking the required date in the date field 
    currentDate = driver.find_element_by_xpath("//td[contains(text(),'25')]")
    currentDate.click()
    time.sleep(3)
    
    # creation of object of search button and clicking the button 
    searchBtn = driver.find_element_by_xpath("//section[@id='search']/div/button")
    searchBtn.click()
    time.sleep(3)
    # closing the browser 
    driver.close()