"""
This program contains all the methods of x-path 
@Rithvik 
"""

# importing the modules 
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

# creation of driver object 
driver = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\Selenium Training\August4\chromedriver_win32\chromedriver.exe")
# get the response of the website and maximise the window
driver.get("https://www.redbus.in/")
driver.maximize_window()

# creating the object of Feom field and sending keys 
fromLabel = driver.find_element_by_xpath("//input[@id='src']")
fromLabel.send_keys("vishakapatnam")
time.sleep(3)

# creating the object of the destination field and send keys 
destLabel = driver.find_element_by_xpath("//input[@class='db'][@id='dest']")
destLabel.send_keys("hyderabad")
time.sleep(3)

# creating object of date and click the date 
date = driver.find_element_by_xpath("//input[@id='onward_cal']")
date.click()

# creating the object of the current date and click 
currentDate = driver.find_element_by_xpath("//td[contains(text(),'25')]")
currentDate.click()
time.sleep(3)

# locating object of search button by xpath id 
searchBtn = driver.find_element_by_xpath("//section[@id='search']/div/button")
searchBtn.click()
time.sleep(3)

# locating object of Hire button by xpath absolute path  
hireBtn = driver.find_element_by_xpath("//div[@id='page_main_header']/nav/ul/li[3]/a")
hireBtn.click()
time.sleep(3)

# locating object of  city Hire field  by xpath starts with and send keys   
cityHire = driver.find_element_by_xpath("//input[starts-with(@class,'Source')]")
cityHire.send_keys("Hyderabad")
time.sleep(3)

# locating object of  destination by xpath with following  and send keys   
start = driver.find_element_by_xpath('//*[@class="StartingPoint autocomplete_input"]//following::input[1]')
start.send_keys("Vishakapatnam")
time.sleep(3)

# going back in website 
driver.back()

# locating object of  rpool by xpath with parent  and click   
newCarsBtn = driver.find_element_by_xpath("//*[@id='cars']//parent::a")
newCarsBtn.click()

time.sleep(3)

# going back in site 
driver.back()
time.sleep(3)

# locating object of  help button by xpath with AND operation and click   
helpBtn = driver.find_element_by_xpath("//a[@href='/info/redcare' and @target='_blank']")
helpBtn.click()

# closing the browser
driver.close()