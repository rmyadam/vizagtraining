"""
This program depicts the testing of few fields using the 
chrome browser
@Rithvik 
"""

# importing the webdriver from selenium 
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time


def stack():
    '''
    This method consists the required actions on the few fields for 
    testing 
    '''

    # creation of the browser driver with the name (driver1)
    driver1 = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\August4\chromedriver_win32\chromedriver.exe")
    # getting the response of required website 
    driver1.get("https://stackoverflow.com/")
    # maximising the window 
    driver1.maximize_window()

    # creating the login button object and performing click operation on it 
    login=driver1.find_element_by_link_text('Log in')
    login.click()
    time.sleep(3)

    # creation of search field object and sending keys 
    driver1.find_element_by_class_name('s-input').send_keys("data flow")
    time.sleep(3)

    # creation of object of email field and sending keys 
    driver1.find_element_by_id("email").send_keys("rithvik@gmail.com")
    time.sleep(3)

    # creation of object of password field and sending keys 
    driver1.find_element_by_name("password").send_keys("rithvik123")
    time.sleep(3)

    # closing the browser 
    driver1.close()