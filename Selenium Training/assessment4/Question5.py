"""
This program contains the different waits in selenium 
"""


from selenium import webdriver
from selenium.webdriver.common.keys import Keys 
import time
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By 
from selenium.webdriver.support import expected_conditions as EC 


# creation of driver object 
driver = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\assessment4\chromedriver_win32\chromedriver.exe")
driver.maximize_window()
driver.get("https://www.geeksforgeeks.org/")
time.sleep(3)

# implicit wait 
driver.implicitly_wait(5)

driver.find_element_by_id("gsc-i-id2").send_keys("data structures")

time.sleep(3)

driver.close()

# creation of driver object 
driver2 = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\assessment4\chromedriver_win32\chromedriver.exe")
driver2.maximize_window()
driver2.get("https://www.carwale.com/")

# declaring explicit wait 
wt = WebDriverWait(driver2,3)
search = wt.until(EC.presence_of_element_located((By.ID,"newCarsList")))
search.send_keys("mahindra")


pt = WebDriverWait(driver2,3)
pt.until(EC.presence_of_element_located((By.XPATH,"//*[@id='google_ads_iframe_/1017752/Homepage_FirstSlot_976x400_0__container__']")))

xt = WebDriverWait(driver2,3)
xt.until(EC.presence_of_element_located((By.XPATH,'//*[@id="topSelling"]/div/div/ul/li[2]/div/div/div[1]/a/img')))


time.sleep(3)
 

driver2.close()