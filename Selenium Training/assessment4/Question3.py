"""
This program contains the window handling and exception handling 
@author Rithvik 
"""

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys 
import time
from selenium.common.exceptions import *


# creation of driver object 
driver = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\assessment4\chromedriver_win32\chromedriver.exe")
driver.maximize_window()
driver.get("file:///C:/Users/rmyadam/Desktop/vizagtraining/Selenium%20Training/assessment4/Question3.html")

time.sleep(3)

driver.find_element_by_xpath("/html/body/a").click()

# switching the windows 
pw = driver.current_window_handle
allwindows = driver.window_handles

time.sleep(3)
for window in allwindows:
    if window != pw:
        driver.switch_to_window(window)

# entering the keys into the required field 
driver.find_element_by_id("newCarsList").send_keys("mahindra")
time.sleep(3)
count = 1
# exception handling with while loop 
while(True):
    try:
        if count == 1:
            driver.find_element_by_id("rithvik")
        elif count == 2:
            driver.switch_to.frame(0)
        elif count ==3:
            driver.switch_to.frame(0)

        elif count == 4:
            driver.switch_to_alert()

            driver.set_page_load_timeout(0)
            driver.get("https://www.redbus.in/")

        elif count == 5:
            driver.set_page_load_timeout(0)
            driver.get("https://www.redbus.in/")
            
        elif count == 6:
            break 

    except NoSuchElementException as ex:
        print(ex.msg)
        count = count + 1
    except NoSuchFrameException as ex:
        print(ex.msg)
        count = count + 1
    except InvalidSwitchToTargetException as ex:
        print(ex.msg)
        count= count+1
    except NoAlertPresentException as ex:
        print(ex.msg)
        count = count + 1
    except TimeoutException as ex:
        print(ex.msg)
        count = count+1
    

driver.close()