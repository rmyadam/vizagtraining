"""
This propgram contains the different methods of xpath 
@author Rithvik 
"""

from selenium import webdriver
from selenium.webdriver.common.keys import Keys 
import time 


# creation of driver object and maximising th screen 
driver = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\assessment4\chromedriver_win32\chromedriver.exe")
driver.maximize_window()
driver.get("https://www.redbus.in/")  

time.sleep(3)

# locating the element by using the parent and child xpath 
driver.find_element_by_xpath("//input[@class='db'][@id='src']").send_keys("Hyderabad")
time.sleep(3)

# locating the element by using the parent and child xpath 
driver.find_element_by_xpath("//input[@class='db'][@id='dest']").send_keys("banglore")

time.sleep(3)

# locating the element using the single attribute 
date = driver.find_element_by_xpath("//input[@id='onward_cal']")
date.click()

# locating the element using the contains method 
daterequired = driver.find_element_by_xpath("//td[contains(text(),'21')]")
daterequired.click()
time.sleep(3)

# locating the element using id 
driver.find_element_by_id('search_btn').click()

# going back in browser 
driver.back()

time.sleep(3)

# locating the element using the starts-with method 
driver.find_element_by_xpath("//input[starts-with(@id,'de')]").clear()
driver.find_element_by_xpath("//input[starts-with(@id,'de')]").send_keys("vizag")
time.sleep(3)

# locating the element using the text() method 
driver.find_element_by_xpath("//a[text()='Help']").click()
time.sleep(3)

driver.close()