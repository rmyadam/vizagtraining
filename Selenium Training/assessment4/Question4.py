"""
This program depicts few test cases of unittest 
@author Rithvik 
"""


from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys 
import time
import unittest


class TestAutomation(unittest.TestCase):
    """
    This class contains few unit test suites 
    """
    global driver
    driver = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\assessment4\chromedriver_win32\chromedriver.exe")
    driver.maximize_window()
    driver.get("https://www.carwale.com/")

    def testTitle(self):
        title = driver.title
        self.assertEqual(title,"New Cars, Used Cars, Buy a Car, Sell Your Car - CarWale")

    def testFalse(self):
        heading = driver.find_element_by_xpath("//h1[@class='welcome-box__title']")
        self.assertFalse(heading.is_selected())

    def testIn(self):
        title = driver.title
        self.assertIn("Cars",title)


if __name__=="__main__":   
    unittest.main()     
