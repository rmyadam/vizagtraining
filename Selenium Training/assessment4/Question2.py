"""
This program containsthe methods for auto suggestions 
@ author Rithvik 
"""

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys 
import time


# creation of driver object and  maximising the screen 
driver = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\assessment4\chromedriver_win32\chromedriver.exe")
driver.maximize_window()
driver.get("https://www.google.com/")  

time.sleep(3)

# sending required keys into the text box 
driver.find_element_by_name("q").send_keys("car")

time.sleep(3)

# saving the screen shot 
driver.save_screenshot(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\assessment4\screenshot.png")

# clicking two times arrow down and selecting the option 
action = ActionChains(driver)
action.key_down(Keys.DOWN)
action.key_up(Keys.DOWN)
action.key_down(Keys.DOWN)
action.key_up(Keys.DOWN)
action.key_down(Keys.ENTER)
action.perform()


driver.close()

# creating another driver obejct 
driver2 = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\assessment4\chromedriver_win32\chromedriver.exe")
driver2.maximize_window()
driver2.get("https://www.google.com/")  

time.sleep(3)

# entering the required keys 
driver2.find_element_by_name("q").send_keys("car")
time.sleep(3)

# collecting the required objects 
list1 = driver2.find_elements_by_xpath("//li[@class='sbct']")

# iterating through the list and clicking the required option 
for i in list1:
    if i.text == "car games":
        i.click()


time.sleep(3)

# close the browser 
driver2.close()