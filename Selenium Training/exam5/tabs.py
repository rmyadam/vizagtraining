"""
This program exhibits the window switching between different tabs 
@author Rithvik
"""


from selenium import webdriver 
import time 
from selenium.webdriver.common.action_chains import ActionChains


# creation of driver object 
driver = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\exam6\chromedriver_win32\chromedriver.exe")
driver.maximize_window()
driver.get("file:///C:/Users/rmyadam/Desktop/vizagtraining/Selenium%20Training/exam6/webpage1.html")


time.sleep(3)

# locating the link and clicking it 
driver.find_element_by_tag_name("a").click()

time.sleep(3)

# getting all the current windows into a list (windows)
windows= driver.window_handles

# taking the second value from the list to switch to the new tab 
tab1 = driver.window_handles[1]
driver.switch_to_window(tab1)

# after switching to the new tab locating the input field and entering keys 
driver.find_element_by_xpath('//input[@id="src"]').send_keys("hyderabad")

time.sleep(3)

# clicking the help button to get another new tab 
driver.find_element_by_xpath('//a[text()="Help"]').click()

time.sleep(3)

# getting all the current tabs into a list 
tab2 = driver.window_handles

# switching to the latest tab 
driver.switch_to_window(tab2[2])

time.sleep(3)

# switching to frame in latest tab and entering the keys in a input field 
driver.switch_to_frame(1)
driver.find_element_by_xpath('//input[@id="mobileNoInp"]').send_keys("7243432")

time.sleep(3)

# closing the latest tab  
driver.close()

time.sleep(3)

# switching to the next latest tab and closing it 
driver.switch_to_window(tab2[1])
driver.close()

time.sleep(3)

# swithcing to the main window and clicking the link in the tab 
driver.switch_to_window(tab2[0])
driver.find_element_by_tag_name("a").click()


time.sleep(3)

# closing the driver 
driver.close()