"""
This program shows all the options in a multiselect box and checks whether
options are in alphabetical order 
"""

from selenium import webdriver
import time 
from selenium.webdriver.support.ui import Select


# creation of driver object 
driver = webdriver.Chrome(r"Selenium Training\exam6\chromedriver_win32\chromedriver.exe")
driver.get(r"file:///C:/Users/rmyadam/Desktop/vizagtraining/Selenium%20Training/exam6/multi.html")
driver.maximize_window()

# locating the multiselect drop down element 
webelement = driver.find_element_by_xpath('//select[@name="cars"]')
mutliDrop = Select(webelement)

# initialising empty list 
optionList = []  
# printing and appending all the options into the list 
for i in mutliDrop.options:
    optionList.append(i.text)
    print(i.text)

# checking for the alphabetical order of  options in mutliselect box 
if optionList== sorted(optionList):
    print("The options in multiselect drop down box are alphabetical order ")

else:
    print("The options in multiselect drop down box are not in alphabetical order")

driver.close()