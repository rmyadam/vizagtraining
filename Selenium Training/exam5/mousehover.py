
"""
This program exhibits the mouse hovering of a submenu in webpage
@author Rithvik 
"""


from selenium import webdriver 
import time 
from selenium.webdriver.common.action_chains import ActionChains

# creation of driver object 
driver = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\assessment3\chromedriver_win32\chromedriver.exe")
driver.get("https://www.sharda.ac.in/")  
time.sleep(3)

# locating the academic field
academic = driver.find_element_by_xpath("//a[text()='Academic']")

# locating the dental study field 
dental = driver.find_element_by_xpath("//a[text()='Dental Sciences']")

# hovering over academic and dental study fields and 
# clicking the dental study  field
action = ActionChains(driver)
action.move_to_element(academic)
action.move_to_element(dental)
action.click()
action.perform()

time.sleep(3)

# closing the browser 
driver.close()