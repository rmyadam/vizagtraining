"""
This program exhibits the unittest cases and batch wise execution
@author Rithvik 
"""


from selenium import webdriver
import time 
from selenium.webdriver.support.ui import Select
import unittest


class TestStackOverflow(unittest.TestCase):
    """
    This class contains the test cases for batch execution  
    """
    
    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome(r"Selenium Training\exam6\chromedriver_win32\chromedriver.exe")
        cls.driver.get("https://stackoverflow.com/")
        cls.driver.maximize_window()

    def testEqual(self):
        title = self.driver.title
        self.assertEqual("Stack Overflow - Where Developers Learn, Share, & Build Careers",title)
    
    
    def testIn(self):
        title = self.driver.title
        self.assertIn("Stack",title)

    def tearDown(self):
        time.sleep(3)

    
class TestStackOverflow2(unittest.TestCase):
    """
    This class contains the test cases for batch execution  
    """
    

    @classmethod
    def setUpClass(cls):
        cls.driver = webdriver.Chrome(r"Selenium Training\exam6\chromedriver_win32\chromedriver.exe")
        cls.driver.get("https://stackoverflow.com/")
        cls.driver.maximize_window()

    def testTrue(self):
        self.assertTrue(self.driver.find_element_by_xpath('//a[text()="Log in"]').is_displayed())

    def testFalse(self):
        self.assertFalse(self.driver.find_element_by_xpath('//a[text()="Log in"]').is_selected())
    
    def tearDown(self):
        time.sleep(3)


def main():
    """
    """
    # creating the test loader 
    test_load = unittest.TestLoader()
    # loading the tests into objects
    t1 = test_load.loadTestsFromTestCase(TestStackOverflow) 
    t2 = test_load.loadTestsFromTestCase(TestStackOverflow2)
    # creating the test suite
    suite = unittest.TestSuite()
    # adding the loaded test objects into suite 
    suite.addTests(t1)
    suite.addTests(t2)
    # running the tests 
    runner = unittest.TextTestRunner()
    runner.run(suite)

main()