

from selenium import webdriver
import time
from selenium.common.exceptions import *
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By 
from selenium.webdriver.support import expected_conditions as EC 






# creation of driver object 
driver = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\assessment4\chromedriver_win32\chromedriver.exe")
driver.maximize_window()
driver.get("https://stackoverflow.com/")
driver.implicitly_wait(3)

count = 1
while(True):
    try:
        if count == 1:
            driver.find_element_by_id("ravi")
            time.sleep(3)
        elif count == 2:
            driver.switch_to.frame(0)
        elif count ==3:
            driver.switch_to.frame(0)
            wt = WebDriverWait(driver,3)
            wt.until(EC.presence_of_element_located((By.NAME,"q"))) 
        elif count == 4:
            driver.switch_to_alert()
        elif count == 5:
            driver.set_page_load_timeout(0)
            driver.get("https://www.redbus.in/")
            
        elif count == 6:
            break 

    except NoSuchElementException as ex:
        print(ex.msg)
        count = count + 1
    except NoSuchFrameException as ex:
        print(ex.msg)
        count = count + 1
    except InvalidSwitchToTargetException as ex:
        print(ex.msg)
        count= count+1
    except NoAlertPresentException as ex:
        print(ex.msg)
        count = count + 1
    except TimeoutException as ex:
        print(ex.msg)
        count = count+1
    

driver.close()