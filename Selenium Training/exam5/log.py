import logging
import unittest


class Testlogging(unittest.TestCase):
    # initializing the level of logging 

    logging.basicConfig(filename="Selenium Training\exam6\logfile",filemode='w')


    logger=logging.getLogger()


    logger.setLevel(logging.DEBUG)



    def test(self):
        """
        This method will display the logging messages based on the conditions
        """
        logging.debug('debug')
        logging.info('info ')
        logging.warning('warning ')
        logging.error(' error ')
        logging.critical('critical')

if __name__ == "__main__":
    unittest.main()
