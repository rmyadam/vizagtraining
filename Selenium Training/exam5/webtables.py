"""
This program exhibits the methods that can be performed on 
webtables 
@author Rithvik 
"""

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest
import time
import logging


class Webtable():
    """
    This class contains all the methods which can be performed on webtables 
    """
    # initializing the level of logging
    logging.basicConfig(filename="Selenium Training\exam6\logfile",filemode='w')

    logger=logging.getLogger()
    logger.setLevel(logging.DEBUG)

    # making driver object global 
    global driver
    driver = webdriver.Chrome(r"Selenium Training\exam6\chromedriver_win32\chromedriver.exe")
    driver.get("file:///C:/Users/rmyadam/Desktop/vizagtraining/Selenium%20Training/exam6/webtable.html")
    driver.maximize_window()
    time.sleep(3)


    def numberOfRows(self):
        """
        This method returns the number of rows of a webtable 
        """
        self.rowno = len(driver.find_elements_by_tag_name("tr"))-1
        logging.info("the rows are found")
        return self.rowno


    def numberOfCols(self):
        """
        This method returns the number of columns of a webtable 
        """
        self.colno = len(driver.find_elements_by_xpath("//tr[2]/td"))
        return self.colno


    def size(self):
        """
        This method returns the size of the webtable 
        """
        return (self.rowno,self.colno)
    


    def readRow(self,param):
        """
        This method returns the record of a particular row parsed 
        input: param(Int)
        """
        # ierating through range of column number displaying that record 
        print("The data in row no "+str(param)+" is :")
        for i in range(1,self.colno+1):
            value = driver.find_element_by_xpath("/html/body/table/tbody/tr["+str(param)+"]/td["+str(i)+"]").text
            print(value,end ="      ")
        print("\n")



    def readCol(self,param):
        """
        This method returns the record of a particular row parsed 
        input: param(Int)
        """
        # ierating through range of column number displaying that record 
        print("The data in column no "+str(param)+" is :")
        for i in range(2,self.rowno+2):
            value = driver.find_element_by_xpath("/html/body/table/tbody/tr["+str(i)+"]/td["+str(param)+"]").text
            print(value,end ="      ")
        print("\n")



    def readAllData(self):
        """
        This method reads all the data from webtable 
        """
    
        print("The all data present in webtable is :")
        # iterating through range of number of rows 
        for i in range(2,self.rowno+2):
            # iterating through range of number of rows 
            for j in range(1,self.colno+1):
                value = driver.find_element_by_xpath("/html/body/table/tbody/tr["+str(i)+"]/td["+str(j)+"]").text
                print(value,end="    ")
            print("\n")



    def readSpecificCell(self,row,column):
        """
        This method retreives the data from a specific cell
        input:row(Int),column(Int)
        """
        # retrieving data from the specific cell by xpath and printing it 
        print("The data in row "+str(row)+" and column "+str(column)+"is :")
        value = driver.find_element_by_xpath("/html/body/table/tbody/tr["+str(row)+"]/td["+str(column)+"]").text
        print(value,end="    ")



def main():
    """
    This function calls all the methods defined in class 
    """
    # creation of object of Webtable 
    webObj= Webtable()
    print("The number of rows in webtable are : ",webObj.numberOfRows())
    print("The number of columns in webtable are : ",webObj.numberOfCols())
    print("The size of webtable is : ",webObj.size())
    webObj.readRow(2)
    webObj.readCol(2)
    webObj.readAllData()
    webObj.readSpecificCell(3,2)
    driver.quit()

main()