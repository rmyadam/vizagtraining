"""
This program displays all the links in a webpage 
@author Rithvik
"""

from selenium import webdriver
import time 


# creation of the driver object 
driver = webdriver.Chrome(r"Selenium Training\exam6\chromedriver_win32\chromedriver.exe")
driver.get("https://www.redbus.in/")
driver.maximize_window()
time.sleep(4)

# appending all the web elements which contains links into a list 
listLink = driver.find_elements_by_tag_name("a")
for link in listLink:
    print(link.get_attribute("href"))

time.sleep(4)

driver.close()