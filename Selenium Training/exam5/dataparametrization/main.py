"""
This program exhibits the retrieval of elements and data from other 
files and testing the webpages 
@author Rithvik 
"""


from openpyxl import *
from webelements import Signup
import unittest
import time 

class TestFreckle(unittest.TestCase):
    """
    This class contains the test cases for webpage 
    """

    @classmethod
    def setUpClass(cls):
        cls.element = Signup()
        loadobj = load_workbook(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\exam6\dataparametrization\testdataexam.xlsx")
        cls.workbook = loadobj.active


    def testFirstName(self):
        """
        This method tests the first name field in the webpage by getting element from 
        webelements file and data from testdataexam excel file
        """
        # getting the web element 
        self.firstname = self.element.getFirstName()
        # getting the data from excel sheet by iterating through loop 
        for i in range(2,6):
            # clearing the field 
            self.firstname.clear()
            # sending the keys to the field 
            self.firstname.send_keys(self.workbook.cell(row=i,column=2).value)
            time.sleep(3)
            # asserting the fields 
            self.assertEqual(self.firstname.get_attribute("value"),self.workbook.cell(row=i,column=2).value)
            self.assertTrue(self.firstname.is_displayed())


    
    def testLastName(self):
        """
        This method tests the last name field in the webpage by getting element from 
        webelements file and data from testdataexam excel file
        """
        # getting the web element 
        self.lastname = self.element.getLastName()
        # getting the data from excel sheet by iterating through loop 
        for i in range(2,6):
            # clearing the field 
            self.lastname.clear()
            # sending the keys to the field 
            self.lastname.send_keys(self.workbook.cell(row=i,column=3).value)
            time.sleep(3)
            # asserting the fields 
            self.assertEqual(self.lastname.get_attribute("value"),self.workbook.cell(row=i,column=3).value)
            self.assertTrue(self.lastname.is_displayed())



if __name__=="__main__":
    unittest.main()