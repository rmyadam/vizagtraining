from selenium import webdriver 
from selenium.webdriver.common.keys import Keys
import time 


class Signup:
    """
    This class contains methods whhich returns webelements of signup 
    page 
    """

    def __init__(self):
        """
        This method creates instance variables of driver object 
        """
        self.driver = webdriver.Chrome(r"Selenium Training\exam6\chromedriver_win32\chromedriver.exe")
        self.driver.get("https://classroom.freckle.com/#/signup")
        self.driver.maximize_window()

    def getFirstName(self):
        """
        This method will return the web element of firstname field 
        """
        displayname  = self.driver.find_element_by_id("first-name")
        return displayname 

    def getLastName(self):
        """
        This method will return the web element of firstname field 
        """
        lastname = self.driver.find_element_by_id("last-name")
        return lastname 