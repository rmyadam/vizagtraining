from selenium import webdriver 
import time 
from selenium.webdriver.support.ui import Select

# creation of the driver object 
driver = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\assessment3\chromedriver_win32\chromedriver.exe")
driver.get("https://form.jotform.com/202222340850441")  


# locating the drop drown web element 
dropDownLocation = driver.find_element_by_xpath("//select[@id='hour_15']")

# creation of dropdown object 
dropDown = Select(dropDownLocation)

# selecting the option by value 
dropDown.select_by_value("1")

time.sleep(3)

# selecting the option by index 
dropDown.select_by_index(2)
time.sleep(3)

# selecting the option by visible text
dropDown.select_by_visible_text("6")
time.sleep(3)



# printing all the options in drop down box 
for x in dropDown.options:
    print(x.text)


# printing the visibility of the drop down box 
print(driver.find_element_by_xpath("//select[@id='hour_15']").is_displayed())

# printing whether drop down box is enabled 
print(driver.find_element_by_xpath("//select[@id='hour_15']").is_enabled())

# printing the name of the drop down box  
print(driver.find_element_by_xpath("//select[@id='hour_15']").get_attribute("name"))

# printing the type of the drop down box  
print(driver.find_element_by_xpath("//select[@id='hour_15']").get_property("type"))

# printing whether drop down box is selected   
print(driver.find_element_by_xpath("//select[@id='hour_15']").is_selected())

# printing the tag name of the drop down box  
print(driver.find_element_by_xpath("//select[@id='hour_15']").tag_name)

# printing the location of the drop down box  
print(driver.find_element_by_xpath("//select[@id='hour_15']").location)

# printing the position of the drop down box  
print(driver.find_element_by_xpath("//select[@id='hour_15']").rect)

# printing the size of the drop down box  
print(driver.find_element_by_xpath("//select[@id='hour_15']").size)

# printing the font-size of the drop down box  
print(driver.find_element_by_xpath("//select[@id='hour_15']").value_of_css_property("font-size"))

# printing the parent object of the drop down box  
print(driver.find_element_by_xpath("//select[@id='hour_15']").parent)

#  closing the browser 
time.sleep(3)
driver.close()