from selenium import webdriver 
import time 
from selenium.webdriver.common import keys

# creation of driver object
driver = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\assessment3\chromedriver_win32\chromedriver.exe")
driver.get("http://demo.automationtesting.in/Frames.html") 

time.sleep(3)

# switching to the frame 
driver.switch_to_frame("singleframe")

# inserting keys in the input field 
driver.find_element_by_xpath("/html/body/section/div/div/div/input").send_keys("Rithvik")
time.sleep(3)

# closing the browser 
driver.close()