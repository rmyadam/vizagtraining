from selenium import webdriver 
import time 
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common import keys

# creation of browser object 
driver = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\assessment3\chromedriver_win32\chromedriver.exe")
driver.get("https://form.jotform.com/202222340850441")  
time.sleep(3)

# locating the input field 
name = driver.find_element_by_xpath("//input[@id='first_7']")

time.sleep(3)

# creation of action object 
action = ActionChains(driver)
action.click(name)
# entering keys in the input field by keyboard actions 
action.key_down(keys.Keys.SHIFT)
action.send_keys("rithvik")
action.key_down(keys.Keys.SHIFT)
# performing action 
action.perform()

time.sleep(3)

# closing window 
driver.close()