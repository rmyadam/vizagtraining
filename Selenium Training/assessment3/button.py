from selenium import webdriver 
import time 
from selenium.webdriver.common.keys import Keys

# creation of driver object 
driver = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\assessment3\chromedriver_win32\chromedriver.exe")
driver.get("file:///C:/Users/rmyadam/Desktop/vizagtraining/Selenium%20Training/assessment3/button.html")  


time.sleep(3)

# print the visibilty of the field  
print(driver.find_element_by_xpath("//button[@class='button button1']").is_displayed())

# print whether the field is enable 
print(driver.find_element_by_xpath("//button[@class='button button1']").is_enabled())
 
# printing the id of the button field  
print(driver.find_element_by_xpath("//button[@class='button button1']").get_attribute("class"))

# printing the type of the button field  
print(driver.find_element_by_xpath("//button[@class='button button1']").get_property("type"))

# printing the tagname of the button field  
print(driver.find_element_by_xpath("//button[@class='button button1']").tag_name)

# printing the location of the button field  
print(driver.find_element_by_xpath("//button[@class='button button1']").location)

# printing the position of the button field  
print(driver.find_element_by_xpath("//button[@class='button button1']").rect)

# printing the size of the button field  
print(driver.find_element_by_xpath("//button[@class='button button1']").size)

# printing the text of the button field  
print(driver.find_element_by_xpath("//button[@class='button button1']").text)

# printing the font-size of the button field  
print(driver.find_element_by_xpath("//button[@class='button button1']").value_of_css_property("font-size"))

# clicking the search button 
driver.find_element_by_xpath("//button[@class='button button1']").click()

time.sleep(3)

driver.close()

