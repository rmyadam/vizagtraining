from selenium import webdriver 
import time 


# creation of driver object 
driver = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\assessment3\chromedriver_win32\chromedriver.exe")
driver.get("https://stackoverflow.com/")  


# window sets the full screen 
driver.fullscreen_window()
time.sleep(3)

# maximises the window 
driver.maximize_window()
time.sleep(3)

# minimises the screen 
driver.minimize_window()
time.sleep(3)

# set the time out of the screen 
driver.set_script_timeout(100)
time.sleep(3)

# setting the position of the window 
driver.set_window_position(4,8)
time.sleep(3)

# setting the window position and size 
driver.set_window_rect(5,10,1000,1000)
time.sleep(3)

# going back in the site 
driver.back()
time.sleep(3)

# going forward in site 
driver.forward()
time.sleep(3)

# refreshing the page 
driver.refresh()

# getting the size of window and printing it 
size = driver.get_window_size()
print("The size of window is :",size)

# getting position of window and printing it 
position = driver.get_window_position()
print("The position of window is :",position)

# getting the title of the website and printing it
title = driver.title
print("The title of page is :",title)

# getting the current url and printing it 
currentUrl = driver.current_url
print("The current Url of page is :",currentUrl)
time.sleep(3)

# closing the window 
driver.close()

# creation of another driver object 
driver2 = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\assessment3\chromedriver_win32\chromedriver.exe")

# getting the reponse from website 
driver2.get("https://www.redbus.in/")
driver2.maximize_window()
time.sleep(3)
# quitting the window 
driver2.quit()