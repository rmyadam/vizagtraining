from selenium import webdriver 
import time 
from selenium.webdriver.common.action_chains import ActionChains

# creation of driver object 
driver = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\assessment3\chromedriver_win32\chromedriver.exe")
driver.get("https://www.sharda.ac.in/")  
time.sleep(3)

# locating careers field 
careers = driver.find_element_by_xpath("//a[text()='Academic']")

# locating the business study field 
business = driver.find_element_by_xpath("//a[text()='Business Studies']")

# hovering over careers and business study fields and 
# clicking the business study fields
action = ActionChains(driver)
action.move_to_element(careers)
action.move_to_element(business)
action.click()
action.perform()

time.sleep(3)

# closing the browser 
driver.close()