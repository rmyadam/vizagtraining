from selenium import webdriver 
import time 
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys

# creation of driver object 
driver = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\assessment3\chromedriver_win32\chromedriver.exe")
driver.get("https://form.jotform.com/202222340850441")  

time.sleep(3)

# checking the visbility of the checkbox 
print(driver.find_element_by_xpath("//input[@id='input_13_1']").is_displayed())

# print whether check box is enable  
print(driver.find_element_by_xpath("//input[@id='input_13_1']").is_enabled())

# print the name of the check box 
print(driver.find_element_by_xpath("//input[@id='input_13_1']").get_attribute("name"))

# print the type of the check box 
print(driver.find_element_by_xpath("//input[@id='input_13_1']").get_property("type"))

# print whether the check box is selected  
print(driver.find_element_by_xpath("//input[@id='input_13_1']").is_selected())


# print the tag name of the check box 
print(driver.find_element_by_xpath("//input[@id='input_13_1']").tag_name)

# print the location of the check box 
print(driver.find_element_by_xpath("//input[@id='input_13_1']").location)

# print the position of the check box 
print(driver.find_element_by_xpath("//input[@id='input_13_1']").rect)

# print the size of the check box 
print(driver.find_element_by_xpath("//input[@id='input_13_1']").size)

# print the font-size of the check box 
print(driver.find_element_by_xpath("//input[@id='input_13_1']").value_of_css_property("font-size"))

# print the parent object of the check box 
print(driver.find_element_by_xpath("//input[@id='input_13_1']").parent)


# clicking the check box 
driver.find_element_by_xpath("//input[@id='input_13_1']").click()

# submitting the form which contains check box 
driver.find_element_by_xpath("//input[@id='input_13_1']").submit()

time.sleep(3)

# closing the browser 
driver.close()