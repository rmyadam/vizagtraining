from selenium import webdriver 
import time 
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys


# creation of driver object 
driver = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\assessment3\chromedriver_win32\chromedriver.exe")
driver.get("https://www.redbus.in/")  
driver.maximize_window()
time.sleep(3)

# sending the keys to the text box  
driver.find_element_by_xpath("//input[@id='src']").send_keys("vishakapatnam")

time.sleep(3)
# checking for visibilty of input field by is displayed 
print(driver.find_element_by_xpath("//input[@id='src']").is_displayed())

# check whether the field is enable or not 
print(driver.find_element_by_xpath("//input[@id='src']").is_enabled())

# printing the value of the text box  
print(driver.find_element_by_xpath("//input[@id='src']").get_attribute("value"))

# printing the type of the text box 
print(driver.find_element_by_xpath("//input[@id='src']").get_property("type"))

# check whether the text box is selected or not 
print(driver.find_element_by_xpath("//input[@id='src']").is_selected())

print(driver.find_element_by_xpath("//input[@id='src']").tag_name)

# printing the location of text box 
print(driver.find_element_by_xpath("//input[@id='src']").location)

# printing the location of text box 
print(driver.find_element_by_xpath("//input[@id='src']").rect)

# printing the size of text box 
print(driver.find_element_by_xpath("//input[@id='src']").size)

# printing the font size of text box 
print(driver.find_element_by_xpath("//input[@id='src']").value_of_css_property("font-size"))

# printing the parent object of text box 
print(driver.find_element_by_xpath("//input[@id='src']").parent)

# clearing the text box field 
driver.find_element_by_xpath("//input[@id='src']").clear()


time.sleep(3)
driver.close()