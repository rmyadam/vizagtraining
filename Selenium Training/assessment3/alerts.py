from selenium import webdriver 
import time 
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys


# creation of driver object 
driver = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\assessment3\chromedriver_win32\chromedriver.exe")

driver.get("file:///C:/Users/rmyadam/Desktop/vizagtraining/Selenium%20Training/assessment3/alerts.html")  

# locating and clicking the button 
driver.find_element_by_xpath("//button[@class='btn']").click()
time.sleep(3)


alert = driver.switch_to_alert()
time.sleep(3)

# accepting the alert 
alert.accept()
time.sleep(3)

# clicking the try it button 
driver.find_element_by_xpath("//button[@class='btn']").click()
time.sleep(3)

# printing the text of alert 
print(alert.text)

# dismissing the alert 
alert.dismiss()
time.sleep(3)
