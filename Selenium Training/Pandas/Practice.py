import pandas as pd 
import numpy as np 

def convertToNumpy():

    dataFrame =  pd.read_csv("Selenium Training\Pandas\cancerData.csv")

    print("the diagnosis Data numpy array is: ")
    diagnosis = dataFrame['diagnosis'].to_numpy()
    print(diagnosis)
    print("\n")

    print("The mean Data numpy array is: ")
    data1 = ['radius_mean',	'texture_mean', 'perimeter_mean', 'area_mean',	'smoothness_mean',	'compactness_mean',	'concavity_mean', 'concave points_mean', 'symmetry_mean']
    meanData = dataFrame[data1].to_numpy()
    print(meanData)
    print("\n")

    print("The se Data numpy array is: ")

    data2 = ['fractal_dimension_mean', 'radius_se',	'texture_se', 'perimeter_se', 'area_se', 'smoothness_se', 'compactness_se',	'concavity_se', 'concave points_se', 'symmetry_se']
    seData = dataFrame[data2].to_numpy()
    print(seData)
    print("\n")


    print("The worst Data numpy array is: ")
    data3 = ['radius_worst', 'texture_worst', 'perimeter_worst', 'area_worst', 'smoothness_worst', 'compactness_worst',	'concavity_worst', 'concave points_worst', 'symmetry_worst', 'fractal_dimension_worst']
    worstData = dataFrame[data3].to_numpy()
    print(worstData)
    print("\n")

def addColumn():

    dataFrame =  pd.read_csv("Selenium Training\Pandas\cancerData.csv")

    dataFrame['Total'] = dataFrame['symmetry_worst'] + dataFrame['fractal_dimension_worst']

    print(dataFrame)



if __name__=='__main__':
    convertToNumpy()
    addColumn()