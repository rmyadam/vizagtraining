"""
This program exhibits the java script executing in selenium 
@author Rithvik 
"""


from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time 


def javaScript():

    # creation of driver object and getting response from website 
    driver = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\August18\chromedriver_win32\chromedriver.exe")
    driver.get("https://stackoverflow.com/")
    
    # maximizing the window 
    driver.maximize_window()
    time.sleep(3)

    # locating the element by class in java script and clicking it
    # executing the java script using execute_script method 
    driver.execute_script("document.getElementsByClassName('login-link s-btn s-btn__filled py8 js-gps-track')[0].click()")
    time.sleep(3)

    # closing the browser 
    driver.close()

if __name__=="__main__":
    javaScript()