"""
This program contains class methods of unittest
@author Rithvik   
"""


from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest
import time


class TestCarwale(unittest.TestCase):
    """
    This class contains class methods of unittest 
    """

    @classmethod
    def setUpClass(cls):
        """
        This is a class method which executes first most of all the methods 
        defined in class 
        """
        # creation of webdriver object and getting response from website 
        cls.driver = webdriver.Chrome(r"C:\Users\rmyadam\Desktop\vizagtraining\Selenium Training\August18\chromedriver_win32\chromedriver.exe")
        cls.driver.get("https://stackoverflow.com/users/login?ssrc=head&returnurl=https%3a%2f%2fstackoverflow.com%2f")
        cls.driver.maximize_window()
    

    def setUp(self):
        """
        This method gets executed before executing each test case 
        """
        time.sleep(3)
        # refreshing web page 
        self.driver.refresh()


    def tearDown(self):
        """
        This method gets executed after executing each test case 
        """
        time.sleep(3)
        # clearing each text field in webpage 
        self.driver.find_element_by_id("email").clear()
        self.driver.find_element_by_id("password").clear()


    def testtitle(self):
        """
        This method asserts title of webpage  
        """
        self.assertEqual(self.driver.title,"Log In - Stack Overflow") 


    def testTextField(self):
        """
        This method asserts the display of the textfield 
        """
        # locating the text field 
        textbox = self.driver.find_element_by_id("email")
        # sending the keys to text field 
        textbox.send_keys("rithvik@aspire.com")
        # checking if text field is displayed 
        self.assertTrue(textbox.is_displayed())


    def testPassword(self):
        """
        This method asserts the select of the textfield 
        """
        # locating password field 
        password = self.driver.find_element_by_id("password")
        # sending keys into the passsword field 
        password.send_keys("rithvik")
        # checking whether password field is selected 
        self.assertFalse(password.is_selected())
        
    
    @classmethod 
    def tearDownClass(cls):
        """
        This method is executed after all test cases are executed 
        """
        # closing the browser 
        cls.driver.quit()


if __name__=="__main__":
    unittest.main()
