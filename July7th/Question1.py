'''
This program contains API's which are created by flask framework
@Rithvik 
'''

# importing Flask and rendert_template from flask module 
from flask import Flask,render_template

# creating a flask app named as webapp 
webapp = Flask(__name__)

# setting up route for home page and setting method as GET
@webapp.route('/home',methods=['GET'])
def home():
    '''
    This api returns the home.html page 
    '''
    return render_template('home.html')


# setting up route for login page and setting method as GETc
@webapp.route('/login',methods=['GET'])
def login():
    '''
    This api returns the login.html page 
    '''
    return render_template('login.html')


# setting up route for login page and setting method as GET
@webapp.route('/contact',methods=['GET'])
def contact():
    '''
    This api returns the contact.html page 
    '''
    return render_template('contact.html')


if __name__=='__main__':
    webapp.run(debug=True)