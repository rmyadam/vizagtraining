'''
This program tests the API's created in another file(Question1.py)
@Rithvik
'''

# importing unittest and flask module 
import unittest,flask
# importing the app that need to be tested by program 
# from Question1 file 
from Question1 import webapp 


class Test_Flaskapp(unittest.TestCase):
    '''
    This class inherits the TestCase class from unittest module to 
    test the APIs 

    It contains three testing suites for testing the login,home and 
    contact html pages 

    '''

    def test_login(self):
        '''
        This test suite tests the login page with respect to status code
        and the data in the response of that page 

        '''
        # creating a virtual client to test the app 
        tester = webapp.test_client(self)
        # getting the response from the login page 
        response = tester.get('/login',content_type = 'html/text')
        # checking the status code of the page 
        self.assertEqual(response.status_code,200)
        # checking the content in the page 
        self.assertTrue(b'Welcome to login page' in response.data)


    def test_home(self):
        '''
        This test suite tests the home page with respect to status code
        and the data in the response of that page 

        '''
        # creating a virtual client to test the app 
        tester = webapp.test_client(self)
        # getting the response from the home page 
        response = tester.get('/home',content_type = 'html/text')
        # checking the status code of the page 
        self.assertEqual(response.status_code,200)
        # checking the content in the page 
        self.assertTrue(b'Welcome to home page' in response.data)


    def test_contact(self):
        '''
        This test suite tests the home page with respect to status code
        and the data in the response of that page 

        '''
        # creating a virtual client to test the app 
        tester = webapp.test_client(self)
        # getting the response from the contact page 
        response = tester.get('/contact',content_type = 'html/text')
        # checking the status code of the page 
        self.assertEqual(response.status_code,200)
        # checking the content in the page 
        self.assertTrue(b'Welcome to contact page' in response.data)

if __name__=='__main__':
    unittest.main()